const fs = require('fs');
const path = require('path');

const RELATIVE_PATH_TO_K8S_SWAGGER = process.env['SWAGGER_PATH'] || './data/kubernetes-swagger.json';
const DATA_DIR = path.join(__dirname, '../data');
const SRC_DIR = path.join(__dirname, '../src');
const OUTPUT_DIR = path.join(SRC_DIR, '/typedefs');
const SPECIFICATION = require(RELATIVE_PATH_TO_K8S_SWAGGER);

try {
  fs.mkdirSync(OUTPUT_DIR);
} catch (ex) {
  if (ex.message.indexOf('file already exists') === -1) {
    throw ex;
  }
}

const hasDescriptionProperty = (object) => (object.description !== undefined);
const hasPropertiesProperty = (object) => (object.properties !== undefined);
const hasTypeProperty = (object) => (object.type !== undefined);
const onlyAPIKeys = (key) => (key.indexOf('io.k8s.api.') === 0);

const {definitions} = SPECIFICATION;
const availableKeys = Object.keys(definitions);
const processedKeys = [];
const skippedKeys = [];

availableKeys.forEach((key) => {
  const subdomains = key.split('.');
  const definitionObject = definitions[key];
  const definitionProperties = definitionObject.properties;
  const definitionType = subdomains[subdomains.length - 1];
  const definitionVersion = subdomains[subdomains.length - 2];
  const definitionGroup = subdomains[subdomains.length - 3];

  let fileData = `export const Version : string = "${definitionGroup}/${definitionVersion}";\n`;
  if (hasPropertiesProperty(definitionObject)) {
    fileData += `\n// ${normalizeDescription(definitionObject.description)}\n`;
    fileData += `export class ${definitionType} {\n`;
    const propertyIDs = Object.keys(definitionProperties);
    const importedTypes = []
    propertyIDs.forEach(propertyID => {
      const property = definitionProperties[propertyID];
      if (property.type !== undefined) {
        let typescriptType = normalizeType(property.type);
        switch(typescriptType) {
          case 'array':
            if (property.items.type !== undefined) {
              typescriptType = `${normalizeType(property.items.type)}[]`;
            } else if (property.items["$ref"] !== undefined) {
              const reference = property.items['$ref'];
              const referencedDefinition = dereferenceDefinition(reference);
              const referencedType = dereferenceType(reference);      
              if (importedTypes.indexOf(referencedType) === -1) {
                if (referencedDefinition != key) {
                  fileData = addImportStatement(fileData, referencedType, referencedDefinition);
                  importedTypes.push(referencedType);
                }
              }
              typescriptType = `${normalizeType(referencedType)}[]`;
            }
            break;
        }
        fileData += `  // ${normalizeDescription(property.description)}\n`;
        fileData += `  ${normalizePropertyID(propertyID)} : ${typescriptType}\n`
      } else if (property['$ref'] !== undefined) {
        const reference = property['$ref'];
        const referencedDefinition = dereferenceDefinition(reference);
        const referencedType = dereferenceType(reference);
        if (importedTypes.indexOf(referencedType) === -1) {
          if (referencedDefinition != key) {
            fileData = addImportStatement(fileData, referencedType, referencedDefinition);
            importedTypes.push(referencedType);
          }
        }
        fileData += `  // ${normalizeDescription(property.description)}\n`;
        fileData += `  ${normalizePropertyID(propertyID)} : ${referencedType}\n`;
      }
    });
    fileData += `}\n`;
    fs.writeFileSync(path.join(OUTPUT_DIR, `/${key}.ts`), fileData.trimLeft());
    processedKeys.push(key);
  } else if (hasTypeProperty(definitionObject)) {
    fileData += `// ${normalizeDescription(definitionObject.description)}\nexport type ${definitionType} = ${definitionObject.type};\n`;
    fs.writeFileSync(path.join(OUTPUT_DIR, `/${key}.ts`), fileData.trimLeft());
    processedKeys.push(key);
  } else if (hasDescriptionProperty(definitionObject)) {
    fileData += `// ${normalizeDescription(definitionObject.description)}\nexport type ${definitionType} = any;\n`;
    fs.writeFileSync(path.join(OUTPUT_DIR, `/${key}.ts`), fileData.trimLeft());
    processedKeys.push(key);
  } else {
    skippedKeys.push(key);
  }
});

function addImportStatement(existingFileData, referencedType, referencedDefinition) {
  return `import { ${referencedType} } from './${referencedDefinition}';\n` + existingFileData;
}

function dereferenceDefinition(reference) {
  const referencePath = reference.split('/');
  const referencedDefinition = referencePath[referencePath.length - 1];
  return referencedDefinition;
}

function dereferenceType(reference) {
  const referencedDefinition = dereferenceDefinition(reference);
  const referencedDefinitionPath = referencedDefinition.split('.');
  const referencedType = referencedDefinitionPath[referencedDefinitionPath.length - 1];
  return referencedType;
}

function normalizeType(propertyType) {
  switch(propertyType) {
    case 'integer':
      return 'number';
  }
  return propertyType;
}

function normalizeDescription(description) {
  if (description === undefined) {
    return 'description not available';
  } else {
    return description.replace(/\n/gi, ' ');
  }
}

function normalizePropertyID(propertyID) {
  if (propertyID.indexOf('-') !== -1) {
    return `"${propertyID}"`;
  } else {
    return propertyID;
  }
}

console.info('processed keys:');
console.info(processedKeys);
console.info('skipped keys:');
console.info(skippedKeys);

console.info(`skipped: ${skippedKeys.length}/${processedKeys.length}`)
if (skippedKeys.length === 0) {
  console.info('all is well (here in hell!)');
} else {
  console.warn('some keys may not have been exported!');
}

console.info('creating index.ts now...');
const typedefListing = fs.readdirSync(OUTPUT_DIR).filter(onlyAPIKeys);
let modules = {};
typedefListing.forEach((listing) => {
  const listingDomains = listing.replace(/(io\.k8s\.api\.|\.ts)/gi, '').split('.');
  const listingGroup = listingDomains[0];
  const listingVersion = listingDomains[1];
  const listingType = listingDomains[2];
  if (!modules[listingGroup]) {
    modules[listingGroup] = {};
  }
  if (!modules[listingGroup][listingVersion]) {
    modules[listingGroup][listingVersion] = {};
  }
  modules[listingGroup][listingVersion][listingType] = listing;
});

let indexFileData = '';

const k8sversion = fs.readFileSync(path.join(DATA_DIR, '/latest'));
indexFileData += `\n// version of kubernetes release these definitions are from\n`;
indexFileData += `export const version = '${k8sversion}';\n\n`;

const groupKeys = Object.keys(modules);
groupKeys.forEach((groupKey) => {
  indexFileData += `export const ${groupKey} = {\n`;
  const group = modules[groupKey];
  versionKeys = Object.keys(group);
  versionKeys.forEach((versionKey) => {
    indexFileData += `  ${versionKey}: {\n`;
    const version = group[versionKey];
    resourceKeys = Object.keys(version);
    resourceKeys.forEach((resourceKey) => {
      const importPath = version[resourceKey].replace('.ts', '');
      indexFileData = `import { ${resourceKey} as ${groupKey}${versionKey}${resourceKey} } from './typedefs/${importPath}';\n` + indexFileData;
      indexFileData += `    ${resourceKey}: ${groupKey}${versionKey}${resourceKey},\n`;
    });
    indexFileData += '  },\n';
  });
  indexFileData += '};\n\n';
});

fs.writeFileSync(path.join(SRC_DIR, '/index.ts'), indexFileData);
