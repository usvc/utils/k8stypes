import { WebhookThrottleConfig } from './io.k8s.api.auditregistration.v1alpha1.WebhookThrottleConfig';
import { WebhookClientConfig } from './io.k8s.api.auditregistration.v1alpha1.WebhookClientConfig';
export const Version : string = "auditregistration/v1alpha1";

// Webhook holds the configuration of the webhook
export class Webhook {
  // ClientConfig holds the connection parameters for the webhook required
  clientConfig : WebhookClientConfig
  // Throttle holds the options for throttling the webhook
  throttle : WebhookThrottleConfig
}
