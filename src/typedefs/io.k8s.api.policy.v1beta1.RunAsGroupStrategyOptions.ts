import { IDRange } from './io.k8s.api.policy.v1beta1.IDRange';
export const Version : string = "policy/v1beta1";

// RunAsGroupStrategyOptions defines the strategy type and any options used to create the strategy.
export class RunAsGroupStrategyOptions {
  // ranges are the allowed ranges of gids that may be used. If you would like to force a single gid then supply a single range with the same start and end. Required for MustRunAs.
  ranges : IDRange[]
  // rule is the strategy that will dictate the allowable RunAsGroup values that may be set.
  rule : string
}
