import { SELinuxOptions } from './io.k8s.api.core.v1.SELinuxOptions';
export const Version : string = "extensions/v1beta1";

// SELinuxStrategyOptions defines the strategy type and any options used to create the strategy. Deprecated: use SELinuxStrategyOptions from policy API Group instead.
export class SELinuxStrategyOptions {
  // rule is the strategy that will dictate the allowable labels that may be set.
  rule : string
  // seLinuxOptions required to run as; required for MustRunAs More info: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
  seLinuxOptions : SELinuxOptions
}
