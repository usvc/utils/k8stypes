import { ContainerState } from './io.k8s.api.core.v1.ContainerState';
export const Version : string = "core/v1";

// ContainerStatus contains details for the current status of this container.
export class ContainerStatus {
  // Container's ID in the format 'docker://<container_id>'.
  containerID : string
  // The image the container is running. More info: https://kubernetes.io/docs/concepts/containers/images
  image : string
  // ImageID of the container's image.
  imageID : string
  // Details about the container's last termination condition.
  lastState : ContainerState
  // This must be a DNS_LABEL. Each container in a pod must have a unique name. Cannot be updated.
  name : string
  // Specifies whether the container has passed its readiness probe.
  ready : boolean
  // The number of times the container has been restarted, currently based on the number of dead containers that have not yet been removed. Note that this is calculated from dead containers. But those containers are subject to garbage collection. This value will get capped at 5 by GC.
  restartCount : number
  // Details about the container's current condition.
  state : ContainerState
}
