export const Version : string = "apiextensions/v1beta1";
// JSONSchemaPropsOrBool represents JSONSchemaProps or a boolean value. Defaults to true for the boolean property.
export type JSONSchemaPropsOrBool = any;
