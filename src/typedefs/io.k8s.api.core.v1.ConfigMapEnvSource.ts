export const Version : string = "core/v1";

// ConfigMapEnvSource selects a ConfigMap to populate the environment variables with.  The contents of the target ConfigMap's Data field will represent the key-value pairs as environment variables.
export class ConfigMapEnvSource {
  // Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
  name : string
  // Specify whether the ConfigMap must be defined
  optional : boolean
}
