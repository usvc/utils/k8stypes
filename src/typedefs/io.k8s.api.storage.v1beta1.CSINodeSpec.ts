import { CSINodeDriver } from './io.k8s.api.storage.v1beta1.CSINodeDriver';
export const Version : string = "storage/v1beta1";

// CSINodeSpec holds information about the specification of all CSI drivers installed on a node
export class CSINodeSpec {
  // drivers is a list of information of all CSI Drivers existing on a node. If all drivers in the list are uninstalled, this can become empty.
  drivers : CSINodeDriver[]
}
