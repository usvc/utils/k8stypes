import { PodSpec } from './io.k8s.api.core.v1.PodSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "core/v1";

// PodTemplateSpec describes the data a pod should have when created from a template
export class PodTemplateSpec {
  // Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ObjectMeta
  // Specification of the desired behavior of the pod. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status
  spec : PodSpec
}
