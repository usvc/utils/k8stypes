export const Version : string = "core/v1";

// Local represents directly-attached storage with node affinity (Beta feature)
export class LocalVolumeSource {
  // Filesystem type to mount. It applies only when the Path is a block device. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". The default value is to auto-select a fileystem if unspecified.
  fsType : string
  // The full path to the volume on the node. It can be either a directory or block device (disk, partition, ...).
  path : string
}
