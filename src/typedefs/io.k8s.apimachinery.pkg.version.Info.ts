export const Version : string = "pkg/version";

// Info contains versioning information. how we'll want to distribute that information.
export class Info {
  // description not available
  buildDate : string
  // description not available
  compiler : string
  // description not available
  gitCommit : string
  // description not available
  gitTreeState : string
  // description not available
  gitVersion : string
  // description not available
  goVersion : string
  // description not available
  major : string
  // description not available
  minor : string
  // description not available
  platform : string
}
