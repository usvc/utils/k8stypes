import { ConfigMapNodeConfigSource } from './io.k8s.api.core.v1.ConfigMapNodeConfigSource';
export const Version : string = "core/v1";

// NodeConfigSource specifies a source of node configuration. Exactly one subfield (excluding metadata) must be non-nil.
export class NodeConfigSource {
  // ConfigMap is a reference to a Node's ConfigMap
  configMap : ConfigMapNodeConfigSource
}
