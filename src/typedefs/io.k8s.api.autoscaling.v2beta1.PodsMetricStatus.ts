import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
export const Version : string = "autoscaling/v2beta1";

// PodsMetricStatus indicates the current value of a metric describing each pod in the current scale target (for example, transactions-processed-per-second).
export class PodsMetricStatus {
  // currentAverageValue is the current value of the average of the metric across all relevant pods (as a quantity)
  currentAverageValue : Quantity
  // metricName is the name of the metric in question
  metricName : string
  // selector is the string-encoded form of a standard kubernetes label selector for the given metric When set in the PodsMetricSource, it is passed as an additional parameter to the metrics server for more specific metrics scoping. When unset, just the metricName will be used to gather metrics.
  selector : LabelSelector
}
