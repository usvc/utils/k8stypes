import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "apiregistration/v1";

// APIServiceCondition describes the state of an APIService at a particular point
export class APIServiceCondition {
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // Human-readable message indicating details about last transition.
  message : string
  // Unique, one-word, CamelCase reason for the condition's last transition.
  reason : string
  // Status is the status of the condition. Can be True, False, Unknown.
  status : string
  // Type is the type of the condition.
  type : string
}
