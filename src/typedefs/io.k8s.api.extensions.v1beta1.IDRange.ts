export const Version : string = "extensions/v1beta1";

// IDRange provides a min/max of an allowed range of IDs. Deprecated: use IDRange from policy API Group instead.
export class IDRange {
  // max is the end of the range, inclusive.
  max : number
  // min is the start of the range, inclusive.
  min : number
}
