import { NodeSelectorTerm } from './io.k8s.api.core.v1.NodeSelectorTerm';
export const Version : string = "core/v1";

// A node selector represents the union of the results of one or more label queries over a set of nodes; that is, it represents the OR of the selectors represented by the node selector terms.
export class NodeSelector {
  // Required. A list of node selector terms. The terms are ORed.
  nodeSelectorTerms : NodeSelectorTerm[]
}
