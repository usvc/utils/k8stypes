export const Version : string = "meta/v1";

// Initializer is information about an initializer that has not yet completed.
export class Initializer {
  // name of the process that is responsible for initializing this object.
  name : string
}
