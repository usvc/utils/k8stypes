export const Version : string = "core/v1";

// NodeAddress contains information for the node's address.
export class NodeAddress {
  // The node address.
  address : string
  // Node address type, one of Hostname, ExternalIP or InternalIP.
  type : string
}
