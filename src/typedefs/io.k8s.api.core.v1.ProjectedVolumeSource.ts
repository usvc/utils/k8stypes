import { VolumeProjection } from './io.k8s.api.core.v1.VolumeProjection';
export const Version : string = "core/v1";

// Represents a projected volume source
export class ProjectedVolumeSource {
  // Mode bits to use on created files by default. Must be a value between 0 and 0777. Directories within the path are not affected by this setting. This might be in conflict with other options that affect the file mode, like fsGroup, and the result can be other mode bits set.
  defaultMode : number
  // list of volume projections
  sources : VolumeProjection[]
}
