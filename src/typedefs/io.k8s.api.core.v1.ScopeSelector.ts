import { ScopedResourceSelectorRequirement } from './io.k8s.api.core.v1.ScopedResourceSelectorRequirement';
export const Version : string = "core/v1";

// A scope selector represents the AND of the selectors represented by the scoped-resource selector requirements.
export class ScopeSelector {
  // A list of scope selector requirements by scope of the resources.
  matchExpressions : ScopedResourceSelectorRequirement[]
}
