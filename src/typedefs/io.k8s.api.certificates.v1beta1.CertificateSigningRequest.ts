import { CertificateSigningRequestStatus } from './io.k8s.api.certificates.v1beta1.CertificateSigningRequestStatus';
import { CertificateSigningRequestSpec } from './io.k8s.api.certificates.v1beta1.CertificateSigningRequestSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "certificates/v1beta1";

// Describes a certificate signing request
export class CertificateSigningRequest {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // The certificate request itself and any additional information.
  spec : CertificateSigningRequestSpec
  // Derived information about the request.
  status : CertificateSigningRequestStatus
}
