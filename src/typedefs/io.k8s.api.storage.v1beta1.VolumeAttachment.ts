import { VolumeAttachmentStatus } from './io.k8s.api.storage.v1beta1.VolumeAttachmentStatus';
import { VolumeAttachmentSpec } from './io.k8s.api.storage.v1beta1.VolumeAttachmentSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "storage/v1beta1";

// VolumeAttachment captures the intent to attach or detach the specified volume to/from the specified node.  VolumeAttachment objects are non-namespaced.
export class VolumeAttachment {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ObjectMeta
  // Specification of the desired attach/detach volume behavior. Populated by the Kubernetes system.
  spec : VolumeAttachmentSpec
  // Status of the VolumeAttachment request. Populated by the entity completing the attach or detach operation, i.e. the external-attacher.
  status : VolumeAttachmentStatus
}
