import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "core/v1";

// PodCondition contains details for the current condition of this pod.
export class PodCondition {
  // Last time we probed the condition.
  lastProbeTime : Time
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // Human-readable message indicating details about last transition.
  message : string
  // Unique, one-word, CamelCase reason for the condition's last transition.
  reason : string
  // Status is the status of the condition. Can be True, False, Unknown. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#pod-conditions
  status : string
  // Type is the type of the condition. More info: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#pod-conditions
  type : string
}
