import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
import { Fields } from './io.k8s.apimachinery.pkg.apis.meta.v1.Fields';
export const Version : string = "meta/v1";

// ManagedFieldsEntry is a workflow-id, a FieldSet and the group version of the resource that the fieldset applies to.
export class ManagedFieldsEntry {
  // APIVersion defines the version of this resource that this field set applies to. The format is "group/version" just like the top-level APIVersion field. It is necessary to track the version of a field set because it cannot be automatically converted.
  apiVersion : string
  // Fields identifies a set of fields.
  fields : Fields
  // Manager is an identifier of the workflow managing these fields.
  manager : string
  // Operation is the type of operation which lead to this ManagedFieldsEntry being created. The only valid values for this field are 'Apply' and 'Update'.
  operation : string
  // Time is timestamp of when these fields were set. It should always be empty if Operation is 'Apply'
  time : Time
}
