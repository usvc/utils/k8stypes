export const Version : string = "core/v1";

// PersistentVolumeStatus is the current status of a persistent volume.
export class PersistentVolumeStatus {
  // A human-readable message indicating details about why the volume is in this state.
  message : string
  // Phase indicates if a volume is available, bound to a claim, or released by a claim. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#phase
  phase : string
  // Reason is a brief CamelCase string that describes any failure and is meant for machine parsing and tidy display in the CLI.
  reason : string
}
