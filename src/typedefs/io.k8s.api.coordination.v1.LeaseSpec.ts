import { MicroTime } from './io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime';
export const Version : string = "coordination/v1";

// LeaseSpec is a specification of a Lease.
export class LeaseSpec {
  // acquireTime is a time when the current lease was acquired.
  acquireTime : MicroTime
  // holderIdentity contains the identity of the holder of a current lease.
  holderIdentity : string
  // leaseDurationSeconds is a duration that candidates for a lease need to wait to force acquire it. This is measure against time of last observed RenewTime.
  leaseDurationSeconds : number
  // leaseTransitions is the number of transitions of a lease between holders.
  leaseTransitions : number
  // renewTime is a time when the current holder of a lease has last updated the lease.
  renewTime : MicroTime
}
