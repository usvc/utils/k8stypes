import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "core/v1";

// ContainerStateRunning is a running state of a container.
export class ContainerStateRunning {
  // Time at which the container was last (re-)started
  startedAt : Time
}
