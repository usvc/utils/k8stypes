import { Volume } from './io.k8s.api.core.v1.Volume';
import { VolumeMount } from './io.k8s.api.core.v1.VolumeMount';
import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { EnvFromSource } from './io.k8s.api.core.v1.EnvFromSource';
import { EnvVar } from './io.k8s.api.core.v1.EnvVar';
export const Version : string = "settings/v1alpha1";

// PodPresetSpec is a description of a pod preset.
export class PodPresetSpec {
  // Env defines the collection of EnvVar to inject into containers.
  env : EnvVar[]
  // EnvFrom defines the collection of EnvFromSource to inject into containers.
  envFrom : EnvFromSource[]
  // Selector is a label query over a set of resources, in this case pods. Required.
  selector : LabelSelector
  // VolumeMounts defines the collection of VolumeMount to inject into containers.
  volumeMounts : VolumeMount[]
  // Volumes defines the collection of Volume to inject into the pod.
  volumes : Volume[]
}
