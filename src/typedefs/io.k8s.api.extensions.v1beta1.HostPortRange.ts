export const Version : string = "extensions/v1beta1";

// HostPortRange defines a range of host ports that will be enabled by a policy for pods to use.  It requires both the start and end to be defined. Deprecated: use HostPortRange from policy API Group instead.
export class HostPortRange {
  // max is the end of the range, inclusive.
  max : number
  // min is the start of the range, inclusive.
  min : number
}
