import { CrossVersionObjectReference } from './io.k8s.api.autoscaling.v2beta1.CrossVersionObjectReference';
import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
export const Version : string = "autoscaling/v2beta1";

// ObjectMetricSource indicates how to scale on a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
export class ObjectMetricSource {
  // averageValue is the target value of the average of the metric across all relevant pods (as a quantity)
  averageValue : Quantity
  // metricName is the name of the metric in question.
  metricName : string
  // selector is the string-encoded form of a standard kubernetes label selector for the given metric When set, it is passed as an additional parameter to the metrics server for more specific metrics scoping When unset, just the metricName will be used to gather metrics.
  selector : LabelSelector
  // target is the described Kubernetes object.
  target : CrossVersionObjectReference
  // targetValue is the target value of the metric (as a quantity).
  targetValue : Quantity
}
