export const Version : string = "core/v1";

// PodDNSConfigOption defines DNS resolver options of a pod.
export class PodDNSConfigOption {
  // Required.
  name : string
  // description not available
  value : string
}
