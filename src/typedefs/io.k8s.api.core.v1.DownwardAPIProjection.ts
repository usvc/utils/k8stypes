import { DownwardAPIVolumeFile } from './io.k8s.api.core.v1.DownwardAPIVolumeFile';
export const Version : string = "core/v1";

// Represents downward API info for projecting into a projected volume. Note that this is identical to a downwardAPI volume source without the default mode.
export class DownwardAPIProjection {
  // Items is a list of DownwardAPIVolume file
  items : DownwardAPIVolumeFile[]
}
