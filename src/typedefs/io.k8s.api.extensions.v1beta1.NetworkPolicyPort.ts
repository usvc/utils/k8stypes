import { IntOrString } from './io.k8s.apimachinery.pkg.util.intstr.IntOrString';
export const Version : string = "extensions/v1beta1";

// DEPRECATED 1.9 - This group version of NetworkPolicyPort is deprecated by networking/v1/NetworkPolicyPort.
export class NetworkPolicyPort {
  // If specified, the port on the given protocol.  This can either be a numerical or named port on a pod.  If this field is not provided, this matches all port names and numbers. If present, only traffic on the specified protocol AND port will be matched.
  port : IntOrString
  // Optional.  The protocol (TCP, UDP, or SCTP) which traffic must match. If not specified, this field defaults to TCP.
  protocol : string
}
