export const Version : string = "core/v1";

// Represents a Photon Controller persistent disk resource.
export class PhotonPersistentDiskVolumeSource {
  // Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified.
  fsType : string
  // ID that identifies Photon Controller persistent disk
  pdID : string
}
