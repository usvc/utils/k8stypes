export const Version : string = "core/v1";

// PortworxVolumeSource represents a Portworx volume resource.
export class PortworxVolumeSource {
  // FSType represents the filesystem type to mount Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs". Implicitly inferred to be "ext4" if unspecified.
  fsType : string
  // Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
  readOnly : boolean
  // VolumeID uniquely identifies a Portworx volume
  volumeID : string
}
