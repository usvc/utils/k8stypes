import { LoadBalancerStatus } from './io.k8s.api.core.v1.LoadBalancerStatus';
export const Version : string = "core/v1";

// ServiceStatus represents the current status of a service.
export class ServiceStatus {
  // LoadBalancer contains the current status of the load-balancer, if one is present.
  loadBalancer : LoadBalancerStatus
}
