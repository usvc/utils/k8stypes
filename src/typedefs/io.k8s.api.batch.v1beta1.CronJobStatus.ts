import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
import { ObjectReference } from './io.k8s.api.core.v1.ObjectReference';
export const Version : string = "batch/v1beta1";

// CronJobStatus represents the current state of a cron job.
export class CronJobStatus {
  // A list of pointers to currently running jobs.
  active : ObjectReference[]
  // Information when was the last time the job was successfully scheduled.
  lastScheduleTime : Time
}
