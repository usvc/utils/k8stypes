import { TokenReviewStatus } from './io.k8s.api.authentication.v1beta1.TokenReviewStatus';
import { TokenReviewSpec } from './io.k8s.api.authentication.v1beta1.TokenReviewSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "authentication/v1beta1";

// TokenReview attempts to authenticate a token to a known user. Note: TokenReview requests may be cached by the webhook token authenticator plugin in the kube-apiserver.
export class TokenReview {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec holds information about the request being evaluated
  spec : TokenReviewSpec
  // Status is filled in by the server and indicates whether the request can be authenticated.
  status : TokenReviewStatus
}
