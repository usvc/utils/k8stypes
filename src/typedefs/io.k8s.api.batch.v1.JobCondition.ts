import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "batch/v1";

// JobCondition describes current state of a job.
export class JobCondition {
  // Last time the condition was checked.
  lastProbeTime : Time
  // Last time the condition transit from one status to another.
  lastTransitionTime : Time
  // Human readable message indicating details about last transition.
  message : string
  // (brief) reason for the condition's last transition.
  reason : string
  // Status of the condition, one of True, False, Unknown.
  status : string
  // Type of job condition, Complete or Failed.
  type : string
}
