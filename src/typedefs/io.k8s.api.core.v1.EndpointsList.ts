import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { Endpoints } from './io.k8s.api.core.v1.Endpoints';
export const Version : string = "core/v1";

// EndpointsList is a list of endpoints.
export class EndpointsList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // List of endpoints.
  items : Endpoints[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  metadata : ListMeta
}
