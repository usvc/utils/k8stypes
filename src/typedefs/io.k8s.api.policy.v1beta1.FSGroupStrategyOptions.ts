import { IDRange } from './io.k8s.api.policy.v1beta1.IDRange';
export const Version : string = "policy/v1beta1";

// FSGroupStrategyOptions defines the strategy type and options used to create the strategy.
export class FSGroupStrategyOptions {
  // ranges are the allowed ranges of fs groups.  If you would like to force a single fs group then supply a single range with the same start and end. Required for MustRunAs.
  ranges : IDRange[]
  // rule is the strategy that will dictate what FSGroup is used in the SecurityContext.
  rule : string
}
