import { NodeSelector } from './io.k8s.api.core.v1.NodeSelector';
export const Version : string = "core/v1";

// VolumeNodeAffinity defines constraints that limit what nodes this volume can be accessed from.
export class VolumeNodeAffinity {
  // Required specifies hard node constraints that must be met.
  required : NodeSelector
}
