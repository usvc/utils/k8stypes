import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "apiextensions/v1beta1";

// CustomResourceDefinitionCondition contains details for the current condition of this pod.
export class CustomResourceDefinitionCondition {
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // Human-readable message indicating details about last transition.
  message : string
  // Unique, one-word, CamelCase reason for the condition's last transition.
  reason : string
  // Status is the status of the condition. Can be True, False, Unknown.
  status : string
  // Type is the type of the condition. Types include Established, NamesAccepted and Terminating.
  type : string
}
