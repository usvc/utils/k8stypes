import { VsphereVirtualDiskVolumeSource } from './io.k8s.api.core.v1.VsphereVirtualDiskVolumeSource';
import { StorageOSVolumeSource } from './io.k8s.api.core.v1.StorageOSVolumeSource';
import { SecretVolumeSource } from './io.k8s.api.core.v1.SecretVolumeSource';
import { ScaleIOVolumeSource } from './io.k8s.api.core.v1.ScaleIOVolumeSource';
import { RBDVolumeSource } from './io.k8s.api.core.v1.RBDVolumeSource';
import { QuobyteVolumeSource } from './io.k8s.api.core.v1.QuobyteVolumeSource';
import { ProjectedVolumeSource } from './io.k8s.api.core.v1.ProjectedVolumeSource';
import { PortworxVolumeSource } from './io.k8s.api.core.v1.PortworxVolumeSource';
import { PhotonPersistentDiskVolumeSource } from './io.k8s.api.core.v1.PhotonPersistentDiskVolumeSource';
import { PersistentVolumeClaimVolumeSource } from './io.k8s.api.core.v1.PersistentVolumeClaimVolumeSource';
import { NFSVolumeSource } from './io.k8s.api.core.v1.NFSVolumeSource';
import { ISCSIVolumeSource } from './io.k8s.api.core.v1.ISCSIVolumeSource';
import { HostPathVolumeSource } from './io.k8s.api.core.v1.HostPathVolumeSource';
import { GlusterfsVolumeSource } from './io.k8s.api.core.v1.GlusterfsVolumeSource';
import { GitRepoVolumeSource } from './io.k8s.api.core.v1.GitRepoVolumeSource';
import { GCEPersistentDiskVolumeSource } from './io.k8s.api.core.v1.GCEPersistentDiskVolumeSource';
import { FlockerVolumeSource } from './io.k8s.api.core.v1.FlockerVolumeSource';
import { FlexVolumeSource } from './io.k8s.api.core.v1.FlexVolumeSource';
import { FCVolumeSource } from './io.k8s.api.core.v1.FCVolumeSource';
import { EmptyDirVolumeSource } from './io.k8s.api.core.v1.EmptyDirVolumeSource';
import { DownwardAPIVolumeSource } from './io.k8s.api.core.v1.DownwardAPIVolumeSource';
import { CSIVolumeSource } from './io.k8s.api.core.v1.CSIVolumeSource';
import { ConfigMapVolumeSource } from './io.k8s.api.core.v1.ConfigMapVolumeSource';
import { CinderVolumeSource } from './io.k8s.api.core.v1.CinderVolumeSource';
import { CephFSVolumeSource } from './io.k8s.api.core.v1.CephFSVolumeSource';
import { AzureFileVolumeSource } from './io.k8s.api.core.v1.AzureFileVolumeSource';
import { AzureDiskVolumeSource } from './io.k8s.api.core.v1.AzureDiskVolumeSource';
import { AWSElasticBlockStoreVolumeSource } from './io.k8s.api.core.v1.AWSElasticBlockStoreVolumeSource';
export const Version : string = "core/v1";

// Volume represents a named volume in a pod that may be accessed by any container in the pod.
export class Volume {
  // AWSElasticBlockStore represents an AWS Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#awselasticblockstore
  awsElasticBlockStore : AWSElasticBlockStoreVolumeSource
  // AzureDisk represents an Azure Data Disk mount on the host and bind mount to the pod.
  azureDisk : AzureDiskVolumeSource
  // AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
  azureFile : AzureFileVolumeSource
  // CephFS represents a Ceph FS mount on the host that shares a pod's lifetime
  cephfs : CephFSVolumeSource
  // Cinder represents a cinder volume attached and mounted on kubelets host machine More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
  cinder : CinderVolumeSource
  // ConfigMap represents a configMap that should populate this volume
  configMap : ConfigMapVolumeSource
  // CSI (Container Storage Interface) represents storage that is handled by an external CSI driver (Alpha feature).
  csi : CSIVolumeSource
  // DownwardAPI represents downward API about the pod that should populate this volume
  downwardAPI : DownwardAPIVolumeSource
  // EmptyDir represents a temporary directory that shares a pod's lifetime. More info: https://kubernetes.io/docs/concepts/storage/volumes#emptydir
  emptyDir : EmptyDirVolumeSource
  // FC represents a Fibre Channel resource that is attached to a kubelet's host machine and then exposed to the pod.
  fc : FCVolumeSource
  // FlexVolume represents a generic volume resource that is provisioned/attached using an exec based plugin.
  flexVolume : FlexVolumeSource
  // Flocker represents a Flocker volume attached to a kubelet's host machine. This depends on the Flocker control service being running
  flocker : FlockerVolumeSource
  // GCEPersistentDisk represents a GCE Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#gcepersistentdisk
  gcePersistentDisk : GCEPersistentDiskVolumeSource
  // GitRepo represents a git repository at a particular revision. DEPRECATED: GitRepo is deprecated. To provision a container with a git repo, mount an EmptyDir into an InitContainer that clones the repo using git, then mount the EmptyDir into the Pod's container.
  gitRepo : GitRepoVolumeSource
  // Glusterfs represents a Glusterfs mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/glusterfs/README.md
  glusterfs : GlusterfsVolumeSource
  // HostPath represents a pre-existing file or directory on the host machine that is directly exposed to the container. This is generally used for system agents or other privileged things that are allowed to see the host machine. Most containers will NOT need this. More info: https://kubernetes.io/docs/concepts/storage/volumes#hostpath
  hostPath : HostPathVolumeSource
  // ISCSI represents an ISCSI Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://releases.k8s.io/HEAD/examples/volumes/iscsi/README.md
  iscsi : ISCSIVolumeSource
  // Volume's name. Must be a DNS_LABEL and unique within the pod. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
  name : string
  // NFS represents an NFS mount on the host that shares a pod's lifetime More info: https://kubernetes.io/docs/concepts/storage/volumes#nfs
  nfs : NFSVolumeSource
  // PersistentVolumeClaimVolumeSource represents a reference to a PersistentVolumeClaim in the same namespace. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
  persistentVolumeClaim : PersistentVolumeClaimVolumeSource
  // PhotonPersistentDisk represents a PhotonController persistent disk attached and mounted on kubelets host machine
  photonPersistentDisk : PhotonPersistentDiskVolumeSource
  // PortworxVolume represents a portworx volume attached and mounted on kubelets host machine
  portworxVolume : PortworxVolumeSource
  // Items for all in one resources secrets, configmaps, and downward API
  projected : ProjectedVolumeSource
  // Quobyte represents a Quobyte mount on the host that shares a pod's lifetime
  quobyte : QuobyteVolumeSource
  // RBD represents a Rados Block Device mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/rbd/README.md
  rbd : RBDVolumeSource
  // ScaleIO represents a ScaleIO persistent volume attached and mounted on Kubernetes nodes.
  scaleIO : ScaleIOVolumeSource
  // Secret represents a secret that should populate this volume. More info: https://kubernetes.io/docs/concepts/storage/volumes#secret
  secret : SecretVolumeSource
  // StorageOS represents a StorageOS volume attached and mounted on Kubernetes nodes.
  storageos : StorageOSVolumeSource
  // VsphereVolume represents a vSphere volume attached and mounted on kubelets host machine
  vsphereVolume : VsphereVirtualDiskVolumeSource
}
