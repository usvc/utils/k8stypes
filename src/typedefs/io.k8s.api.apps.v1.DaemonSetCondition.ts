import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "apps/v1";

// DaemonSetCondition describes the state of a DaemonSet at a certain point.
export class DaemonSetCondition {
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // A human readable message indicating details about the transition.
  message : string
  // The reason for the condition's last transition.
  reason : string
  // Status of the condition, one of True, False, Unknown.
  status : string
  // Type of DaemonSet condition.
  type : string
}
