import { MicroTime } from './io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime';
export const Version : string = "events/v1beta1";

// EventSeries contain information on series of events, i.e. thing that was/is happening continuously for some time.
export class EventSeries {
  // Number of occurrences in this series up to the last heartbeat time
  count : number
  // Time when last Event from the series was seen before last heartbeat.
  lastObservedTime : MicroTime
  // Information whether this series is ongoing or finished. Deprecated. Planned removal for 1.18
  state : string
}
