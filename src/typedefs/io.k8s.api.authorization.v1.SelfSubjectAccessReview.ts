import { SubjectAccessReviewStatus } from './io.k8s.api.authorization.v1.SubjectAccessReviewStatus';
import { SelfSubjectAccessReviewSpec } from './io.k8s.api.authorization.v1.SelfSubjectAccessReviewSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "authorization/v1";

// SelfSubjectAccessReview checks whether or the current user can perform an action.  Not filling in a spec.namespace means "in all namespaces".  Self is a special case, because users should always be able to check whether they can perform an action
export class SelfSubjectAccessReview {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec holds information about the request being evaluated.  user and groups must be empty
  spec : SelfSubjectAccessReviewSpec
  // Status is filled in by the server and indicates whether the request is allowed or not
  status : SubjectAccessReviewStatus
}
