import { SubjectAccessReviewStatus } from './io.k8s.api.authorization.v1.SubjectAccessReviewStatus';
import { SubjectAccessReviewSpec } from './io.k8s.api.authorization.v1.SubjectAccessReviewSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "authorization/v1";

// LocalSubjectAccessReview checks whether or not a user or group can perform an action in a given namespace. Having a namespace scoped resource makes it much easier to grant namespace scoped policy that includes permissions checking.
export class LocalSubjectAccessReview {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec holds information about the request being evaluated.  spec.namespace must be equal to the namespace you made the request against.  If empty, it is defaulted.
  spec : SubjectAccessReviewSpec
  // Status is filled in by the server and indicates whether the request is allowed or not
  status : SubjectAccessReviewStatus
}
