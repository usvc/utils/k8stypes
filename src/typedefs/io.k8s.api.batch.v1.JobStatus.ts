import { JobCondition } from './io.k8s.api.batch.v1.JobCondition';
import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "batch/v1";

// JobStatus represents the current state of a Job.
export class JobStatus {
  // The number of actively running pods.
  active : number
  // Represents time when the job was completed. It is not guaranteed to be set in happens-before order across separate operations. It is represented in RFC3339 form and is in UTC.
  completionTime : Time
  // The latest available observations of an object's current state. More info: https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/
  conditions : JobCondition[]
  // The number of pods which reached phase Failed.
  failed : number
  // Represents time when the job was acknowledged by the job controller. It is not guaranteed to be set in happens-before order across separate operations. It is represented in RFC3339 form and is in UTC.
  startTime : Time
  // The number of pods which reached phase Succeeded.
  succeeded : number
}
