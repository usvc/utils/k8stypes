import { Webhook } from './io.k8s.api.auditregistration.v1alpha1.Webhook';
import { Policy } from './io.k8s.api.auditregistration.v1alpha1.Policy';
export const Version : string = "auditregistration/v1alpha1";

// AuditSinkSpec holds the spec for the audit sink
export class AuditSinkSpec {
  // Policy defines the policy for selecting which events should be sent to the webhook required
  policy : Policy
  // Webhook to send events required
  webhook : Webhook
}
