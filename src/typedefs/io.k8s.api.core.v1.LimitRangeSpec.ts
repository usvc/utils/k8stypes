import { LimitRangeItem } from './io.k8s.api.core.v1.LimitRangeItem';
export const Version : string = "core/v1";

// LimitRangeSpec defines a min/max usage limit for resources that match on kind.
export class LimitRangeSpec {
  // Limits is the list of LimitRangeItem objects that are enforced.
  limits : LimitRangeItem[]
}
