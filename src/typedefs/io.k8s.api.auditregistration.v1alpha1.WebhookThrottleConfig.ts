export const Version : string = "auditregistration/v1alpha1";

// WebhookThrottleConfig holds the configuration for throttling events
export class WebhookThrottleConfig {
  // ThrottleBurst is the maximum number of events sent at the same moment default 15 QPS
  burst : number
  // ThrottleQPS maximum number of batches per second default 10 QPS
  qps : number
}
