import { PolicyRule } from './io.k8s.api.rbac.v1.PolicyRule';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "rbac/v1";

// Role is a namespaced, logical grouping of PolicyRules that can be referenced as a unit by a RoleBinding.
export class Role {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object's metadata.
  metadata : ObjectMeta
  // Rules holds all the PolicyRules for this Role
  rules : PolicyRule[]
}
