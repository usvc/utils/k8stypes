export const Version : string = "authorization/v1";

// NonResourceAttributes includes the authorization attributes available for non-resource requests to the Authorizer interface
export class NonResourceAttributes {
  // Path is the URL path of the request
  path : string
  // Verb is the standard HTTP verb
  verb : string
}
