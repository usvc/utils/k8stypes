export const Version : string = "apiextensions/v1beta1";

// CustomResourceDefinitionNames indicates the names to serve this CustomResourceDefinition
export class CustomResourceDefinitionNames {
  // Categories is a list of grouped resources custom resources belong to (e.g. 'all')
  categories : string[]
  // Kind is the serialized kind of the resource.  It is normally CamelCase and singular.
  kind : string
  // ListKind is the serialized kind of the list for this resource.  Defaults to <kind>List.
  listKind : string
  // Plural is the plural name of the resource to serve.  It must match the name of the CustomResourceDefinition-registration too: plural.group and it must be all lowercase.
  plural : string
  // ShortNames are short names for the resource.  It must be all lowercase.
  shortNames : string[]
  // Singular is the singular name of the resource.  It must be all lowercase  Defaults to lowercased <kind>
  singular : string
}
