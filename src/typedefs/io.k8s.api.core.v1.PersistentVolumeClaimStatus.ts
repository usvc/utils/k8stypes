import { PersistentVolumeClaimCondition } from './io.k8s.api.core.v1.PersistentVolumeClaimCondition';
export const Version : string = "core/v1";

// PersistentVolumeClaimStatus is the current status of a persistent volume claim.
export class PersistentVolumeClaimStatus {
  // AccessModes contains the actual access modes the volume backing the PVC has. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes-1
  accessModes : string[]
  // Represents the actual resources of the underlying volume.
  capacity : object
  // Current Condition of persistent volume claim. If underlying persistent volume is being resized then the Condition will be set to 'ResizeStarted'.
  conditions : PersistentVolumeClaimCondition[]
  // Phase represents the current phase of PersistentVolumeClaim.
  phase : string
}
