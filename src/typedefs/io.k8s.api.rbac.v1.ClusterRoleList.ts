import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { ClusterRole } from './io.k8s.api.rbac.v1.ClusterRole';
export const Version : string = "rbac/v1";

// ClusterRoleList is a collection of ClusterRoles
export class ClusterRoleList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Items is a list of ClusterRoles
  items : ClusterRole[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object's metadata.
  metadata : ListMeta
}
