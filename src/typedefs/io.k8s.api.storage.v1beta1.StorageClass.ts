import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
import { TopologySelectorTerm } from './io.k8s.api.core.v1.TopologySelectorTerm';
export const Version : string = "storage/v1beta1";

// StorageClass describes the parameters for a class of storage for which PersistentVolumes can be dynamically provisioned.  StorageClasses are non-namespaced; the name of the storage class according to etcd is in ObjectMeta.Name.
export class StorageClass {
  // AllowVolumeExpansion shows whether the storage class allow volume expand
  allowVolumeExpansion : boolean
  // Restrict the node topologies where volumes can be dynamically provisioned. Each volume plugin defines its own supported topology specifications. An empty TopologySelectorTerm list means there is no topology restriction. This field is only honored by servers that enable the VolumeScheduling feature.
  allowedTopologies : TopologySelectorTerm[]
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ObjectMeta
  // Dynamically provisioned PersistentVolumes of this storage class are created with these mountOptions, e.g. ["ro", "soft"]. Not validated - mount of the PVs will simply fail if one is invalid.
  mountOptions : string[]
  // Parameters holds the parameters for the provisioner that should create volumes of this storage class.
  parameters : object
  // Provisioner indicates the type of the provisioner.
  provisioner : string
  // Dynamically provisioned PersistentVolumes of this storage class are created with this reclaimPolicy. Defaults to Delete.
  reclaimPolicy : string
  // VolumeBindingMode indicates how PersistentVolumeClaims should be provisioned and bound.  When unset, VolumeBindingImmediate is used. This field is only honored by servers that enable the VolumeScheduling feature.
  volumeBindingMode : string
}
