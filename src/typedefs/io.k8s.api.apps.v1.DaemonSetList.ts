import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { DaemonSet } from './io.k8s.api.apps.v1.DaemonSet';
export const Version : string = "apps/v1";

// DaemonSetList is a collection of daemon sets.
export class DaemonSetList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // A list of daemon sets.
  items : DaemonSet[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard list metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ListMeta
}
