export const Version : string = "core/v1";

// Describe a container image
export class ContainerImage {
  // Names by which this image is known. e.g. ["k8s.gcr.io/hyperkube:v1.0.7", "dockerhub.io/google_containers/hyperkube:v1.0.7"]
  names : string[]
  // The size of the image in bytes.
  sizeBytes : number
}
