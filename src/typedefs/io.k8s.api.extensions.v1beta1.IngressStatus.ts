import { LoadBalancerStatus } from './io.k8s.api.core.v1.LoadBalancerStatus';
export const Version : string = "extensions/v1beta1";

// IngressStatus describe the current state of the Ingress.
export class IngressStatus {
  // LoadBalancer contains the current status of the load-balancer.
  loadBalancer : LoadBalancerStatus
}
