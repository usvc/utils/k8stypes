import { JSONSchemaProps } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaProps';
export const Version : string = "apiextensions/v1beta1";

// CustomResourceValidation is a list of validation methods for CustomResources.
export class CustomResourceValidation {
  // OpenAPIV3Schema is the OpenAPI v3 schema to be validated against.
  openAPIV3Schema : JSONSchemaProps
}
