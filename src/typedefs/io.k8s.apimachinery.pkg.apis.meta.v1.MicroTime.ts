export const Version : string = "meta/v1";
// MicroTime is version of Time with microsecond level precision.
export type MicroTime = string;
