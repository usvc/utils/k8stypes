import { ScopeSelector } from './io.k8s.api.core.v1.ScopeSelector';
export const Version : string = "core/v1";

// ResourceQuotaSpec defines the desired hard limits to enforce for Quota.
export class ResourceQuotaSpec {
  // hard is the set of desired hard limits for each named resource. More info: https://kubernetes.io/docs/concepts/policy/resource-quotas/
  hard : object
  // scopeSelector is also a collection of filters like scopes that must match each object tracked by a quota but expressed using ScopeSelectorOperator in combination with possible values. For a resource to match, both scopes AND scopeSelector (if specified in spec), must be matched.
  scopeSelector : ScopeSelector
  // A collection of filters that must match each object tracked by a quota. If not specified, the quota matches all objects.
  scopes : string[]
}
