import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
import { CrossVersionObjectReference } from './io.k8s.api.autoscaling.v2beta2.CrossVersionObjectReference';
import { MetricValueStatus } from './io.k8s.api.autoscaling.v2beta2.MetricValueStatus';
export const Version : string = "autoscaling/v2beta2";

// ObjectMetricStatus indicates the current value of a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
export class ObjectMetricStatus {
  // current contains the current value for the given metric
  current : MetricValueStatus
  // description not available
  describedObject : CrossVersionObjectReference
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
}
