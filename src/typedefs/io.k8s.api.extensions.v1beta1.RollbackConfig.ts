export const Version : string = "extensions/v1beta1";

// DEPRECATED.
export class RollbackConfig {
  // The revision to rollback to. If set to 0, rollback to the last revision.
  revision : number
}
