export const Version : string = "core/v1";

// HostAlias holds the mapping between IP and hostnames that will be injected as an entry in the pod's hosts file.
export class HostAlias {
  // Hostnames for the above IP address.
  hostnames : string[]
  // IP address of the host file entry.
  ip : string
}
