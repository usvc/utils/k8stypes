export const Version : string = "apps/v1";

// RollingUpdateStatefulSetStrategy is used to communicate parameter for RollingUpdateStatefulSetStrategyType.
export class RollingUpdateStatefulSetStrategy {
  // Partition indicates the ordinal at which the StatefulSet should be partitioned. Default value is 0.
  partition : number
}
