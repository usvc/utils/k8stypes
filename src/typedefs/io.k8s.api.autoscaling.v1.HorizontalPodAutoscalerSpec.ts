import { CrossVersionObjectReference } from './io.k8s.api.autoscaling.v1.CrossVersionObjectReference';
export const Version : string = "autoscaling/v1";

// specification of a horizontal pod autoscaler.
export class HorizontalPodAutoscalerSpec {
  // upper limit for the number of pods that can be set by the autoscaler; cannot be smaller than MinReplicas.
  maxReplicas : number
  // lower limit for the number of pods that can be set by the autoscaler, default 1.
  minReplicas : number
  // reference to scaled resource; horizontal pod autoscaler will learn the current resource consumption and will set the desired number of pods by using its Scale subresource.
  scaleTargetRef : CrossVersionObjectReference
  // target average CPU utilization (represented as a percentage of requested CPU) over all the pods; if not specified the default autoscaling policy will be used.
  targetCPUUtilizationPercentage : number
}
