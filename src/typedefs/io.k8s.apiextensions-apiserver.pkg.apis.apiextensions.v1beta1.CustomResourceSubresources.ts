import { CustomResourceSubresourceStatus } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresourceStatus';
import { CustomResourceSubresourceScale } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceSubresourceScale';
export const Version : string = "apiextensions/v1beta1";

// CustomResourceSubresources defines the status and scale subresources for CustomResources.
export class CustomResourceSubresources {
  // Scale denotes the scale subresource for CustomResources
  scale : CustomResourceSubresourceScale
  // Status denotes the status subresource for CustomResources
  status : CustomResourceSubresourceStatus
}
