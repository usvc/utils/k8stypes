export const Version : string = "apiextensions/v1beta1";
// JSON represents any valid JSON value. These types are supported: bool, int64, float64, string, []interface{}, map[string]interface{} and nil.
export type JSON = any;
