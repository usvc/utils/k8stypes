import { RollingUpdateDeployment } from './io.k8s.api.apps.v1.RollingUpdateDeployment';
export const Version : string = "apps/v1";

// DeploymentStrategy describes how to replace existing pods with new ones.
export class DeploymentStrategy {
  // Rolling update config params. Present only if DeploymentStrategyType = RollingUpdate.
  rollingUpdate : RollingUpdateDeployment
  // Type of deployment. Can be "Recreate" or "RollingUpdate". Default is RollingUpdate.
  type : string
}
