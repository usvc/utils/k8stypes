export const Version : string = "apiextensions/v1beta1";
// JSONSchemaPropsOrStringArray represents a JSONSchemaProps or a string array.
export type JSONSchemaPropsOrStringArray = any;
