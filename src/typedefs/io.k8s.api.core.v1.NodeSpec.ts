import { Taint } from './io.k8s.api.core.v1.Taint';
import { NodeConfigSource } from './io.k8s.api.core.v1.NodeConfigSource';
export const Version : string = "core/v1";

// NodeSpec describes the attributes that a node is created with.
export class NodeSpec {
  // If specified, the source to get node configuration from The DynamicKubeletConfig feature gate must be enabled for the Kubelet to use this field
  configSource : NodeConfigSource
  // Deprecated. Not all kubelets will set this field. Remove field after 1.13. see: https://issues.k8s.io/61966
  externalID : string
  // PodCIDR represents the pod IP range assigned to the node.
  podCIDR : string
  // ID of the node assigned by the cloud provider in the format: <ProviderName>://<ProviderSpecificNodeID>
  providerID : string
  // If specified, the node's taints.
  taints : Taint[]
  // Unschedulable controls node schedulability of new pods. By default, node is schedulable. More info: https://kubernetes.io/docs/concepts/nodes/node/#manual-node-administration
  unschedulable : boolean
}
