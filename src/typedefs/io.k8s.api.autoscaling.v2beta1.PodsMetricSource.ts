import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
export const Version : string = "autoscaling/v2beta1";

// PodsMetricSource indicates how to scale on a metric describing each pod in the current scale target (for example, transactions-processed-per-second). The values will be averaged together before being compared to the target value.
export class PodsMetricSource {
  // metricName is the name of the metric in question
  metricName : string
  // selector is the string-encoded form of a standard kubernetes label selector for the given metric When set, it is passed as an additional parameter to the metrics server for more specific metrics scoping When unset, just the metricName will be used to gather metrics.
  selector : LabelSelector
  // targetAverageValue is the target value of the average of the metric across all relevant pods (as a quantity)
  targetAverageValue : Quantity
}
