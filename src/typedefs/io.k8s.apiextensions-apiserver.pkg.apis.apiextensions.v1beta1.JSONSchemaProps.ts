import { JSONSchemaPropsOrArray } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrArray';
import { ExternalDocumentation } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.ExternalDocumentation';
import { JSON } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSON';
import { JSONSchemaPropsOrBool } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.JSONSchemaPropsOrBool';
export const Version : string = "apiextensions/v1beta1";

// JSONSchemaProps is a JSON-Schema following Specification Draft 4 (http://json-schema.org/).
export class JSONSchemaProps {
  // description not available
  $ref : string
  // description not available
  $schema : string
  // description not available
  additionalItems : JSONSchemaPropsOrBool
  // description not available
  additionalProperties : JSONSchemaPropsOrBool
  // description not available
  allOf : JSONSchemaProps[]
  // description not available
  anyOf : JSONSchemaProps[]
  // default is a default value for undefined object fields. Defaulting is an alpha feature under the CustomResourceDefaulting feature gate. Defaulting requires spec.preserveUnknownFields to be false.
  default : JSON
  // description not available
  definitions : object
  // description not available
  dependencies : object
  // description not available
  description : string
  // description not available
  enum : JSON[]
  // description not available
  example : JSON
  // description not available
  exclusiveMaximum : boolean
  // description not available
  exclusiveMinimum : boolean
  // description not available
  externalDocs : ExternalDocumentation
  // description not available
  format : string
  // description not available
  id : string
  // description not available
  items : JSONSchemaPropsOrArray
  // description not available
  maxItems : number
  // description not available
  maxLength : number
  // description not available
  maxProperties : number
  // description not available
  maximum : number
  // description not available
  minItems : number
  // description not available
  minLength : number
  // description not available
  minProperties : number
  // description not available
  minimum : number
  // description not available
  multipleOf : number
  // description not available
  not : JSONSchemaProps
  // description not available
  nullable : boolean
  // description not available
  oneOf : JSONSchemaProps[]
  // description not available
  pattern : string
  // description not available
  patternProperties : object
  // description not available
  properties : object
  // description not available
  required : string[]
  // description not available
  title : string
  // description not available
  type : string
  // description not available
  uniqueItems : boolean
  // x-kubernetes-embedded-resource defines that the value is an embedded Kubernetes runtime.Object, with TypeMeta and ObjectMeta. The type must be object. It is allowed to further restrict the embedded object. kind, apiVersion and metadata are validated automatically. x-kubernetes-preserve-unknown-fields is allowed to be true, but does not have to be if the object is fully specified (up to kind, apiVersion, metadata).
  "x-kubernetes-embedded-resource" : boolean
  // x-kubernetes-int-or-string specifies that this value is either an integer or a string. If this is true, an empty type is allowed and type as child of anyOf is permitted if following one of the following patterns:  1) anyOf:    - type: integer    - type: string 2) allOf:    - anyOf:      - type: integer      - type: string    - ... zero or more
  "x-kubernetes-int-or-string" : boolean
  // x-kubernetes-preserve-unknown-fields stops the API server decoding step from pruning fields which are not specified in the validation schema. This affects fields recursively, but switches back to normal pruning behaviour if nested properties or additionalProperties are specified in the schema. This can either be true or undefined. False is forbidden.
  "x-kubernetes-preserve-unknown-fields" : boolean
}
