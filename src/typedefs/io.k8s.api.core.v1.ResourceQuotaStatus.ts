export const Version : string = "core/v1";

// ResourceQuotaStatus defines the enforced hard limits and observed use.
export class ResourceQuotaStatus {
  // Hard is the set of enforced hard limits for each named resource. More info: https://kubernetes.io/docs/concepts/policy/resource-quotas/
  hard : object
  // Used is the current observed total usage of the resource in the namespace.
  used : object
}
