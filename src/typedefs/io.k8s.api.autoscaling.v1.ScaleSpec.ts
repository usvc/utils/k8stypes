export const Version : string = "autoscaling/v1";

// ScaleSpec describes the attributes of a scale subresource.
export class ScaleSpec {
  // desired number of instances for the scaled object.
  replicas : number
}
