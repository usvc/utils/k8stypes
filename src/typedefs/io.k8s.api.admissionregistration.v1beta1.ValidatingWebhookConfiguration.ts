import { ValidatingWebhook } from './io.k8s.api.admissionregistration.v1beta1.ValidatingWebhook';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "admissionregistration/v1beta1";

// ValidatingWebhookConfiguration describes the configuration of and admission webhook that accept or reject and object without changing it.
export class ValidatingWebhookConfiguration {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object metadata; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata.
  metadata : ObjectMeta
  // Webhooks is a list of webhooks and the affected resources and operations.
  webhooks : ValidatingWebhook[]
}
