export const Version : string = "apiextensions/v1beta1";
// JSONSchemaPropsOrArray represents a value that can either be a JSONSchemaProps or an array of JSONSchemaProps. Mainly here for serialization purposes.
export type JSONSchemaPropsOrArray = any;
