import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { HorizontalPodAutoscaler } from './io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscaler';
export const Version : string = "autoscaling/v2beta1";

// HorizontalPodAutoscaler is a list of horizontal pod autoscaler objects.
export class HorizontalPodAutoscalerList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // items is the list of horizontal pod autoscaler objects.
  items : HorizontalPodAutoscaler[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // metadata is the standard list metadata.
  metadata : ListMeta
}
