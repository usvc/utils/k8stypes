import { ContainerStateWaiting } from './io.k8s.api.core.v1.ContainerStateWaiting';
import { ContainerStateTerminated } from './io.k8s.api.core.v1.ContainerStateTerminated';
import { ContainerStateRunning } from './io.k8s.api.core.v1.ContainerStateRunning';
export const Version : string = "core/v1";

// ContainerState holds a possible state of container. Only one of its members may be specified. If none of them is specified, the default one is ContainerStateWaiting.
export class ContainerState {
  // Details about a running container
  running : ContainerStateRunning
  // Details about a terminated container
  terminated : ContainerStateTerminated
  // Details about a waiting container
  waiting : ContainerStateWaiting
}
