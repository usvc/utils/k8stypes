import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { APIService } from './io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIService';
export const Version : string = "apiregistration/v1";

// APIServiceList is a list of APIService objects.
export class APIServiceList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // description not available
  items : APIService[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ListMeta
}
