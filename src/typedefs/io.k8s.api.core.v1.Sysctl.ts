export const Version : string = "core/v1";

// Sysctl defines a kernel parameter to be set
export class Sysctl {
  // Name of a property to set
  name : string
  // Value of a property to set
  value : string
}
