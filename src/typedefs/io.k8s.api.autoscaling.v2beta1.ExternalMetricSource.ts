import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
export const Version : string = "autoscaling/v2beta1";

// ExternalMetricSource indicates how to scale on a metric not associated with any Kubernetes object (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster). Exactly one "target" type should be set.
export class ExternalMetricSource {
  // metricName is the name of the metric in question.
  metricName : string
  // metricSelector is used to identify a specific time series within a given metric.
  metricSelector : LabelSelector
  // targetAverageValue is the target per-pod value of global metric (as a quantity). Mutually exclusive with TargetValue.
  targetAverageValue : Quantity
  // targetValue is the target value of the metric (as a quantity). Mutually exclusive with TargetAverageValue.
  targetValue : Quantity
}
