import { AuditSinkSpec } from './io.k8s.api.auditregistration.v1alpha1.AuditSinkSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "auditregistration/v1alpha1";

// AuditSink represents a cluster level audit sink
export class AuditSink {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec defines the audit configuration spec
  spec : AuditSinkSpec
}
