export const Version : string = "core/v1";

// EndpointPort is a tuple that describes a single port.
export class EndpointPort {
  // The name of this port (corresponds to ServicePort.Name). Must be a DNS_LABEL. Optional only if one port is defined.
  name : string
  // The port number of the endpoint.
  port : number
  // The IP protocol for this port. Must be UDP, TCP, or SCTP. Default is TCP.
  protocol : string
}
