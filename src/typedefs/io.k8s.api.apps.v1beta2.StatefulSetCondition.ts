import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "apps/v1beta2";

// StatefulSetCondition describes the state of a statefulset at a certain point.
export class StatefulSetCondition {
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // A human readable message indicating details about the transition.
  message : string
  // The reason for the condition's last transition.
  reason : string
  // Status of the condition, one of True, False, Unknown.
  status : string
  // Type of statefulset condition.
  type : string
}
