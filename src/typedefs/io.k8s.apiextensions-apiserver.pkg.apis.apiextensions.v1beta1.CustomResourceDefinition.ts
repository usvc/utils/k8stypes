import { CustomResourceDefinitionStatus } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionStatus';
import { CustomResourceDefinitionSpec } from './io.k8s.apiextensions-apiserver.pkg.apis.apiextensions.v1beta1.CustomResourceDefinitionSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "apiextensions/v1beta1";

// CustomResourceDefinition represents a resource that should be exposed on the API server.  Its name MUST be in the format <.spec.name>.<.spec.group>.
export class CustomResourceDefinition {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec describes how the user wants the resources to appear
  spec : CustomResourceDefinitionSpec
  // Status indicates the actual state of the CustomResourceDefinition
  status : CustomResourceDefinitionStatus
}
