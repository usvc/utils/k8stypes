import { TCPSocketAction } from './io.k8s.api.core.v1.TCPSocketAction';
import { HTTPGetAction } from './io.k8s.api.core.v1.HTTPGetAction';
import { ExecAction } from './io.k8s.api.core.v1.ExecAction';
export const Version : string = "core/v1";

// Handler defines a specific action that should be taken
export class Handler {
  // One and only one of the following should be specified. Exec specifies the action to take.
  exec : ExecAction
  // HTTPGet specifies the http request to perform.
  httpGet : HTTPGetAction
  // TCPSocket specifies an action involving a TCP port. TCP hooks not yet supported
  tcpSocket : TCPSocketAction
}
