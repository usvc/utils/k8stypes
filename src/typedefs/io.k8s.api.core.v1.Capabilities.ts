export const Version : string = "core/v1";

// Adds and removes POSIX capabilities from running containers.
export class Capabilities {
  // Added capabilities
  add : string[]
  // Removed capabilities
  drop : string[]
}
