import { PodAntiAffinity } from './io.k8s.api.core.v1.PodAntiAffinity';
import { PodAffinity } from './io.k8s.api.core.v1.PodAffinity';
import { NodeAffinity } from './io.k8s.api.core.v1.NodeAffinity';
export const Version : string = "core/v1";

// Affinity is a group of affinity scheduling rules.
export class Affinity {
  // Describes node affinity scheduling rules for the pod.
  nodeAffinity : NodeAffinity
  // Describes pod affinity scheduling rules (e.g. co-locate this pod in the same node, zone, etc. as some other pod(s)).
  podAffinity : PodAffinity
  // Describes pod anti-affinity scheduling rules (e.g. avoid putting this pod in the same node, zone, etc. as some other pod(s)).
  podAntiAffinity : PodAntiAffinity
}
