import { MetricTarget } from './io.k8s.api.autoscaling.v2beta2.MetricTarget';
import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
export const Version : string = "autoscaling/v2beta2";

// ExternalMetricSource indicates how to scale on a metric not associated with any Kubernetes object (for example length of queue in cloud messaging service, or QPS from loadbalancer running outside of cluster).
export class ExternalMetricSource {
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
  // target specifies the target value for the given metric
  target : MetricTarget
}
