import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
import { MetricValueStatus } from './io.k8s.api.autoscaling.v2beta2.MetricValueStatus';
export const Version : string = "autoscaling/v2beta2";

// ExternalMetricStatus indicates the current value of a global metric not associated with any Kubernetes object.
export class ExternalMetricStatus {
  // current contains the current value for the given metric
  current : MetricValueStatus
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
}
