import { ClientIPConfig } from './io.k8s.api.core.v1.ClientIPConfig';
export const Version : string = "core/v1";

// SessionAffinityConfig represents the configurations of session affinity.
export class SessionAffinityConfig {
  // clientIP contains the configurations of Client IP based session affinity.
  clientIP : ClientIPConfig
}
