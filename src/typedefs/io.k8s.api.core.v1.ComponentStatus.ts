import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
import { ComponentCondition } from './io.k8s.api.core.v1.ComponentCondition';
export const Version : string = "core/v1";

// ComponentStatus (and ComponentStatusList) holds the cluster validation info.
export class ComponentStatus {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // List of component conditions observed
  conditions : ComponentCondition[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object's metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ObjectMeta
}
