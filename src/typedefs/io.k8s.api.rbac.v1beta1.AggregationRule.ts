import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
export const Version : string = "rbac/v1beta1";

// AggregationRule describes how to locate ClusterRoles to aggregate into the ClusterRole
export class AggregationRule {
  // ClusterRoleSelectors holds a list of selectors which will be used to find ClusterRoles and create the rules. If any of the selectors match, then the ClusterRole's permissions will be added
  clusterRoleSelectors : LabelSelector[]
}
