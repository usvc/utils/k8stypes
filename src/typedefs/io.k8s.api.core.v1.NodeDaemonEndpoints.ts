import { DaemonEndpoint } from './io.k8s.api.core.v1.DaemonEndpoint';
export const Version : string = "core/v1";

// NodeDaemonEndpoints lists ports opened by daemons running on the Node.
export class NodeDaemonEndpoints {
  // Endpoint on which Kubelet is listening.
  kubeletEndpoint : DaemonEndpoint
}
