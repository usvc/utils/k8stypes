import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { ResourceRequirements } from './io.k8s.api.core.v1.ResourceRequirements';
import { TypedLocalObjectReference } from './io.k8s.api.core.v1.TypedLocalObjectReference';
export const Version : string = "core/v1";

// PersistentVolumeClaimSpec describes the common attributes of storage devices and allows a Source for provider-specific attributes
export class PersistentVolumeClaimSpec {
  // AccessModes contains the desired access modes the volume should have. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes-1
  accessModes : string[]
  // This field requires the VolumeSnapshotDataSource alpha feature gate to be enabled and currently VolumeSnapshot is the only supported data source. If the provisioner can support VolumeSnapshot data source, it will create a new volume and data will be restored to the volume at the same time. If the provisioner does not support VolumeSnapshot data source, volume will not be created and the failure will be reported as an event. In the future, we plan to support more data source types and the behavior of the provisioner may change.
  dataSource : TypedLocalObjectReference
  // Resources represents the minimum resources the volume should have. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#resources
  resources : ResourceRequirements
  // A label query over volumes to consider for binding.
  selector : LabelSelector
  // Name of the StorageClass required by the claim. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#class-1
  storageClassName : string
  // volumeMode defines what type of volume is required by the claim. Value of Filesystem is implied when not included in claim spec. This is a beta feature.
  volumeMode : string
  // VolumeName is the binding reference to the PersistentVolume backing this claim.
  volumeName : string
}
