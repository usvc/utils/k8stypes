import { CrossVersionObjectReference } from './io.k8s.api.autoscaling.v2beta2.CrossVersionObjectReference';
import { MetricSpec } from './io.k8s.api.autoscaling.v2beta2.MetricSpec';
export const Version : string = "autoscaling/v2beta2";

// HorizontalPodAutoscalerSpec describes the desired functionality of the HorizontalPodAutoscaler.
export class HorizontalPodAutoscalerSpec {
  // maxReplicas is the upper limit for the number of replicas to which the autoscaler can scale up. It cannot be less that minReplicas.
  maxReplicas : number
  // metrics contains the specifications for which to use to calculate the desired replica count (the maximum replica count across all metrics will be used).  The desired replica count is calculated multiplying the ratio between the target value and the current value by the current number of pods.  Ergo, metrics used must decrease as the pod count is increased, and vice-versa.  See the individual metric source types for more information about how each type of metric must respond. If not set, the default metric will be set to 80% average CPU utilization.
  metrics : MetricSpec[]
  // minReplicas is the lower limit for the number of replicas to which the autoscaler can scale down. It defaults to 1 pod.
  minReplicas : number
  // scaleTargetRef points to the target resource to scale, and is used to the pods for which metrics should be collected, as well as to actually change the replica count.
  scaleTargetRef : CrossVersionObjectReference
}
