import { CertificateSigningRequestCondition } from './io.k8s.api.certificates.v1beta1.CertificateSigningRequestCondition';
export const Version : string = "certificates/v1beta1";

// description not available
export class CertificateSigningRequestStatus {
  // If request was approved, the controller will place the issued certificate here.
  certificate : string
  // Conditions applied to the request, such as approval or denial.
  conditions : CertificateSigningRequestCondition[]
}
