import { HTTPIngressPath } from './io.k8s.api.networking.v1beta1.HTTPIngressPath';
export const Version : string = "networking/v1beta1";

// HTTPIngressRuleValue is a list of http selectors pointing to backends. In the example: http://<host>/<path>?<searchpart> -> backend where where parts of the url correspond to RFC 3986, this resource will be used to match against everything after the last '/' and before the first '?' or '#'.
export class HTTPIngressRuleValue {
  // A collection of paths that map requests to backends.
  paths : HTTPIngressPath[]
}
