export const Version : string = "core/v1";

// LimitRangeItem defines a min/max usage limit for any resource that matches on kind.
export class LimitRangeItem {
  // Default resource requirement limit value by resource name if resource limit is omitted.
  default : object
  // DefaultRequest is the default resource requirement request value by resource name if resource request is omitted.
  defaultRequest : object
  // Max usage constraints on this kind by resource name.
  max : object
  // MaxLimitRequestRatio if specified, the named resource must have a request and limit that are both non-zero where limit divided by request is less than or equal to the enumerated value; this represents the max burst for the named resource.
  maxLimitRequestRatio : object
  // Min usage constraints on this kind by resource name.
  min : object
  // Type of resource that this limit applies to.
  type : string
}
