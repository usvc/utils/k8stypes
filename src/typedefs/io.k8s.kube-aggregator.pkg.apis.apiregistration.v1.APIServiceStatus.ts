import { APIServiceCondition } from './io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceCondition';
export const Version : string = "apiregistration/v1";

// APIServiceStatus contains derived information about an API server
export class APIServiceStatus {
  // Current service state of apiService.
  conditions : APIServiceCondition[]
}
