export const Version : string = "core/v1";

// EventSource contains information for an event.
export class EventSource {
  // Component from which the event is generated.
  component : string
  // Node name on which the event is generated.
  host : string
}
