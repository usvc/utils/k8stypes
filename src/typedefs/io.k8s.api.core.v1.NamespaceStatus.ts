export const Version : string = "core/v1";

// NamespaceStatus is information about the current status of a Namespace.
export class NamespaceStatus {
  // Phase is the current lifecycle phase of the namespace. More info: https://kubernetes.io/docs/tasks/administer-cluster/namespaces/
  phase : string
}
