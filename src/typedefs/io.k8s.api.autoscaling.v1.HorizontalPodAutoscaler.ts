import { HorizontalPodAutoscalerStatus } from './io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerStatus';
import { HorizontalPodAutoscalerSpec } from './io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "autoscaling/v1";

// configuration of a horizontal pod autoscaler.
export class HorizontalPodAutoscaler {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object metadata. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#metadata
  metadata : ObjectMeta
  // behaviour of autoscaler. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#spec-and-status.
  spec : HorizontalPodAutoscalerSpec
  // current information about the autoscaler.
  status : HorizontalPodAutoscalerStatus
}
