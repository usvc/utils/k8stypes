export const Version : string = "autoscaling/v2beta1";

// CrossVersionObjectReference contains enough information to let you identify the referred resource.
export class CrossVersionObjectReference {
  // API version of the referent
  apiVersion : string
  // Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
  kind : string
  // Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
  name : string
}
