import { RollbackConfig } from './io.k8s.api.apps.v1beta1.RollbackConfig';
export const Version : string = "apps/v1beta1";

// DEPRECATED. DeploymentRollback stores the information required to rollback a deployment.
export class DeploymentRollback {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Required: This must match the Name of a deployment.
  name : string
  // The config of this deployment rollback.
  rollbackTo : RollbackConfig
  // The annotations to be updated to a deployment
  updatedAnnotations : object
}
