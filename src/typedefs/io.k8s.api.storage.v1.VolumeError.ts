import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "storage/v1";

// VolumeError captures an error encountered during a volume operation.
export class VolumeError {
  // String detailing the error encountered during Attach or Detach operation. This string may be logged, so it should not contain sensitive information.
  message : string
  // Time the error was encountered.
  time : Time
}
