import { VolumeAttachmentSource } from './io.k8s.api.storage.v1.VolumeAttachmentSource';
export const Version : string = "storage/v1";

// VolumeAttachmentSpec is the specification of a VolumeAttachment request.
export class VolumeAttachmentSpec {
  // Attacher indicates the name of the volume driver that MUST handle this request. This is the name returned by GetPluginName().
  attacher : string
  // The node that the volume should be attached to.
  nodeName : string
  // Source represents the volume that should be attached.
  source : VolumeAttachmentSource
}
