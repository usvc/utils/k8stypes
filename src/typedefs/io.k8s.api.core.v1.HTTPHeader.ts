export const Version : string = "core/v1";

// HTTPHeader describes a custom header to be used in HTTP probes
export class HTTPHeader {
  // The header field name
  name : string
  // The header field value
  value : string
}
