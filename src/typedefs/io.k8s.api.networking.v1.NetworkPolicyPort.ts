import { IntOrString } from './io.k8s.apimachinery.pkg.util.intstr.IntOrString';
export const Version : string = "networking/v1";

// NetworkPolicyPort describes a port to allow traffic on
export class NetworkPolicyPort {
  // The port on the given protocol. This can either be a numerical or named port on a pod. If this field is not provided, this matches all port names and numbers.
  port : IntOrString
  // The protocol (TCP, UDP, or SCTP) which traffic must match. If not specified, this field defaults to TCP.
  protocol : string
}
