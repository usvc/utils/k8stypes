export const Version : string = "core/v1";

// PodReadinessGate contains the reference to a pod condition
export class PodReadinessGate {
  // ConditionType refers to a condition in the pod's condition list with matching type.
  conditionType : string
}
