import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
export const Version : string = "core/v1";

// ResourceFieldSelector represents container resources (cpu, memory) and their output format
export class ResourceFieldSelector {
  // Container name: required for volumes, optional for env vars
  containerName : string
  // Specifies the output format of the exposed resources, defaults to "1"
  divisor : Quantity
  // Required: resource to select
  resource : string
}
