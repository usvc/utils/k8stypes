export const Version : string = "apps/v1beta2";

// ScaleSpec describes the attributes of a scale subresource
export class ScaleSpec {
  // desired number of instances for the scaled object.
  replicas : number
}
