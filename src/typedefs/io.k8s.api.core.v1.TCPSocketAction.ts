import { IntOrString } from './io.k8s.apimachinery.pkg.util.intstr.IntOrString';
export const Version : string = "core/v1";

// TCPSocketAction describes an action based on opening a socket
export class TCPSocketAction {
  // Optional: Host name to connect to, defaults to the pod IP.
  host : string
  // Number or name of the port to access on the container. Number must be in the range 1 to 65535. Name must be an IANA_SVC_NAME.
  port : IntOrString
}
