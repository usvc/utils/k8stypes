export const Version : string = "core/v1";

// LoadBalancerIngress represents the status of a load-balancer ingress point: traffic intended for the service should be sent to an ingress point.
export class LoadBalancerIngress {
  // Hostname is set for load-balancer ingress points that are DNS based (typically AWS load-balancers)
  hostname : string
  // IP is set for load-balancer ingress points that are IP based (typically GCE or OpenStack load-balancers)
  ip : string
}
