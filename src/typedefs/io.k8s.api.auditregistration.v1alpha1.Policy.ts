export const Version : string = "auditregistration/v1alpha1";

// Policy defines the configuration of how audit events are logged
export class Policy {
  // The Level that all requests are recorded at. available options: None, Metadata, Request, RequestResponse required
  level : string
  // Stages is a list of stages for which events are created.
  stages : string[]
}
