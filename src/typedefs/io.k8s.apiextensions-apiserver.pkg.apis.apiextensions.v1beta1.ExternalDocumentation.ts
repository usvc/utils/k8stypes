export const Version : string = "apiextensions/v1beta1";

// ExternalDocumentation allows referencing an external resource for extended documentation.
export class ExternalDocumentation {
  // description not available
  description : string
  // description not available
  url : string
}
