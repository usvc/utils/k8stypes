import { ListMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ListMeta';
import { StatefulSet } from './io.k8s.api.apps.v1beta2.StatefulSet';
export const Version : string = "apps/v1beta2";

// StatefulSetList is a collection of StatefulSets.
export class StatefulSetList {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // description not available
  items : StatefulSet[]
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ListMeta
}
