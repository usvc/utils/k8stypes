export const Version : string = "core/v1";

// AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
export class AzureFileVolumeSource {
  // Defaults to false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
  readOnly : boolean
  // the name of secret that contains Azure Storage Account Name and Key
  secretName : string
  // Share Name
  shareName : string
}
