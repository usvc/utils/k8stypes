import { ServiceAccountTokenProjection } from './io.k8s.api.core.v1.ServiceAccountTokenProjection';
import { SecretProjection } from './io.k8s.api.core.v1.SecretProjection';
import { DownwardAPIProjection } from './io.k8s.api.core.v1.DownwardAPIProjection';
import { ConfigMapProjection } from './io.k8s.api.core.v1.ConfigMapProjection';
export const Version : string = "core/v1";

// Projection that may be projected along with other supported volume types
export class VolumeProjection {
  // information about the configMap data to project
  configMap : ConfigMapProjection
  // information about the downwardAPI data to project
  downwardAPI : DownwardAPIProjection
  // information about the secret data to project
  secret : SecretProjection
  // information about the serviceAccountToken data to project
  serviceAccountToken : ServiceAccountTokenProjection
}
