import { SecretKeySelector } from './io.k8s.api.core.v1.SecretKeySelector';
import { ResourceFieldSelector } from './io.k8s.api.core.v1.ResourceFieldSelector';
import { ObjectFieldSelector } from './io.k8s.api.core.v1.ObjectFieldSelector';
import { ConfigMapKeySelector } from './io.k8s.api.core.v1.ConfigMapKeySelector';
export const Version : string = "core/v1";

// EnvVarSource represents a source for the value of an EnvVar.
export class EnvVarSource {
  // Selects a key of a ConfigMap.
  configMapKeyRef : ConfigMapKeySelector
  // Selects a field of the pod: supports metadata.name, metadata.namespace, metadata.labels, metadata.annotations, spec.nodeName, spec.serviceAccountName, status.hostIP, status.podIP.
  fieldRef : ObjectFieldSelector
  // Selects a resource of the container: only resources limits and requests (limits.cpu, limits.memory, limits.ephemeral-storage, requests.cpu, requests.memory and requests.ephemeral-storage) are currently supported.
  resourceFieldRef : ResourceFieldSelector
  // Selects a key of a secret in the pod's namespace
  secretKeyRef : SecretKeySelector
}
