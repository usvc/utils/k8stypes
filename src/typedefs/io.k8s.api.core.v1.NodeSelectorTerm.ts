import { NodeSelectorRequirement } from './io.k8s.api.core.v1.NodeSelectorRequirement';
export const Version : string = "core/v1";

// A null or empty node selector term matches no objects. The requirements of them are ANDed. The TopologySelectorTerm type implements a subset of the NodeSelectorTerm.
export class NodeSelectorTerm {
  // A list of node selector requirements by node's labels.
  matchExpressions : NodeSelectorRequirement[]
  // A list of node selector requirements by node's fields.
  matchFields : NodeSelectorRequirement[]
}
