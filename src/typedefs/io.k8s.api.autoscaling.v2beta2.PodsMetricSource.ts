import { MetricTarget } from './io.k8s.api.autoscaling.v2beta2.MetricTarget';
import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
export const Version : string = "autoscaling/v2beta2";

// PodsMetricSource indicates how to scale on a metric describing each pod in the current scale target (for example, transactions-processed-per-second). The values will be averaged together before being compared to the target value.
export class PodsMetricSource {
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
  // target specifies the target value for the given metric
  target : MetricTarget
}
