import { RollingUpdateDaemonSet } from './io.k8s.api.apps.v1.RollingUpdateDaemonSet';
export const Version : string = "apps/v1";

// DaemonSetUpdateStrategy is a struct used to control the update strategy for a DaemonSet.
export class DaemonSetUpdateStrategy {
  // Rolling update config params. Present only if type = "RollingUpdate".
  rollingUpdate : RollingUpdateDaemonSet
  // Type of daemon set update. Can be "RollingUpdate" or "OnDelete". Default is RollingUpdate.
  type : string
}
