import { MetricTarget } from './io.k8s.api.autoscaling.v2beta2.MetricTarget';
import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
import { CrossVersionObjectReference } from './io.k8s.api.autoscaling.v2beta2.CrossVersionObjectReference';
export const Version : string = "autoscaling/v2beta2";

// ObjectMetricSource indicates how to scale on a metric describing a kubernetes object (for example, hits-per-second on an Ingress object).
export class ObjectMetricSource {
  // description not available
  describedObject : CrossVersionObjectReference
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
  // target specifies the target value for the given metric
  target : MetricTarget
}
