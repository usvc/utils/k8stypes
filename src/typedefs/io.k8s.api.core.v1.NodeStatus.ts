import { AttachedVolume } from './io.k8s.api.core.v1.AttachedVolume';
import { NodeSystemInfo } from './io.k8s.api.core.v1.NodeSystemInfo';
import { ContainerImage } from './io.k8s.api.core.v1.ContainerImage';
import { NodeDaemonEndpoints } from './io.k8s.api.core.v1.NodeDaemonEndpoints';
import { NodeConfigStatus } from './io.k8s.api.core.v1.NodeConfigStatus';
import { NodeCondition } from './io.k8s.api.core.v1.NodeCondition';
import { NodeAddress } from './io.k8s.api.core.v1.NodeAddress';
export const Version : string = "core/v1";

// NodeStatus is information about the current status of a node.
export class NodeStatus {
  // List of addresses reachable to the node. Queried from cloud provider, if available. More info: https://kubernetes.io/docs/concepts/nodes/node/#addresses
  addresses : NodeAddress[]
  // Allocatable represents the resources of a node that are available for scheduling. Defaults to Capacity.
  allocatable : object
  // Capacity represents the total resources of a node. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#capacity
  capacity : object
  // Conditions is an array of current observed node conditions. More info: https://kubernetes.io/docs/concepts/nodes/node/#condition
  conditions : NodeCondition[]
  // Status of the config assigned to the node via the dynamic Kubelet config feature.
  config : NodeConfigStatus
  // Endpoints of daemons running on the Node.
  daemonEndpoints : NodeDaemonEndpoints
  // List of container images on this node
  images : ContainerImage[]
  // Set of ids/uuids to uniquely identify the node. More info: https://kubernetes.io/docs/concepts/nodes/node/#info
  nodeInfo : NodeSystemInfo
  // NodePhase is the recently observed lifecycle phase of the node. More info: https://kubernetes.io/docs/concepts/nodes/node/#phase The field is never populated, and now is deprecated.
  phase : string
  // List of volumes that are attached to the node.
  volumesAttached : AttachedVolume[]
  // List of attachable volumes in use (mounted) by the node.
  volumesInUse : string[]
}
