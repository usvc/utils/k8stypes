export const Version : string = "policy/v1beta1";

// AllowedFlexVolume represents a single Flexvolume that is allowed to be used.
export class AllowedFlexVolume {
  // driver is the name of the Flexvolume driver.
  driver : string
}
