export const Version : string = "core/v1";

// Represents a vSphere volume resource.
export class VsphereVirtualDiskVolumeSource {
  // Filesystem type to mount. Must be a filesystem type supported by the host operating system. Ex. "ext4", "xfs", "ntfs". Implicitly inferred to be "ext4" if unspecified.
  fsType : string
  // Storage Policy Based Management (SPBM) profile ID associated with the StoragePolicyName.
  storagePolicyID : string
  // Storage Policy Based Management (SPBM) profile name.
  storagePolicyName : string
  // Path that identifies vSphere volume vmdk
  volumePath : string
}
