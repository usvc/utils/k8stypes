export const Version : string = "policy/v1beta1";

// AllowedCSIDriver represents a single inline CSI Driver that is allowed to be used.
export class AllowedCSIDriver {
  // Name is the registered name of the CSI driver
  name : string
}
