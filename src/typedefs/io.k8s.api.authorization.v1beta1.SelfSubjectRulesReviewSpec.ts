export const Version : string = "authorization/v1beta1";

// description not available
export class SelfSubjectRulesReviewSpec {
  // Namespace to evaluate rules for. Required.
  namespace : string
}
