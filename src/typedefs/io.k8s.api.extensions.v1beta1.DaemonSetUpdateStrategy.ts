import { RollingUpdateDaemonSet } from './io.k8s.api.extensions.v1beta1.RollingUpdateDaemonSet';
export const Version : string = "extensions/v1beta1";

// description not available
export class DaemonSetUpdateStrategy {
  // Rolling update config params. Present only if type = "RollingUpdate".
  rollingUpdate : RollingUpdateDaemonSet
  // Type of daemon set update. Can be "RollingUpdate" or "OnDelete". Default is OnDelete.
  type : string
}
