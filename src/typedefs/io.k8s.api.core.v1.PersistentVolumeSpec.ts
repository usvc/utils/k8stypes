import { VsphereVirtualDiskVolumeSource } from './io.k8s.api.core.v1.VsphereVirtualDiskVolumeSource';
import { StorageOSPersistentVolumeSource } from './io.k8s.api.core.v1.StorageOSPersistentVolumeSource';
import { ScaleIOPersistentVolumeSource } from './io.k8s.api.core.v1.ScaleIOPersistentVolumeSource';
import { RBDPersistentVolumeSource } from './io.k8s.api.core.v1.RBDPersistentVolumeSource';
import { QuobyteVolumeSource } from './io.k8s.api.core.v1.QuobyteVolumeSource';
import { PortworxVolumeSource } from './io.k8s.api.core.v1.PortworxVolumeSource';
import { PhotonPersistentDiskVolumeSource } from './io.k8s.api.core.v1.PhotonPersistentDiskVolumeSource';
import { VolumeNodeAffinity } from './io.k8s.api.core.v1.VolumeNodeAffinity';
import { NFSVolumeSource } from './io.k8s.api.core.v1.NFSVolumeSource';
import { LocalVolumeSource } from './io.k8s.api.core.v1.LocalVolumeSource';
import { ISCSIPersistentVolumeSource } from './io.k8s.api.core.v1.ISCSIPersistentVolumeSource';
import { HostPathVolumeSource } from './io.k8s.api.core.v1.HostPathVolumeSource';
import { GlusterfsPersistentVolumeSource } from './io.k8s.api.core.v1.GlusterfsPersistentVolumeSource';
import { GCEPersistentDiskVolumeSource } from './io.k8s.api.core.v1.GCEPersistentDiskVolumeSource';
import { FlockerVolumeSource } from './io.k8s.api.core.v1.FlockerVolumeSource';
import { FlexPersistentVolumeSource } from './io.k8s.api.core.v1.FlexPersistentVolumeSource';
import { FCVolumeSource } from './io.k8s.api.core.v1.FCVolumeSource';
import { CSIPersistentVolumeSource } from './io.k8s.api.core.v1.CSIPersistentVolumeSource';
import { ObjectReference } from './io.k8s.api.core.v1.ObjectReference';
import { CinderPersistentVolumeSource } from './io.k8s.api.core.v1.CinderPersistentVolumeSource';
import { CephFSPersistentVolumeSource } from './io.k8s.api.core.v1.CephFSPersistentVolumeSource';
import { AzureFilePersistentVolumeSource } from './io.k8s.api.core.v1.AzureFilePersistentVolumeSource';
import { AzureDiskVolumeSource } from './io.k8s.api.core.v1.AzureDiskVolumeSource';
import { AWSElasticBlockStoreVolumeSource } from './io.k8s.api.core.v1.AWSElasticBlockStoreVolumeSource';
export const Version : string = "core/v1";

// PersistentVolumeSpec is the specification of a persistent volume.
export class PersistentVolumeSpec {
  // AccessModes contains all ways the volume can be mounted. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#access-modes
  accessModes : string[]
  // AWSElasticBlockStore represents an AWS Disk resource that is attached to a kubelet's host machine and then exposed to the pod. More info: https://kubernetes.io/docs/concepts/storage/volumes#awselasticblockstore
  awsElasticBlockStore : AWSElasticBlockStoreVolumeSource
  // AzureDisk represents an Azure Data Disk mount on the host and bind mount to the pod.
  azureDisk : AzureDiskVolumeSource
  // AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
  azureFile : AzureFilePersistentVolumeSource
  // A description of the persistent volume's resources and capacity. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#capacity
  capacity : object
  // CephFS represents a Ceph FS mount on the host that shares a pod's lifetime
  cephfs : CephFSPersistentVolumeSource
  // Cinder represents a cinder volume attached and mounted on kubelets host machine More info: https://releases.k8s.io/HEAD/examples/mysql-cinder-pd/README.md
  cinder : CinderPersistentVolumeSource
  // ClaimRef is part of a bi-directional binding between PersistentVolume and PersistentVolumeClaim. Expected to be non-nil when bound. claim.VolumeName is the authoritative bind between PV and PVC. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#binding
  claimRef : ObjectReference
  // CSI represents storage that is handled by an external CSI driver (Beta feature).
  csi : CSIPersistentVolumeSource
  // FC represents a Fibre Channel resource that is attached to a kubelet's host machine and then exposed to the pod.
  fc : FCVolumeSource
  // FlexVolume represents a generic volume resource that is provisioned/attached using an exec based plugin.
  flexVolume : FlexPersistentVolumeSource
  // Flocker represents a Flocker volume attached to a kubelet's host machine and exposed to the pod for its usage. This depends on the Flocker control service being running
  flocker : FlockerVolumeSource
  // GCEPersistentDisk represents a GCE Disk resource that is attached to a kubelet's host machine and then exposed to the pod. Provisioned by an admin. More info: https://kubernetes.io/docs/concepts/storage/volumes#gcepersistentdisk
  gcePersistentDisk : GCEPersistentDiskVolumeSource
  // Glusterfs represents a Glusterfs volume that is attached to a host and exposed to the pod. Provisioned by an admin. More info: https://releases.k8s.io/HEAD/examples/volumes/glusterfs/README.md
  glusterfs : GlusterfsPersistentVolumeSource
  // HostPath represents a directory on the host. Provisioned by a developer or tester. This is useful for single-node development and testing only! On-host storage is not supported in any way and WILL NOT WORK in a multi-node cluster. More info: https://kubernetes.io/docs/concepts/storage/volumes#hostpath
  hostPath : HostPathVolumeSource
  // ISCSI represents an ISCSI Disk resource that is attached to a kubelet's host machine and then exposed to the pod. Provisioned by an admin.
  iscsi : ISCSIPersistentVolumeSource
  // Local represents directly-attached storage with node affinity
  local : LocalVolumeSource
  // A list of mount options, e.g. ["ro", "soft"]. Not validated - mount will simply fail if one is invalid. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#mount-options
  mountOptions : string[]
  // NFS represents an NFS mount on the host. Provisioned by an admin. More info: https://kubernetes.io/docs/concepts/storage/volumes#nfs
  nfs : NFSVolumeSource
  // NodeAffinity defines constraints that limit what nodes this volume can be accessed from. This field influences the scheduling of pods that use this volume.
  nodeAffinity : VolumeNodeAffinity
  // What happens to a persistent volume when released from its claim. Valid options are Retain (default for manually created PersistentVolumes), Delete (default for dynamically provisioned PersistentVolumes), and Recycle (deprecated). Recycle must be supported by the volume plugin underlying this PersistentVolume. More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#reclaiming
  persistentVolumeReclaimPolicy : string
  // PhotonPersistentDisk represents a PhotonController persistent disk attached and mounted on kubelets host machine
  photonPersistentDisk : PhotonPersistentDiskVolumeSource
  // PortworxVolume represents a portworx volume attached and mounted on kubelets host machine
  portworxVolume : PortworxVolumeSource
  // Quobyte represents a Quobyte mount on the host that shares a pod's lifetime
  quobyte : QuobyteVolumeSource
  // RBD represents a Rados Block Device mount on the host that shares a pod's lifetime. More info: https://releases.k8s.io/HEAD/examples/volumes/rbd/README.md
  rbd : RBDPersistentVolumeSource
  // ScaleIO represents a ScaleIO persistent volume attached and mounted on Kubernetes nodes.
  scaleIO : ScaleIOPersistentVolumeSource
  // Name of StorageClass to which this persistent volume belongs. Empty value means that this volume does not belong to any StorageClass.
  storageClassName : string
  // StorageOS represents a StorageOS volume that is attached to the kubelet's host machine and mounted into the pod More info: https://releases.k8s.io/HEAD/examples/volumes/storageos/README.md
  storageos : StorageOSPersistentVolumeSource
  // volumeMode defines if a volume is intended to be used with a formatted filesystem or to remain in raw block state. Value of Filesystem is implied when not included in spec. This is a beta feature.
  volumeMode : string
  // VsphereVolume represents a vSphere volume attached and mounted on kubelets host machine
  vsphereVolume : VsphereVirtualDiskVolumeSource
}
