export const Version : string = "core/v1";

// Selects a key from a ConfigMap.
export class ConfigMapKeySelector {
  // The key to select.
  key : string
  // Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
  name : string
  // Specify whether the ConfigMap or its key must be defined
  optional : boolean
}
