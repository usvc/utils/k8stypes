export const Version : string = "extensions/v1beta1";

// describes the attributes of a scale subresource
export class ScaleSpec {
  // desired number of instances for the scaled object.
  replicas : number
}
