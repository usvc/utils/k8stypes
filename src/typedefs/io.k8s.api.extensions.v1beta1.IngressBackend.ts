import { IntOrString } from './io.k8s.apimachinery.pkg.util.intstr.IntOrString';
export const Version : string = "extensions/v1beta1";

// IngressBackend describes all endpoints for a given service and port.
export class IngressBackend {
  // Specifies the name of the referenced service.
  serviceName : string
  // Specifies the port of the referenced service.
  servicePort : IntOrString
}
