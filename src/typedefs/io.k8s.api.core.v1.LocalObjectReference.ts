export const Version : string = "core/v1";

// LocalObjectReference contains enough information to let you locate the referenced object inside the same namespace.
export class LocalObjectReference {
  // Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
  name : string
}
