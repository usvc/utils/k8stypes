import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "core/v1";

// PersistentVolumeClaimCondition contails details about state of pvc
export class PersistentVolumeClaimCondition {
  // Last time we probed the condition.
  lastProbeTime : Time
  // Last time the condition transitioned from one status to another.
  lastTransitionTime : Time
  // Human-readable message indicating details about last transition.
  message : string
  // Unique, this should be a short, machine understandable string that gives the reason for condition's last transition. If it reports "ResizeStarted" that means the underlying persistent volume is being resized.
  reason : string
  // description not available
  status : string
  // description not available
  type : string
}
