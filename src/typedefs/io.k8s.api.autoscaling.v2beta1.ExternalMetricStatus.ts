import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { Quantity } from './io.k8s.apimachinery.pkg.api.resource.Quantity';
export const Version : string = "autoscaling/v2beta1";

// ExternalMetricStatus indicates the current value of a global metric not associated with any Kubernetes object.
export class ExternalMetricStatus {
  // currentAverageValue is the current value of metric averaged over autoscaled pods.
  currentAverageValue : Quantity
  // currentValue is the current value of the metric (as a quantity)
  currentValue : Quantity
  // metricName is the name of a metric used for autoscaling in metric system.
  metricName : string
  // metricSelector is used to identify a specific time series within a given metric.
  metricSelector : LabelSelector
}
