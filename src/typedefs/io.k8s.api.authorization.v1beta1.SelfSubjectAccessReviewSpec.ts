import { ResourceAttributes } from './io.k8s.api.authorization.v1beta1.ResourceAttributes';
import { NonResourceAttributes } from './io.k8s.api.authorization.v1beta1.NonResourceAttributes';
export const Version : string = "authorization/v1beta1";

// SelfSubjectAccessReviewSpec is a description of the access request.  Exactly one of ResourceAuthorizationAttributes and NonResourceAuthorizationAttributes must be set
export class SelfSubjectAccessReviewSpec {
  // NonResourceAttributes describes information for a non-resource access request
  nonResourceAttributes : NonResourceAttributes
  // ResourceAuthorizationAttributes describes information for a resource access request
  resourceAttributes : ResourceAttributes
}
