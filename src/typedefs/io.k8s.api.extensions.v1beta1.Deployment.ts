import { DeploymentStatus } from './io.k8s.api.extensions.v1beta1.DeploymentStatus';
import { DeploymentSpec } from './io.k8s.api.extensions.v1beta1.DeploymentSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "extensions/v1beta1";

// DEPRECATED - This group version of Deployment is deprecated by apps/v1beta2/Deployment. See the release notes for more information. Deployment enables declarative updates for Pods and ReplicaSets.
export class Deployment {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // Standard object metadata.
  metadata : ObjectMeta
  // Specification of the desired behavior of the Deployment.
  spec : DeploymentSpec
  // Most recently observed status of the Deployment.
  status : DeploymentStatus
}
