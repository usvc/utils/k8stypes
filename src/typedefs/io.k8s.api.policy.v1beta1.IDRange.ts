export const Version : string = "policy/v1beta1";

// IDRange provides a min/max of an allowed range of IDs.
export class IDRange {
  // max is the end of the range, inclusive.
  max : number
  // min is the start of the range, inclusive.
  min : number
}
