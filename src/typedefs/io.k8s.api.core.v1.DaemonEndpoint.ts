export const Version : string = "core/v1";

// DaemonEndpoint contains information about a single Daemon endpoint.
export class DaemonEndpoint {
  // Port number of the given endpoint.
  Port : number
}
