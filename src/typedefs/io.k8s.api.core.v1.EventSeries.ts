import { MicroTime } from './io.k8s.apimachinery.pkg.apis.meta.v1.MicroTime';
export const Version : string = "core/v1";

// EventSeries contain information on series of events, i.e. thing that was/is happening continuously for some time.
export class EventSeries {
  // Number of occurrences in this series up to the last heartbeat time
  count : number
  // Time of the last occurrence observed
  lastObservedTime : MicroTime
  // State of this Series: Ongoing or Finished Deprecated. Planned removal for 1.18
  state : string
}
