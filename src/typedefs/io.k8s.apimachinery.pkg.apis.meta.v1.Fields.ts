export const Version : string = "meta/v1";
// Fields stores a set of fields in a data structure like a Trie. To understand how this is used, see: https://github.com/kubernetes-sigs/structured-merge-diff
export type Fields = object;
