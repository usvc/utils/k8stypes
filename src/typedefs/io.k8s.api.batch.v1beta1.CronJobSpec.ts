import { JobTemplateSpec } from './io.k8s.api.batch.v1beta1.JobTemplateSpec';
export const Version : string = "batch/v1beta1";

// CronJobSpec describes how the job execution will look like and when it will actually run.
export class CronJobSpec {
  // Specifies how to treat concurrent executions of a Job. Valid values are: - "Allow" (default): allows CronJobs to run concurrently; - "Forbid": forbids concurrent runs, skipping next run if previous run hasn't finished yet; - "Replace": cancels currently running job and replaces it with a new one
  concurrencyPolicy : string
  // The number of failed finished jobs to retain. This is a pointer to distinguish between explicit zero and not specified. Defaults to 1.
  failedJobsHistoryLimit : number
  // Specifies the job that will be created when executing a CronJob.
  jobTemplate : JobTemplateSpec
  // The schedule in Cron format, see https://en.wikipedia.org/wiki/Cron.
  schedule : string
  // Optional deadline in seconds for starting the job if it misses scheduled time for any reason.  Missed jobs executions will be counted as failed ones.
  startingDeadlineSeconds : number
  // The number of successful finished jobs to retain. This is a pointer to distinguish between explicit zero and not specified. Defaults to 3.
  successfulJobsHistoryLimit : number
  // This flag tells the controller to suspend subsequent executions, it does not apply to already started executions.  Defaults to false.
  suspend : boolean
}
