import { RollingUpdateStatefulSetStrategy } from './io.k8s.api.apps.v1beta2.RollingUpdateStatefulSetStrategy';
export const Version : string = "apps/v1beta2";

// StatefulSetUpdateStrategy indicates the strategy that the StatefulSet controller will use to perform updates. It includes any additional parameters necessary to perform the update for the indicated strategy.
export class StatefulSetUpdateStrategy {
  // RollingUpdate is used to communicate parameters when Type is RollingUpdateStatefulSetStrategyType.
  rollingUpdate : RollingUpdateStatefulSetStrategy
  // Type indicates the type of the StatefulSetUpdateStrategy. Default is RollingUpdate.
  type : string
}
