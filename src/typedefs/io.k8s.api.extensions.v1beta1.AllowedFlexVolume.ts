export const Version : string = "extensions/v1beta1";

// AllowedFlexVolume represents a single Flexvolume that is allowed to be used. Deprecated: use AllowedFlexVolume from policy API Group instead.
export class AllowedFlexVolume {
  // driver is the name of the Flexvolume driver.
  driver : string
}
