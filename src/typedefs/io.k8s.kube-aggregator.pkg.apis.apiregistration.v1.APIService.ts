import { APIServiceStatus } from './io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceStatus';
import { APIServiceSpec } from './io.k8s.kube-aggregator.pkg.apis.apiregistration.v1.APIServiceSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "apiregistration/v1";

// APIService represents a server for a particular GroupVersion. Name must be "version.group".
export class APIService {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Spec contains information for locating and communicating with a server
  spec : APIServiceSpec
  // Status contains derived information about an API server
  status : APIServiceStatus
}
