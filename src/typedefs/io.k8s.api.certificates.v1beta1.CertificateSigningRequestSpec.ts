export const Version : string = "certificates/v1beta1";

// This information is immutable after the request is created. Only the Request and Usages fields can be set on creation, other fields are derived by Kubernetes and cannot be modified by users.
export class CertificateSigningRequestSpec {
  // Extra information about the requesting user. See user.Info interface for details.
  extra : object
  // Group information about the requesting user. See user.Info interface for details.
  groups : string[]
  // Base64-encoded PKCS#10 CSR data
  request : string
  // UID information about the requesting user. See user.Info interface for details.
  uid : string
  // allowedUsages specifies a set of usage contexts the key will be valid for. See: https://tools.ietf.org/html/rfc5280#section-4.2.1.3      https://tools.ietf.org/html/rfc5280#section-4.2.1.12
  usages : string[]
  // Information about the requesting user. See user.Info interface for details.
  username : string
}
