import { ReplicaSetCondition } from './io.k8s.api.apps.v1beta2.ReplicaSetCondition';
export const Version : string = "apps/v1beta2";

// ReplicaSetStatus represents the current status of a ReplicaSet.
export class ReplicaSetStatus {
  // The number of available replicas (ready for at least minReadySeconds) for this replica set.
  availableReplicas : number
  // Represents the latest available observations of a replica set's current state.
  conditions : ReplicaSetCondition[]
  // The number of pods that have labels matching the labels of the pod template of the replicaset.
  fullyLabeledReplicas : number
  // ObservedGeneration reflects the generation of the most recently observed ReplicaSet.
  observedGeneration : number
  // The number of ready replicas for this replica set.
  readyReplicas : number
  // Replicas is the most recently oberved number of replicas. More info: https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/#what-is-a-replicationcontroller
  replicas : number
}
