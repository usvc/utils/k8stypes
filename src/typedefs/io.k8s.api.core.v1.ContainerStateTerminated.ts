import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "core/v1";

// ContainerStateTerminated is a terminated state of a container.
export class ContainerStateTerminated {
  // Container's ID in the format 'docker://<container_id>'
  containerID : string
  // Exit status from the last termination of the container
  exitCode : number
  // Time at which the container last terminated
  finishedAt : Time
  // Message regarding the last termination of the container
  message : string
  // (brief) reason from the last termination of the container
  reason : string
  // Signal from the last termination of the container
  signal : number
  // Time at which previous execution of the container started
  startedAt : Time
}
