export const Version : string = "meta/v1";
// Patch is provided to give a concrete name and type to the Kubernetes PATCH request body.
export type Patch = object;
