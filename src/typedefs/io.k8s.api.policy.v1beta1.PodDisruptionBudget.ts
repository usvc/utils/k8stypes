import { PodDisruptionBudgetStatus } from './io.k8s.api.policy.v1beta1.PodDisruptionBudgetStatus';
import { PodDisruptionBudgetSpec } from './io.k8s.api.policy.v1beta1.PodDisruptionBudgetSpec';
import { ObjectMeta } from './io.k8s.apimachinery.pkg.apis.meta.v1.ObjectMeta';
export const Version : string = "policy/v1beta1";

// PodDisruptionBudget is an object to define the max disruption that can be caused to a collection of pods
export class PodDisruptionBudget {
  // APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
  apiVersion : string
  // Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
  kind : string
  // description not available
  metadata : ObjectMeta
  // Specification of the desired behavior of the PodDisruptionBudget.
  spec : PodDisruptionBudgetSpec
  // Most recently observed status of the PodDisruptionBudget.
  status : PodDisruptionBudgetStatus
}
