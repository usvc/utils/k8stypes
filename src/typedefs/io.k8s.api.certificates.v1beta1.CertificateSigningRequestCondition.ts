import { Time } from './io.k8s.apimachinery.pkg.apis.meta.v1.Time';
export const Version : string = "certificates/v1beta1";

// description not available
export class CertificateSigningRequestCondition {
  // timestamp for the last update to this condition
  lastUpdateTime : Time
  // human readable message with details about the request state
  message : string
  // brief reason for the request state
  reason : string
  // request approval state, currently Approved or Denied.
  type : string
}
