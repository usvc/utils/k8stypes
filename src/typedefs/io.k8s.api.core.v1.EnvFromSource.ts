import { SecretEnvSource } from './io.k8s.api.core.v1.SecretEnvSource';
import { ConfigMapEnvSource } from './io.k8s.api.core.v1.ConfigMapEnvSource';
export const Version : string = "core/v1";

// EnvFromSource represents the source of a set of ConfigMaps
export class EnvFromSource {
  // The ConfigMap to select from
  configMapRef : ConfigMapEnvSource
  // An optional identifier to prepend to each key in the ConfigMap. Must be a C_IDENTIFIER.
  prefix : string
  // The Secret to select from
  secretRef : SecretEnvSource
}
