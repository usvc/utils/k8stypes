import { MetricIdentifier } from './io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
import { MetricValueStatus } from './io.k8s.api.autoscaling.v2beta2.MetricValueStatus';
export const Version : string = "autoscaling/v2beta2";

// PodsMetricStatus indicates the current value of a metric describing each pod in the current scale target (for example, transactions-processed-per-second).
export class PodsMetricStatus {
  // current contains the current value for the given metric
  current : MetricValueStatus
  // metric identifies the target metric by name and selector
  metric : MetricIdentifier
}
