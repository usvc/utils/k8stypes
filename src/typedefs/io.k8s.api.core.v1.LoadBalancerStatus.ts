import { LoadBalancerIngress } from './io.k8s.api.core.v1.LoadBalancerIngress';
export const Version : string = "core/v1";

// LoadBalancerStatus represents the status of a load-balancer.
export class LoadBalancerStatus {
  // Ingress is a list containing ingress points for the load-balancer. Traffic intended for the service should be sent to these ingress points.
  ingress : LoadBalancerIngress[]
}
