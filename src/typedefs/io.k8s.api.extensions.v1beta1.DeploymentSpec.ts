import { PodTemplateSpec } from './io.k8s.api.core.v1.PodTemplateSpec';
import { DeploymentStrategy } from './io.k8s.api.extensions.v1beta1.DeploymentStrategy';
import { LabelSelector } from './io.k8s.apimachinery.pkg.apis.meta.v1.LabelSelector';
import { RollbackConfig } from './io.k8s.api.extensions.v1beta1.RollbackConfig';
export const Version : string = "extensions/v1beta1";

// DeploymentSpec is the specification of the desired behavior of the Deployment.
export class DeploymentSpec {
  // Minimum number of seconds for which a newly created pod should be ready without any of its container crashing, for it to be considered available. Defaults to 0 (pod will be considered available as soon as it is ready)
  minReadySeconds : number
  // Indicates that the deployment is paused and will not be processed by the deployment controller.
  paused : boolean
  // The maximum time in seconds for a deployment to make progress before it is considered to be failed. The deployment controller will continue to process failed deployments and a condition with a ProgressDeadlineExceeded reason will be surfaced in the deployment status. Note that progress will not be estimated during the time a deployment is paused. This is set to the max value of int32 (i.e. 2147483647) by default, which means "no deadline".
  progressDeadlineSeconds : number
  // Number of desired pods. This is a pointer to distinguish between explicit zero and not specified. Defaults to 1.
  replicas : number
  // The number of old ReplicaSets to retain to allow rollback. This is a pointer to distinguish between explicit zero and not specified. This is set to the max value of int32 (i.e. 2147483647) by default, which means "retaining all old RelicaSets".
  revisionHistoryLimit : number
  // DEPRECATED. The config this deployment is rolling back to. Will be cleared after rollback is done.
  rollbackTo : RollbackConfig
  // Label selector for pods. Existing ReplicaSets whose pods are selected by this will be the ones affected by this deployment.
  selector : LabelSelector
  // The deployment strategy to use to replace existing pods with new ones.
  strategy : DeploymentStrategy
  // Template describes the pods that will be created.
  template : PodTemplateSpec
}
