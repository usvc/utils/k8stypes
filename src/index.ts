import { VolumeError as storagev1beta1VolumeError } from './typedefs/io.k8s.api.storage.v1beta1.VolumeError';
import { VolumeAttachmentStatus as storagev1beta1VolumeAttachmentStatus } from './typedefs/io.k8s.api.storage.v1beta1.VolumeAttachmentStatus';
import { VolumeAttachmentSpec as storagev1beta1VolumeAttachmentSpec } from './typedefs/io.k8s.api.storage.v1beta1.VolumeAttachmentSpec';
import { VolumeAttachmentSource as storagev1beta1VolumeAttachmentSource } from './typedefs/io.k8s.api.storage.v1beta1.VolumeAttachmentSource';
import { VolumeAttachmentList as storagev1beta1VolumeAttachmentList } from './typedefs/io.k8s.api.storage.v1beta1.VolumeAttachmentList';
import { VolumeAttachment as storagev1beta1VolumeAttachment } from './typedefs/io.k8s.api.storage.v1beta1.VolumeAttachment';
import { StorageClassList as storagev1beta1StorageClassList } from './typedefs/io.k8s.api.storage.v1beta1.StorageClassList';
import { StorageClass as storagev1beta1StorageClass } from './typedefs/io.k8s.api.storage.v1beta1.StorageClass';
import { CSINodeSpec as storagev1beta1CSINodeSpec } from './typedefs/io.k8s.api.storage.v1beta1.CSINodeSpec';
import { CSINodeList as storagev1beta1CSINodeList } from './typedefs/io.k8s.api.storage.v1beta1.CSINodeList';
import { CSINodeDriver as storagev1beta1CSINodeDriver } from './typedefs/io.k8s.api.storage.v1beta1.CSINodeDriver';
import { CSINode as storagev1beta1CSINode } from './typedefs/io.k8s.api.storage.v1beta1.CSINode';
import { CSIDriverSpec as storagev1beta1CSIDriverSpec } from './typedefs/io.k8s.api.storage.v1beta1.CSIDriverSpec';
import { CSIDriverList as storagev1beta1CSIDriverList } from './typedefs/io.k8s.api.storage.v1beta1.CSIDriverList';
import { CSIDriver as storagev1beta1CSIDriver } from './typedefs/io.k8s.api.storage.v1beta1.CSIDriver';
import { VolumeError as storagev1alpha1VolumeError } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeError';
import { VolumeAttachmentStatus as storagev1alpha1VolumeAttachmentStatus } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeAttachmentStatus';
import { VolumeAttachmentSpec as storagev1alpha1VolumeAttachmentSpec } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeAttachmentSpec';
import { VolumeAttachmentSource as storagev1alpha1VolumeAttachmentSource } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeAttachmentSource';
import { VolumeAttachmentList as storagev1alpha1VolumeAttachmentList } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeAttachmentList';
import { VolumeAttachment as storagev1alpha1VolumeAttachment } from './typedefs/io.k8s.api.storage.v1alpha1.VolumeAttachment';
import { VolumeError as storagev1VolumeError } from './typedefs/io.k8s.api.storage.v1.VolumeError';
import { VolumeAttachmentStatus as storagev1VolumeAttachmentStatus } from './typedefs/io.k8s.api.storage.v1.VolumeAttachmentStatus';
import { VolumeAttachmentSpec as storagev1VolumeAttachmentSpec } from './typedefs/io.k8s.api.storage.v1.VolumeAttachmentSpec';
import { VolumeAttachmentSource as storagev1VolumeAttachmentSource } from './typedefs/io.k8s.api.storage.v1.VolumeAttachmentSource';
import { VolumeAttachmentList as storagev1VolumeAttachmentList } from './typedefs/io.k8s.api.storage.v1.VolumeAttachmentList';
import { VolumeAttachment as storagev1VolumeAttachment } from './typedefs/io.k8s.api.storage.v1.VolumeAttachment';
import { StorageClassList as storagev1StorageClassList } from './typedefs/io.k8s.api.storage.v1.StorageClassList';
import { StorageClass as storagev1StorageClass } from './typedefs/io.k8s.api.storage.v1.StorageClass';
import { PodPresetSpec as settingsv1alpha1PodPresetSpec } from './typedefs/io.k8s.api.settings.v1alpha1.PodPresetSpec';
import { PodPresetList as settingsv1alpha1PodPresetList } from './typedefs/io.k8s.api.settings.v1alpha1.PodPresetList';
import { PodPreset as settingsv1alpha1PodPreset } from './typedefs/io.k8s.api.settings.v1alpha1.PodPreset';
import { PriorityClassList as schedulingv1beta1PriorityClassList } from './typedefs/io.k8s.api.scheduling.v1beta1.PriorityClassList';
import { PriorityClass as schedulingv1beta1PriorityClass } from './typedefs/io.k8s.api.scheduling.v1beta1.PriorityClass';
import { PriorityClassList as schedulingv1alpha1PriorityClassList } from './typedefs/io.k8s.api.scheduling.v1alpha1.PriorityClassList';
import { PriorityClass as schedulingv1alpha1PriorityClass } from './typedefs/io.k8s.api.scheduling.v1alpha1.PriorityClass';
import { PriorityClassList as schedulingv1PriorityClassList } from './typedefs/io.k8s.api.scheduling.v1.PriorityClassList';
import { PriorityClass as schedulingv1PriorityClass } from './typedefs/io.k8s.api.scheduling.v1.PriorityClass';
import { Subject as rbacv1beta1Subject } from './typedefs/io.k8s.api.rbac.v1beta1.Subject';
import { RoleRef as rbacv1beta1RoleRef } from './typedefs/io.k8s.api.rbac.v1beta1.RoleRef';
import { RoleList as rbacv1beta1RoleList } from './typedefs/io.k8s.api.rbac.v1beta1.RoleList';
import { RoleBindingList as rbacv1beta1RoleBindingList } from './typedefs/io.k8s.api.rbac.v1beta1.RoleBindingList';
import { RoleBinding as rbacv1beta1RoleBinding } from './typedefs/io.k8s.api.rbac.v1beta1.RoleBinding';
import { Role as rbacv1beta1Role } from './typedefs/io.k8s.api.rbac.v1beta1.Role';
import { PolicyRule as rbacv1beta1PolicyRule } from './typedefs/io.k8s.api.rbac.v1beta1.PolicyRule';
import { ClusterRoleList as rbacv1beta1ClusterRoleList } from './typedefs/io.k8s.api.rbac.v1beta1.ClusterRoleList';
import { ClusterRoleBindingList as rbacv1beta1ClusterRoleBindingList } from './typedefs/io.k8s.api.rbac.v1beta1.ClusterRoleBindingList';
import { ClusterRoleBinding as rbacv1beta1ClusterRoleBinding } from './typedefs/io.k8s.api.rbac.v1beta1.ClusterRoleBinding';
import { ClusterRole as rbacv1beta1ClusterRole } from './typedefs/io.k8s.api.rbac.v1beta1.ClusterRole';
import { AggregationRule as rbacv1beta1AggregationRule } from './typedefs/io.k8s.api.rbac.v1beta1.AggregationRule';
import { Subject as rbacv1alpha1Subject } from './typedefs/io.k8s.api.rbac.v1alpha1.Subject';
import { RoleRef as rbacv1alpha1RoleRef } from './typedefs/io.k8s.api.rbac.v1alpha1.RoleRef';
import { RoleList as rbacv1alpha1RoleList } from './typedefs/io.k8s.api.rbac.v1alpha1.RoleList';
import { RoleBindingList as rbacv1alpha1RoleBindingList } from './typedefs/io.k8s.api.rbac.v1alpha1.RoleBindingList';
import { RoleBinding as rbacv1alpha1RoleBinding } from './typedefs/io.k8s.api.rbac.v1alpha1.RoleBinding';
import { Role as rbacv1alpha1Role } from './typedefs/io.k8s.api.rbac.v1alpha1.Role';
import { PolicyRule as rbacv1alpha1PolicyRule } from './typedefs/io.k8s.api.rbac.v1alpha1.PolicyRule';
import { ClusterRoleList as rbacv1alpha1ClusterRoleList } from './typedefs/io.k8s.api.rbac.v1alpha1.ClusterRoleList';
import { ClusterRoleBindingList as rbacv1alpha1ClusterRoleBindingList } from './typedefs/io.k8s.api.rbac.v1alpha1.ClusterRoleBindingList';
import { ClusterRoleBinding as rbacv1alpha1ClusterRoleBinding } from './typedefs/io.k8s.api.rbac.v1alpha1.ClusterRoleBinding';
import { ClusterRole as rbacv1alpha1ClusterRole } from './typedefs/io.k8s.api.rbac.v1alpha1.ClusterRole';
import { AggregationRule as rbacv1alpha1AggregationRule } from './typedefs/io.k8s.api.rbac.v1alpha1.AggregationRule';
import { Subject as rbacv1Subject } from './typedefs/io.k8s.api.rbac.v1.Subject';
import { RoleRef as rbacv1RoleRef } from './typedefs/io.k8s.api.rbac.v1.RoleRef';
import { RoleList as rbacv1RoleList } from './typedefs/io.k8s.api.rbac.v1.RoleList';
import { RoleBindingList as rbacv1RoleBindingList } from './typedefs/io.k8s.api.rbac.v1.RoleBindingList';
import { RoleBinding as rbacv1RoleBinding } from './typedefs/io.k8s.api.rbac.v1.RoleBinding';
import { Role as rbacv1Role } from './typedefs/io.k8s.api.rbac.v1.Role';
import { PolicyRule as rbacv1PolicyRule } from './typedefs/io.k8s.api.rbac.v1.PolicyRule';
import { ClusterRoleList as rbacv1ClusterRoleList } from './typedefs/io.k8s.api.rbac.v1.ClusterRoleList';
import { ClusterRoleBindingList as rbacv1ClusterRoleBindingList } from './typedefs/io.k8s.api.rbac.v1.ClusterRoleBindingList';
import { ClusterRoleBinding as rbacv1ClusterRoleBinding } from './typedefs/io.k8s.api.rbac.v1.ClusterRoleBinding';
import { ClusterRole as rbacv1ClusterRole } from './typedefs/io.k8s.api.rbac.v1.ClusterRole';
import { AggregationRule as rbacv1AggregationRule } from './typedefs/io.k8s.api.rbac.v1.AggregationRule';
import { SupplementalGroupsStrategyOptions as policyv1beta1SupplementalGroupsStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.SupplementalGroupsStrategyOptions';
import { SELinuxStrategyOptions as policyv1beta1SELinuxStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.SELinuxStrategyOptions';
import { RuntimeClassStrategyOptions as policyv1beta1RuntimeClassStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.RuntimeClassStrategyOptions';
import { RunAsUserStrategyOptions as policyv1beta1RunAsUserStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.RunAsUserStrategyOptions';
import { RunAsGroupStrategyOptions as policyv1beta1RunAsGroupStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.RunAsGroupStrategyOptions';
import { PodSecurityPolicySpec as policyv1beta1PodSecurityPolicySpec } from './typedefs/io.k8s.api.policy.v1beta1.PodSecurityPolicySpec';
import { PodSecurityPolicyList as policyv1beta1PodSecurityPolicyList } from './typedefs/io.k8s.api.policy.v1beta1.PodSecurityPolicyList';
import { PodSecurityPolicy as policyv1beta1PodSecurityPolicy } from './typedefs/io.k8s.api.policy.v1beta1.PodSecurityPolicy';
import { PodDisruptionBudgetStatus as policyv1beta1PodDisruptionBudgetStatus } from './typedefs/io.k8s.api.policy.v1beta1.PodDisruptionBudgetStatus';
import { PodDisruptionBudgetSpec as policyv1beta1PodDisruptionBudgetSpec } from './typedefs/io.k8s.api.policy.v1beta1.PodDisruptionBudgetSpec';
import { PodDisruptionBudgetList as policyv1beta1PodDisruptionBudgetList } from './typedefs/io.k8s.api.policy.v1beta1.PodDisruptionBudgetList';
import { PodDisruptionBudget as policyv1beta1PodDisruptionBudget } from './typedefs/io.k8s.api.policy.v1beta1.PodDisruptionBudget';
import { IDRange as policyv1beta1IDRange } from './typedefs/io.k8s.api.policy.v1beta1.IDRange';
import { HostPortRange as policyv1beta1HostPortRange } from './typedefs/io.k8s.api.policy.v1beta1.HostPortRange';
import { FSGroupStrategyOptions as policyv1beta1FSGroupStrategyOptions } from './typedefs/io.k8s.api.policy.v1beta1.FSGroupStrategyOptions';
import { Eviction as policyv1beta1Eviction } from './typedefs/io.k8s.api.policy.v1beta1.Eviction';
import { AllowedHostPath as policyv1beta1AllowedHostPath } from './typedefs/io.k8s.api.policy.v1beta1.AllowedHostPath';
import { AllowedFlexVolume as policyv1beta1AllowedFlexVolume } from './typedefs/io.k8s.api.policy.v1beta1.AllowedFlexVolume';
import { AllowedCSIDriver as policyv1beta1AllowedCSIDriver } from './typedefs/io.k8s.api.policy.v1beta1.AllowedCSIDriver';
import { RuntimeClassList as nodev1beta1RuntimeClassList } from './typedefs/io.k8s.api.node.v1beta1.RuntimeClassList';
import { RuntimeClass as nodev1beta1RuntimeClass } from './typedefs/io.k8s.api.node.v1beta1.RuntimeClass';
import { RuntimeClassSpec as nodev1alpha1RuntimeClassSpec } from './typedefs/io.k8s.api.node.v1alpha1.RuntimeClassSpec';
import { RuntimeClassList as nodev1alpha1RuntimeClassList } from './typedefs/io.k8s.api.node.v1alpha1.RuntimeClassList';
import { RuntimeClass as nodev1alpha1RuntimeClass } from './typedefs/io.k8s.api.node.v1alpha1.RuntimeClass';
import { IngressTLS as networkingv1beta1IngressTLS } from './typedefs/io.k8s.api.networking.v1beta1.IngressTLS';
import { IngressStatus as networkingv1beta1IngressStatus } from './typedefs/io.k8s.api.networking.v1beta1.IngressStatus';
import { IngressSpec as networkingv1beta1IngressSpec } from './typedefs/io.k8s.api.networking.v1beta1.IngressSpec';
import { IngressRule as networkingv1beta1IngressRule } from './typedefs/io.k8s.api.networking.v1beta1.IngressRule';
import { IngressList as networkingv1beta1IngressList } from './typedefs/io.k8s.api.networking.v1beta1.IngressList';
import { IngressBackend as networkingv1beta1IngressBackend } from './typedefs/io.k8s.api.networking.v1beta1.IngressBackend';
import { Ingress as networkingv1beta1Ingress } from './typedefs/io.k8s.api.networking.v1beta1.Ingress';
import { HTTPIngressRuleValue as networkingv1beta1HTTPIngressRuleValue } from './typedefs/io.k8s.api.networking.v1beta1.HTTPIngressRuleValue';
import { HTTPIngressPath as networkingv1beta1HTTPIngressPath } from './typedefs/io.k8s.api.networking.v1beta1.HTTPIngressPath';
import { NetworkPolicySpec as networkingv1NetworkPolicySpec } from './typedefs/io.k8s.api.networking.v1.NetworkPolicySpec';
import { NetworkPolicyPort as networkingv1NetworkPolicyPort } from './typedefs/io.k8s.api.networking.v1.NetworkPolicyPort';
import { NetworkPolicyPeer as networkingv1NetworkPolicyPeer } from './typedefs/io.k8s.api.networking.v1.NetworkPolicyPeer';
import { NetworkPolicyList as networkingv1NetworkPolicyList } from './typedefs/io.k8s.api.networking.v1.NetworkPolicyList';
import { NetworkPolicyIngressRule as networkingv1NetworkPolicyIngressRule } from './typedefs/io.k8s.api.networking.v1.NetworkPolicyIngressRule';
import { NetworkPolicyEgressRule as networkingv1NetworkPolicyEgressRule } from './typedefs/io.k8s.api.networking.v1.NetworkPolicyEgressRule';
import { NetworkPolicy as networkingv1NetworkPolicy } from './typedefs/io.k8s.api.networking.v1.NetworkPolicy';
import { IPBlock as networkingv1IPBlock } from './typedefs/io.k8s.api.networking.v1.IPBlock';
import { SupplementalGroupsStrategyOptions as extensionsv1beta1SupplementalGroupsStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.SupplementalGroupsStrategyOptions';
import { ScaleStatus as extensionsv1beta1ScaleStatus } from './typedefs/io.k8s.api.extensions.v1beta1.ScaleStatus';
import { ScaleSpec as extensionsv1beta1ScaleSpec } from './typedefs/io.k8s.api.extensions.v1beta1.ScaleSpec';
import { Scale as extensionsv1beta1Scale } from './typedefs/io.k8s.api.extensions.v1beta1.Scale';
import { SELinuxStrategyOptions as extensionsv1beta1SELinuxStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.SELinuxStrategyOptions';
import { RuntimeClassStrategyOptions as extensionsv1beta1RuntimeClassStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.RuntimeClassStrategyOptions';
import { RunAsUserStrategyOptions as extensionsv1beta1RunAsUserStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.RunAsUserStrategyOptions';
import { RunAsGroupStrategyOptions as extensionsv1beta1RunAsGroupStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.RunAsGroupStrategyOptions';
import { RollingUpdateDeployment as extensionsv1beta1RollingUpdateDeployment } from './typedefs/io.k8s.api.extensions.v1beta1.RollingUpdateDeployment';
import { RollingUpdateDaemonSet as extensionsv1beta1RollingUpdateDaemonSet } from './typedefs/io.k8s.api.extensions.v1beta1.RollingUpdateDaemonSet';
import { RollbackConfig as extensionsv1beta1RollbackConfig } from './typedefs/io.k8s.api.extensions.v1beta1.RollbackConfig';
import { ReplicaSetStatus as extensionsv1beta1ReplicaSetStatus } from './typedefs/io.k8s.api.extensions.v1beta1.ReplicaSetStatus';
import { ReplicaSetSpec as extensionsv1beta1ReplicaSetSpec } from './typedefs/io.k8s.api.extensions.v1beta1.ReplicaSetSpec';
import { ReplicaSetList as extensionsv1beta1ReplicaSetList } from './typedefs/io.k8s.api.extensions.v1beta1.ReplicaSetList';
import { ReplicaSetCondition as extensionsv1beta1ReplicaSetCondition } from './typedefs/io.k8s.api.extensions.v1beta1.ReplicaSetCondition';
import { ReplicaSet as extensionsv1beta1ReplicaSet } from './typedefs/io.k8s.api.extensions.v1beta1.ReplicaSet';
import { PodSecurityPolicySpec as extensionsv1beta1PodSecurityPolicySpec } from './typedefs/io.k8s.api.extensions.v1beta1.PodSecurityPolicySpec';
import { PodSecurityPolicyList as extensionsv1beta1PodSecurityPolicyList } from './typedefs/io.k8s.api.extensions.v1beta1.PodSecurityPolicyList';
import { PodSecurityPolicy as extensionsv1beta1PodSecurityPolicy } from './typedefs/io.k8s.api.extensions.v1beta1.PodSecurityPolicy';
import { NetworkPolicySpec as extensionsv1beta1NetworkPolicySpec } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicySpec';
import { NetworkPolicyPort as extensionsv1beta1NetworkPolicyPort } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicyPort';
import { NetworkPolicyPeer as extensionsv1beta1NetworkPolicyPeer } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicyPeer';
import { NetworkPolicyList as extensionsv1beta1NetworkPolicyList } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicyList';
import { NetworkPolicyIngressRule as extensionsv1beta1NetworkPolicyIngressRule } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicyIngressRule';
import { NetworkPolicyEgressRule as extensionsv1beta1NetworkPolicyEgressRule } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicyEgressRule';
import { NetworkPolicy as extensionsv1beta1NetworkPolicy } from './typedefs/io.k8s.api.extensions.v1beta1.NetworkPolicy';
import { IngressTLS as extensionsv1beta1IngressTLS } from './typedefs/io.k8s.api.extensions.v1beta1.IngressTLS';
import { IngressStatus as extensionsv1beta1IngressStatus } from './typedefs/io.k8s.api.extensions.v1beta1.IngressStatus';
import { IngressSpec as extensionsv1beta1IngressSpec } from './typedefs/io.k8s.api.extensions.v1beta1.IngressSpec';
import { IngressRule as extensionsv1beta1IngressRule } from './typedefs/io.k8s.api.extensions.v1beta1.IngressRule';
import { IngressList as extensionsv1beta1IngressList } from './typedefs/io.k8s.api.extensions.v1beta1.IngressList';
import { IngressBackend as extensionsv1beta1IngressBackend } from './typedefs/io.k8s.api.extensions.v1beta1.IngressBackend';
import { Ingress as extensionsv1beta1Ingress } from './typedefs/io.k8s.api.extensions.v1beta1.Ingress';
import { IPBlock as extensionsv1beta1IPBlock } from './typedefs/io.k8s.api.extensions.v1beta1.IPBlock';
import { IDRange as extensionsv1beta1IDRange } from './typedefs/io.k8s.api.extensions.v1beta1.IDRange';
import { HostPortRange as extensionsv1beta1HostPortRange } from './typedefs/io.k8s.api.extensions.v1beta1.HostPortRange';
import { HTTPIngressRuleValue as extensionsv1beta1HTTPIngressRuleValue } from './typedefs/io.k8s.api.extensions.v1beta1.HTTPIngressRuleValue';
import { HTTPIngressPath as extensionsv1beta1HTTPIngressPath } from './typedefs/io.k8s.api.extensions.v1beta1.HTTPIngressPath';
import { FSGroupStrategyOptions as extensionsv1beta1FSGroupStrategyOptions } from './typedefs/io.k8s.api.extensions.v1beta1.FSGroupStrategyOptions';
import { DeploymentStrategy as extensionsv1beta1DeploymentStrategy } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentStrategy';
import { DeploymentStatus as extensionsv1beta1DeploymentStatus } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentStatus';
import { DeploymentSpec as extensionsv1beta1DeploymentSpec } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentSpec';
import { DeploymentRollback as extensionsv1beta1DeploymentRollback } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentRollback';
import { DeploymentList as extensionsv1beta1DeploymentList } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentList';
import { DeploymentCondition as extensionsv1beta1DeploymentCondition } from './typedefs/io.k8s.api.extensions.v1beta1.DeploymentCondition';
import { Deployment as extensionsv1beta1Deployment } from './typedefs/io.k8s.api.extensions.v1beta1.Deployment';
import { DaemonSetUpdateStrategy as extensionsv1beta1DaemonSetUpdateStrategy } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSetUpdateStrategy';
import { DaemonSetStatus as extensionsv1beta1DaemonSetStatus } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSetStatus';
import { DaemonSetSpec as extensionsv1beta1DaemonSetSpec } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSetSpec';
import { DaemonSetList as extensionsv1beta1DaemonSetList } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSetList';
import { DaemonSetCondition as extensionsv1beta1DaemonSetCondition } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSetCondition';
import { DaemonSet as extensionsv1beta1DaemonSet } from './typedefs/io.k8s.api.extensions.v1beta1.DaemonSet';
import { AllowedHostPath as extensionsv1beta1AllowedHostPath } from './typedefs/io.k8s.api.extensions.v1beta1.AllowedHostPath';
import { AllowedFlexVolume as extensionsv1beta1AllowedFlexVolume } from './typedefs/io.k8s.api.extensions.v1beta1.AllowedFlexVolume';
import { AllowedCSIDriver as extensionsv1beta1AllowedCSIDriver } from './typedefs/io.k8s.api.extensions.v1beta1.AllowedCSIDriver';
import { EventSeries as eventsv1beta1EventSeries } from './typedefs/io.k8s.api.events.v1beta1.EventSeries';
import { EventList as eventsv1beta1EventList } from './typedefs/io.k8s.api.events.v1beta1.EventList';
import { Event as eventsv1beta1Event } from './typedefs/io.k8s.api.events.v1beta1.Event';
import { WindowsSecurityContextOptions as corev1WindowsSecurityContextOptions } from './typedefs/io.k8s.api.core.v1.WindowsSecurityContextOptions';
import { WeightedPodAffinityTerm as corev1WeightedPodAffinityTerm } from './typedefs/io.k8s.api.core.v1.WeightedPodAffinityTerm';
import { VsphereVirtualDiskVolumeSource as corev1VsphereVirtualDiskVolumeSource } from './typedefs/io.k8s.api.core.v1.VsphereVirtualDiskVolumeSource';
import { VolumeProjection as corev1VolumeProjection } from './typedefs/io.k8s.api.core.v1.VolumeProjection';
import { VolumeNodeAffinity as corev1VolumeNodeAffinity } from './typedefs/io.k8s.api.core.v1.VolumeNodeAffinity';
import { VolumeMount as corev1VolumeMount } from './typedefs/io.k8s.api.core.v1.VolumeMount';
import { VolumeDevice as corev1VolumeDevice } from './typedefs/io.k8s.api.core.v1.VolumeDevice';
import { Volume as corev1Volume } from './typedefs/io.k8s.api.core.v1.Volume';
import { TypedLocalObjectReference as corev1TypedLocalObjectReference } from './typedefs/io.k8s.api.core.v1.TypedLocalObjectReference';
import { TopologySelectorTerm as corev1TopologySelectorTerm } from './typedefs/io.k8s.api.core.v1.TopologySelectorTerm';
import { TopologySelectorLabelRequirement as corev1TopologySelectorLabelRequirement } from './typedefs/io.k8s.api.core.v1.TopologySelectorLabelRequirement';
import { Toleration as corev1Toleration } from './typedefs/io.k8s.api.core.v1.Toleration';
import { Taint as corev1Taint } from './typedefs/io.k8s.api.core.v1.Taint';
import { TCPSocketAction as corev1TCPSocketAction } from './typedefs/io.k8s.api.core.v1.TCPSocketAction';
import { Sysctl as corev1Sysctl } from './typedefs/io.k8s.api.core.v1.Sysctl';
import { StorageOSVolumeSource as corev1StorageOSVolumeSource } from './typedefs/io.k8s.api.core.v1.StorageOSVolumeSource';
import { StorageOSPersistentVolumeSource as corev1StorageOSPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.StorageOSPersistentVolumeSource';
import { SessionAffinityConfig as corev1SessionAffinityConfig } from './typedefs/io.k8s.api.core.v1.SessionAffinityConfig';
import { ServiceStatus as corev1ServiceStatus } from './typedefs/io.k8s.api.core.v1.ServiceStatus';
import { ServiceSpec as corev1ServiceSpec } from './typedefs/io.k8s.api.core.v1.ServiceSpec';
import { ServicePort as corev1ServicePort } from './typedefs/io.k8s.api.core.v1.ServicePort';
import { ServiceList as corev1ServiceList } from './typedefs/io.k8s.api.core.v1.ServiceList';
import { ServiceAccountTokenProjection as corev1ServiceAccountTokenProjection } from './typedefs/io.k8s.api.core.v1.ServiceAccountTokenProjection';
import { ServiceAccountList as corev1ServiceAccountList } from './typedefs/io.k8s.api.core.v1.ServiceAccountList';
import { ServiceAccount as corev1ServiceAccount } from './typedefs/io.k8s.api.core.v1.ServiceAccount';
import { Service as corev1Service } from './typedefs/io.k8s.api.core.v1.Service';
import { SecurityContext as corev1SecurityContext } from './typedefs/io.k8s.api.core.v1.SecurityContext';
import { SecretVolumeSource as corev1SecretVolumeSource } from './typedefs/io.k8s.api.core.v1.SecretVolumeSource';
import { SecretReference as corev1SecretReference } from './typedefs/io.k8s.api.core.v1.SecretReference';
import { SecretProjection as corev1SecretProjection } from './typedefs/io.k8s.api.core.v1.SecretProjection';
import { SecretList as corev1SecretList } from './typedefs/io.k8s.api.core.v1.SecretList';
import { SecretKeySelector as corev1SecretKeySelector } from './typedefs/io.k8s.api.core.v1.SecretKeySelector';
import { SecretEnvSource as corev1SecretEnvSource } from './typedefs/io.k8s.api.core.v1.SecretEnvSource';
import { Secret as corev1Secret } from './typedefs/io.k8s.api.core.v1.Secret';
import { ScopedResourceSelectorRequirement as corev1ScopedResourceSelectorRequirement } from './typedefs/io.k8s.api.core.v1.ScopedResourceSelectorRequirement';
import { ScopeSelector as corev1ScopeSelector } from './typedefs/io.k8s.api.core.v1.ScopeSelector';
import { ScaleIOVolumeSource as corev1ScaleIOVolumeSource } from './typedefs/io.k8s.api.core.v1.ScaleIOVolumeSource';
import { ScaleIOPersistentVolumeSource as corev1ScaleIOPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.ScaleIOPersistentVolumeSource';
import { SELinuxOptions as corev1SELinuxOptions } from './typedefs/io.k8s.api.core.v1.SELinuxOptions';
import { ResourceRequirements as corev1ResourceRequirements } from './typedefs/io.k8s.api.core.v1.ResourceRequirements';
import { ResourceQuotaStatus as corev1ResourceQuotaStatus } from './typedefs/io.k8s.api.core.v1.ResourceQuotaStatus';
import { ResourceQuotaSpec as corev1ResourceQuotaSpec } from './typedefs/io.k8s.api.core.v1.ResourceQuotaSpec';
import { ResourceQuotaList as corev1ResourceQuotaList } from './typedefs/io.k8s.api.core.v1.ResourceQuotaList';
import { ResourceQuota as corev1ResourceQuota } from './typedefs/io.k8s.api.core.v1.ResourceQuota';
import { ResourceFieldSelector as corev1ResourceFieldSelector } from './typedefs/io.k8s.api.core.v1.ResourceFieldSelector';
import { ReplicationControllerStatus as corev1ReplicationControllerStatus } from './typedefs/io.k8s.api.core.v1.ReplicationControllerStatus';
import { ReplicationControllerSpec as corev1ReplicationControllerSpec } from './typedefs/io.k8s.api.core.v1.ReplicationControllerSpec';
import { ReplicationControllerList as corev1ReplicationControllerList } from './typedefs/io.k8s.api.core.v1.ReplicationControllerList';
import { ReplicationControllerCondition as corev1ReplicationControllerCondition } from './typedefs/io.k8s.api.core.v1.ReplicationControllerCondition';
import { ReplicationController as corev1ReplicationController } from './typedefs/io.k8s.api.core.v1.ReplicationController';
import { RBDVolumeSource as corev1RBDVolumeSource } from './typedefs/io.k8s.api.core.v1.RBDVolumeSource';
import { RBDPersistentVolumeSource as corev1RBDPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.RBDPersistentVolumeSource';
import { QuobyteVolumeSource as corev1QuobyteVolumeSource } from './typedefs/io.k8s.api.core.v1.QuobyteVolumeSource';
import { ProjectedVolumeSource as corev1ProjectedVolumeSource } from './typedefs/io.k8s.api.core.v1.ProjectedVolumeSource';
import { Probe as corev1Probe } from './typedefs/io.k8s.api.core.v1.Probe';
import { PreferredSchedulingTerm as corev1PreferredSchedulingTerm } from './typedefs/io.k8s.api.core.v1.PreferredSchedulingTerm';
import { PortworxVolumeSource as corev1PortworxVolumeSource } from './typedefs/io.k8s.api.core.v1.PortworxVolumeSource';
import { PodTemplateSpec as corev1PodTemplateSpec } from './typedefs/io.k8s.api.core.v1.PodTemplateSpec';
import { PodTemplateList as corev1PodTemplateList } from './typedefs/io.k8s.api.core.v1.PodTemplateList';
import { PodTemplate as corev1PodTemplate } from './typedefs/io.k8s.api.core.v1.PodTemplate';
import { PodStatus as corev1PodStatus } from './typedefs/io.k8s.api.core.v1.PodStatus';
import { PodSpec as corev1PodSpec } from './typedefs/io.k8s.api.core.v1.PodSpec';
import { PodSecurityContext as corev1PodSecurityContext } from './typedefs/io.k8s.api.core.v1.PodSecurityContext';
import { PodReadinessGate as corev1PodReadinessGate } from './typedefs/io.k8s.api.core.v1.PodReadinessGate';
import { PodList as corev1PodList } from './typedefs/io.k8s.api.core.v1.PodList';
import { PodDNSConfigOption as corev1PodDNSConfigOption } from './typedefs/io.k8s.api.core.v1.PodDNSConfigOption';
import { PodDNSConfig as corev1PodDNSConfig } from './typedefs/io.k8s.api.core.v1.PodDNSConfig';
import { PodCondition as corev1PodCondition } from './typedefs/io.k8s.api.core.v1.PodCondition';
import { PodAntiAffinity as corev1PodAntiAffinity } from './typedefs/io.k8s.api.core.v1.PodAntiAffinity';
import { PodAffinityTerm as corev1PodAffinityTerm } from './typedefs/io.k8s.api.core.v1.PodAffinityTerm';
import { PodAffinity as corev1PodAffinity } from './typedefs/io.k8s.api.core.v1.PodAffinity';
import { Pod as corev1Pod } from './typedefs/io.k8s.api.core.v1.Pod';
import { PhotonPersistentDiskVolumeSource as corev1PhotonPersistentDiskVolumeSource } from './typedefs/io.k8s.api.core.v1.PhotonPersistentDiskVolumeSource';
import { PersistentVolumeStatus as corev1PersistentVolumeStatus } from './typedefs/io.k8s.api.core.v1.PersistentVolumeStatus';
import { PersistentVolumeSpec as corev1PersistentVolumeSpec } from './typedefs/io.k8s.api.core.v1.PersistentVolumeSpec';
import { PersistentVolumeList as corev1PersistentVolumeList } from './typedefs/io.k8s.api.core.v1.PersistentVolumeList';
import { PersistentVolumeClaimVolumeSource as corev1PersistentVolumeClaimVolumeSource } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaimVolumeSource';
import { PersistentVolumeClaimStatus as corev1PersistentVolumeClaimStatus } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaimStatus';
import { PersistentVolumeClaimSpec as corev1PersistentVolumeClaimSpec } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaimSpec';
import { PersistentVolumeClaimList as corev1PersistentVolumeClaimList } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaimList';
import { PersistentVolumeClaimCondition as corev1PersistentVolumeClaimCondition } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaimCondition';
import { PersistentVolumeClaim as corev1PersistentVolumeClaim } from './typedefs/io.k8s.api.core.v1.PersistentVolumeClaim';
import { PersistentVolume as corev1PersistentVolume } from './typedefs/io.k8s.api.core.v1.PersistentVolume';
import { ObjectReference as corev1ObjectReference } from './typedefs/io.k8s.api.core.v1.ObjectReference';
import { ObjectFieldSelector as corev1ObjectFieldSelector } from './typedefs/io.k8s.api.core.v1.ObjectFieldSelector';
import { NodeSystemInfo as corev1NodeSystemInfo } from './typedefs/io.k8s.api.core.v1.NodeSystemInfo';
import { NodeStatus as corev1NodeStatus } from './typedefs/io.k8s.api.core.v1.NodeStatus';
import { NodeSpec as corev1NodeSpec } from './typedefs/io.k8s.api.core.v1.NodeSpec';
import { NodeSelectorTerm as corev1NodeSelectorTerm } from './typedefs/io.k8s.api.core.v1.NodeSelectorTerm';
import { NodeSelectorRequirement as corev1NodeSelectorRequirement } from './typedefs/io.k8s.api.core.v1.NodeSelectorRequirement';
import { NodeSelector as corev1NodeSelector } from './typedefs/io.k8s.api.core.v1.NodeSelector';
import { NodeList as corev1NodeList } from './typedefs/io.k8s.api.core.v1.NodeList';
import { NodeDaemonEndpoints as corev1NodeDaemonEndpoints } from './typedefs/io.k8s.api.core.v1.NodeDaemonEndpoints';
import { NodeConfigStatus as corev1NodeConfigStatus } from './typedefs/io.k8s.api.core.v1.NodeConfigStatus';
import { NodeConfigSource as corev1NodeConfigSource } from './typedefs/io.k8s.api.core.v1.NodeConfigSource';
import { NodeCondition as corev1NodeCondition } from './typedefs/io.k8s.api.core.v1.NodeCondition';
import { NodeAffinity as corev1NodeAffinity } from './typedefs/io.k8s.api.core.v1.NodeAffinity';
import { NodeAddress as corev1NodeAddress } from './typedefs/io.k8s.api.core.v1.NodeAddress';
import { Node as corev1Node } from './typedefs/io.k8s.api.core.v1.Node';
import { NamespaceStatus as corev1NamespaceStatus } from './typedefs/io.k8s.api.core.v1.NamespaceStatus';
import { NamespaceSpec as corev1NamespaceSpec } from './typedefs/io.k8s.api.core.v1.NamespaceSpec';
import { NamespaceList as corev1NamespaceList } from './typedefs/io.k8s.api.core.v1.NamespaceList';
import { Namespace as corev1Namespace } from './typedefs/io.k8s.api.core.v1.Namespace';
import { NFSVolumeSource as corev1NFSVolumeSource } from './typedefs/io.k8s.api.core.v1.NFSVolumeSource';
import { LocalVolumeSource as corev1LocalVolumeSource } from './typedefs/io.k8s.api.core.v1.LocalVolumeSource';
import { LocalObjectReference as corev1LocalObjectReference } from './typedefs/io.k8s.api.core.v1.LocalObjectReference';
import { LoadBalancerStatus as corev1LoadBalancerStatus } from './typedefs/io.k8s.api.core.v1.LoadBalancerStatus';
import { LoadBalancerIngress as corev1LoadBalancerIngress } from './typedefs/io.k8s.api.core.v1.LoadBalancerIngress';
import { LimitRangeSpec as corev1LimitRangeSpec } from './typedefs/io.k8s.api.core.v1.LimitRangeSpec';
import { LimitRangeList as corev1LimitRangeList } from './typedefs/io.k8s.api.core.v1.LimitRangeList';
import { LimitRangeItem as corev1LimitRangeItem } from './typedefs/io.k8s.api.core.v1.LimitRangeItem';
import { LimitRange as corev1LimitRange } from './typedefs/io.k8s.api.core.v1.LimitRange';
import { Lifecycle as corev1Lifecycle } from './typedefs/io.k8s.api.core.v1.Lifecycle';
import { KeyToPath as corev1KeyToPath } from './typedefs/io.k8s.api.core.v1.KeyToPath';
import { ISCSIVolumeSource as corev1ISCSIVolumeSource } from './typedefs/io.k8s.api.core.v1.ISCSIVolumeSource';
import { ISCSIPersistentVolumeSource as corev1ISCSIPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.ISCSIPersistentVolumeSource';
import { HostPathVolumeSource as corev1HostPathVolumeSource } from './typedefs/io.k8s.api.core.v1.HostPathVolumeSource';
import { HostAlias as corev1HostAlias } from './typedefs/io.k8s.api.core.v1.HostAlias';
import { Handler as corev1Handler } from './typedefs/io.k8s.api.core.v1.Handler';
import { HTTPHeader as corev1HTTPHeader } from './typedefs/io.k8s.api.core.v1.HTTPHeader';
import { HTTPGetAction as corev1HTTPGetAction } from './typedefs/io.k8s.api.core.v1.HTTPGetAction';
import { GlusterfsVolumeSource as corev1GlusterfsVolumeSource } from './typedefs/io.k8s.api.core.v1.GlusterfsVolumeSource';
import { GlusterfsPersistentVolumeSource as corev1GlusterfsPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.GlusterfsPersistentVolumeSource';
import { GitRepoVolumeSource as corev1GitRepoVolumeSource } from './typedefs/io.k8s.api.core.v1.GitRepoVolumeSource';
import { GCEPersistentDiskVolumeSource as corev1GCEPersistentDiskVolumeSource } from './typedefs/io.k8s.api.core.v1.GCEPersistentDiskVolumeSource';
import { FlockerVolumeSource as corev1FlockerVolumeSource } from './typedefs/io.k8s.api.core.v1.FlockerVolumeSource';
import { FlexVolumeSource as corev1FlexVolumeSource } from './typedefs/io.k8s.api.core.v1.FlexVolumeSource';
import { FlexPersistentVolumeSource as corev1FlexPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.FlexPersistentVolumeSource';
import { FCVolumeSource as corev1FCVolumeSource } from './typedefs/io.k8s.api.core.v1.FCVolumeSource';
import { ExecAction as corev1ExecAction } from './typedefs/io.k8s.api.core.v1.ExecAction';
import { EventSource as corev1EventSource } from './typedefs/io.k8s.api.core.v1.EventSource';
import { EventSeries as corev1EventSeries } from './typedefs/io.k8s.api.core.v1.EventSeries';
import { EventList as corev1EventList } from './typedefs/io.k8s.api.core.v1.EventList';
import { Event as corev1Event } from './typedefs/io.k8s.api.core.v1.Event';
import { EnvVarSource as corev1EnvVarSource } from './typedefs/io.k8s.api.core.v1.EnvVarSource';
import { EnvVar as corev1EnvVar } from './typedefs/io.k8s.api.core.v1.EnvVar';
import { EnvFromSource as corev1EnvFromSource } from './typedefs/io.k8s.api.core.v1.EnvFromSource';
import { EndpointsList as corev1EndpointsList } from './typedefs/io.k8s.api.core.v1.EndpointsList';
import { Endpoints as corev1Endpoints } from './typedefs/io.k8s.api.core.v1.Endpoints';
import { EndpointSubset as corev1EndpointSubset } from './typedefs/io.k8s.api.core.v1.EndpointSubset';
import { EndpointPort as corev1EndpointPort } from './typedefs/io.k8s.api.core.v1.EndpointPort';
import { EndpointAddress as corev1EndpointAddress } from './typedefs/io.k8s.api.core.v1.EndpointAddress';
import { EmptyDirVolumeSource as corev1EmptyDirVolumeSource } from './typedefs/io.k8s.api.core.v1.EmptyDirVolumeSource';
import { DownwardAPIVolumeSource as corev1DownwardAPIVolumeSource } from './typedefs/io.k8s.api.core.v1.DownwardAPIVolumeSource';
import { DownwardAPIVolumeFile as corev1DownwardAPIVolumeFile } from './typedefs/io.k8s.api.core.v1.DownwardAPIVolumeFile';
import { DownwardAPIProjection as corev1DownwardAPIProjection } from './typedefs/io.k8s.api.core.v1.DownwardAPIProjection';
import { DaemonEndpoint as corev1DaemonEndpoint } from './typedefs/io.k8s.api.core.v1.DaemonEndpoint';
import { ContainerStatus as corev1ContainerStatus } from './typedefs/io.k8s.api.core.v1.ContainerStatus';
import { ContainerStateWaiting as corev1ContainerStateWaiting } from './typedefs/io.k8s.api.core.v1.ContainerStateWaiting';
import { ContainerStateTerminated as corev1ContainerStateTerminated } from './typedefs/io.k8s.api.core.v1.ContainerStateTerminated';
import { ContainerStateRunning as corev1ContainerStateRunning } from './typedefs/io.k8s.api.core.v1.ContainerStateRunning';
import { ContainerState as corev1ContainerState } from './typedefs/io.k8s.api.core.v1.ContainerState';
import { ContainerPort as corev1ContainerPort } from './typedefs/io.k8s.api.core.v1.ContainerPort';
import { ContainerImage as corev1ContainerImage } from './typedefs/io.k8s.api.core.v1.ContainerImage';
import { Container as corev1Container } from './typedefs/io.k8s.api.core.v1.Container';
import { ConfigMapVolumeSource as corev1ConfigMapVolumeSource } from './typedefs/io.k8s.api.core.v1.ConfigMapVolumeSource';
import { ConfigMapProjection as corev1ConfigMapProjection } from './typedefs/io.k8s.api.core.v1.ConfigMapProjection';
import { ConfigMapNodeConfigSource as corev1ConfigMapNodeConfigSource } from './typedefs/io.k8s.api.core.v1.ConfigMapNodeConfigSource';
import { ConfigMapList as corev1ConfigMapList } from './typedefs/io.k8s.api.core.v1.ConfigMapList';
import { ConfigMapKeySelector as corev1ConfigMapKeySelector } from './typedefs/io.k8s.api.core.v1.ConfigMapKeySelector';
import { ConfigMapEnvSource as corev1ConfigMapEnvSource } from './typedefs/io.k8s.api.core.v1.ConfigMapEnvSource';
import { ConfigMap as corev1ConfigMap } from './typedefs/io.k8s.api.core.v1.ConfigMap';
import { ComponentStatusList as corev1ComponentStatusList } from './typedefs/io.k8s.api.core.v1.ComponentStatusList';
import { ComponentStatus as corev1ComponentStatus } from './typedefs/io.k8s.api.core.v1.ComponentStatus';
import { ComponentCondition as corev1ComponentCondition } from './typedefs/io.k8s.api.core.v1.ComponentCondition';
import { ClientIPConfig as corev1ClientIPConfig } from './typedefs/io.k8s.api.core.v1.ClientIPConfig';
import { CinderVolumeSource as corev1CinderVolumeSource } from './typedefs/io.k8s.api.core.v1.CinderVolumeSource';
import { CinderPersistentVolumeSource as corev1CinderPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.CinderPersistentVolumeSource';
import { CephFSVolumeSource as corev1CephFSVolumeSource } from './typedefs/io.k8s.api.core.v1.CephFSVolumeSource';
import { CephFSPersistentVolumeSource as corev1CephFSPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.CephFSPersistentVolumeSource';
import { Capabilities as corev1Capabilities } from './typedefs/io.k8s.api.core.v1.Capabilities';
import { CSIVolumeSource as corev1CSIVolumeSource } from './typedefs/io.k8s.api.core.v1.CSIVolumeSource';
import { CSIPersistentVolumeSource as corev1CSIPersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.CSIPersistentVolumeSource';
import { Binding as corev1Binding } from './typedefs/io.k8s.api.core.v1.Binding';
import { AzureFileVolumeSource as corev1AzureFileVolumeSource } from './typedefs/io.k8s.api.core.v1.AzureFileVolumeSource';
import { AzureFilePersistentVolumeSource as corev1AzureFilePersistentVolumeSource } from './typedefs/io.k8s.api.core.v1.AzureFilePersistentVolumeSource';
import { AzureDiskVolumeSource as corev1AzureDiskVolumeSource } from './typedefs/io.k8s.api.core.v1.AzureDiskVolumeSource';
import { AttachedVolume as corev1AttachedVolume } from './typedefs/io.k8s.api.core.v1.AttachedVolume';
import { Affinity as corev1Affinity } from './typedefs/io.k8s.api.core.v1.Affinity';
import { AWSElasticBlockStoreVolumeSource as corev1AWSElasticBlockStoreVolumeSource } from './typedefs/io.k8s.api.core.v1.AWSElasticBlockStoreVolumeSource';
import { LeaseSpec as coordinationv1beta1LeaseSpec } from './typedefs/io.k8s.api.coordination.v1beta1.LeaseSpec';
import { LeaseList as coordinationv1beta1LeaseList } from './typedefs/io.k8s.api.coordination.v1beta1.LeaseList';
import { Lease as coordinationv1beta1Lease } from './typedefs/io.k8s.api.coordination.v1beta1.Lease';
import { LeaseSpec as coordinationv1LeaseSpec } from './typedefs/io.k8s.api.coordination.v1.LeaseSpec';
import { LeaseList as coordinationv1LeaseList } from './typedefs/io.k8s.api.coordination.v1.LeaseList';
import { Lease as coordinationv1Lease } from './typedefs/io.k8s.api.coordination.v1.Lease';
import { CertificateSigningRequestStatus as certificatesv1beta1CertificateSigningRequestStatus } from './typedefs/io.k8s.api.certificates.v1beta1.CertificateSigningRequestStatus';
import { CertificateSigningRequestSpec as certificatesv1beta1CertificateSigningRequestSpec } from './typedefs/io.k8s.api.certificates.v1beta1.CertificateSigningRequestSpec';
import { CertificateSigningRequestList as certificatesv1beta1CertificateSigningRequestList } from './typedefs/io.k8s.api.certificates.v1beta1.CertificateSigningRequestList';
import { CertificateSigningRequestCondition as certificatesv1beta1CertificateSigningRequestCondition } from './typedefs/io.k8s.api.certificates.v1beta1.CertificateSigningRequestCondition';
import { CertificateSigningRequest as certificatesv1beta1CertificateSigningRequest } from './typedefs/io.k8s.api.certificates.v1beta1.CertificateSigningRequest';
import { JobTemplateSpec as batchv2alpha1JobTemplateSpec } from './typedefs/io.k8s.api.batch.v2alpha1.JobTemplateSpec';
import { CronJobStatus as batchv2alpha1CronJobStatus } from './typedefs/io.k8s.api.batch.v2alpha1.CronJobStatus';
import { CronJobSpec as batchv2alpha1CronJobSpec } from './typedefs/io.k8s.api.batch.v2alpha1.CronJobSpec';
import { CronJobList as batchv2alpha1CronJobList } from './typedefs/io.k8s.api.batch.v2alpha1.CronJobList';
import { CronJob as batchv2alpha1CronJob } from './typedefs/io.k8s.api.batch.v2alpha1.CronJob';
import { JobTemplateSpec as batchv1beta1JobTemplateSpec } from './typedefs/io.k8s.api.batch.v1beta1.JobTemplateSpec';
import { CronJobStatus as batchv1beta1CronJobStatus } from './typedefs/io.k8s.api.batch.v1beta1.CronJobStatus';
import { CronJobSpec as batchv1beta1CronJobSpec } from './typedefs/io.k8s.api.batch.v1beta1.CronJobSpec';
import { CronJobList as batchv1beta1CronJobList } from './typedefs/io.k8s.api.batch.v1beta1.CronJobList';
import { CronJob as batchv1beta1CronJob } from './typedefs/io.k8s.api.batch.v1beta1.CronJob';
import { JobStatus as batchv1JobStatus } from './typedefs/io.k8s.api.batch.v1.JobStatus';
import { JobSpec as batchv1JobSpec } from './typedefs/io.k8s.api.batch.v1.JobSpec';
import { JobList as batchv1JobList } from './typedefs/io.k8s.api.batch.v1.JobList';
import { JobCondition as batchv1JobCondition } from './typedefs/io.k8s.api.batch.v1.JobCondition';
import { Job as batchv1Job } from './typedefs/io.k8s.api.batch.v1.Job';
import { ResourceMetricStatus as autoscalingv2beta2ResourceMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.ResourceMetricStatus';
import { ResourceMetricSource as autoscalingv2beta2ResourceMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta2.ResourceMetricSource';
import { PodsMetricStatus as autoscalingv2beta2PodsMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.PodsMetricStatus';
import { PodsMetricSource as autoscalingv2beta2PodsMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta2.PodsMetricSource';
import { ObjectMetricStatus as autoscalingv2beta2ObjectMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.ObjectMetricStatus';
import { ObjectMetricSource as autoscalingv2beta2ObjectMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta2.ObjectMetricSource';
import { MetricValueStatus as autoscalingv2beta2MetricValueStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.MetricValueStatus';
import { MetricTarget as autoscalingv2beta2MetricTarget } from './typedefs/io.k8s.api.autoscaling.v2beta2.MetricTarget';
import { MetricStatus as autoscalingv2beta2MetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.MetricStatus';
import { MetricSpec as autoscalingv2beta2MetricSpec } from './typedefs/io.k8s.api.autoscaling.v2beta2.MetricSpec';
import { MetricIdentifier as autoscalingv2beta2MetricIdentifier } from './typedefs/io.k8s.api.autoscaling.v2beta2.MetricIdentifier';
import { HorizontalPodAutoscalerStatus as autoscalingv2beta2HorizontalPodAutoscalerStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscalerStatus';
import { HorizontalPodAutoscalerSpec as autoscalingv2beta2HorizontalPodAutoscalerSpec } from './typedefs/io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscalerSpec';
import { HorizontalPodAutoscalerList as autoscalingv2beta2HorizontalPodAutoscalerList } from './typedefs/io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscalerList';
import { HorizontalPodAutoscalerCondition as autoscalingv2beta2HorizontalPodAutoscalerCondition } from './typedefs/io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscalerCondition';
import { HorizontalPodAutoscaler as autoscalingv2beta2HorizontalPodAutoscaler } from './typedefs/io.k8s.api.autoscaling.v2beta2.HorizontalPodAutoscaler';
import { ExternalMetricStatus as autoscalingv2beta2ExternalMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta2.ExternalMetricStatus';
import { ExternalMetricSource as autoscalingv2beta2ExternalMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta2.ExternalMetricSource';
import { CrossVersionObjectReference as autoscalingv2beta2CrossVersionObjectReference } from './typedefs/io.k8s.api.autoscaling.v2beta2.CrossVersionObjectReference';
import { ResourceMetricStatus as autoscalingv2beta1ResourceMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.ResourceMetricStatus';
import { ResourceMetricSource as autoscalingv2beta1ResourceMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta1.ResourceMetricSource';
import { PodsMetricStatus as autoscalingv2beta1PodsMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.PodsMetricStatus';
import { PodsMetricSource as autoscalingv2beta1PodsMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta1.PodsMetricSource';
import { ObjectMetricStatus as autoscalingv2beta1ObjectMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.ObjectMetricStatus';
import { ObjectMetricSource as autoscalingv2beta1ObjectMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta1.ObjectMetricSource';
import { MetricStatus as autoscalingv2beta1MetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.MetricStatus';
import { MetricSpec as autoscalingv2beta1MetricSpec } from './typedefs/io.k8s.api.autoscaling.v2beta1.MetricSpec';
import { HorizontalPodAutoscalerStatus as autoscalingv2beta1HorizontalPodAutoscalerStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscalerStatus';
import { HorizontalPodAutoscalerSpec as autoscalingv2beta1HorizontalPodAutoscalerSpec } from './typedefs/io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscalerSpec';
import { HorizontalPodAutoscalerList as autoscalingv2beta1HorizontalPodAutoscalerList } from './typedefs/io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscalerList';
import { HorizontalPodAutoscalerCondition as autoscalingv2beta1HorizontalPodAutoscalerCondition } from './typedefs/io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscalerCondition';
import { HorizontalPodAutoscaler as autoscalingv2beta1HorizontalPodAutoscaler } from './typedefs/io.k8s.api.autoscaling.v2beta1.HorizontalPodAutoscaler';
import { ExternalMetricStatus as autoscalingv2beta1ExternalMetricStatus } from './typedefs/io.k8s.api.autoscaling.v2beta1.ExternalMetricStatus';
import { ExternalMetricSource as autoscalingv2beta1ExternalMetricSource } from './typedefs/io.k8s.api.autoscaling.v2beta1.ExternalMetricSource';
import { CrossVersionObjectReference as autoscalingv2beta1CrossVersionObjectReference } from './typedefs/io.k8s.api.autoscaling.v2beta1.CrossVersionObjectReference';
import { ScaleStatus as autoscalingv1ScaleStatus } from './typedefs/io.k8s.api.autoscaling.v1.ScaleStatus';
import { ScaleSpec as autoscalingv1ScaleSpec } from './typedefs/io.k8s.api.autoscaling.v1.ScaleSpec';
import { Scale as autoscalingv1Scale } from './typedefs/io.k8s.api.autoscaling.v1.Scale';
import { HorizontalPodAutoscalerStatus as autoscalingv1HorizontalPodAutoscalerStatus } from './typedefs/io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerStatus';
import { HorizontalPodAutoscalerSpec as autoscalingv1HorizontalPodAutoscalerSpec } from './typedefs/io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerSpec';
import { HorizontalPodAutoscalerList as autoscalingv1HorizontalPodAutoscalerList } from './typedefs/io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerList';
import { HorizontalPodAutoscaler as autoscalingv1HorizontalPodAutoscaler } from './typedefs/io.k8s.api.autoscaling.v1.HorizontalPodAutoscaler';
import { CrossVersionObjectReference as autoscalingv1CrossVersionObjectReference } from './typedefs/io.k8s.api.autoscaling.v1.CrossVersionObjectReference';
import { SubjectRulesReviewStatus as authorizationv1beta1SubjectRulesReviewStatus } from './typedefs/io.k8s.api.authorization.v1beta1.SubjectRulesReviewStatus';
import { SubjectAccessReviewStatus as authorizationv1beta1SubjectAccessReviewStatus } from './typedefs/io.k8s.api.authorization.v1beta1.SubjectAccessReviewStatus';
import { SubjectAccessReviewSpec as authorizationv1beta1SubjectAccessReviewSpec } from './typedefs/io.k8s.api.authorization.v1beta1.SubjectAccessReviewSpec';
import { SubjectAccessReview as authorizationv1beta1SubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1beta1.SubjectAccessReview';
import { SelfSubjectRulesReviewSpec as authorizationv1beta1SelfSubjectRulesReviewSpec } from './typedefs/io.k8s.api.authorization.v1beta1.SelfSubjectRulesReviewSpec';
import { SelfSubjectRulesReview as authorizationv1beta1SelfSubjectRulesReview } from './typedefs/io.k8s.api.authorization.v1beta1.SelfSubjectRulesReview';
import { SelfSubjectAccessReviewSpec as authorizationv1beta1SelfSubjectAccessReviewSpec } from './typedefs/io.k8s.api.authorization.v1beta1.SelfSubjectAccessReviewSpec';
import { SelfSubjectAccessReview as authorizationv1beta1SelfSubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1beta1.SelfSubjectAccessReview';
import { ResourceRule as authorizationv1beta1ResourceRule } from './typedefs/io.k8s.api.authorization.v1beta1.ResourceRule';
import { ResourceAttributes as authorizationv1beta1ResourceAttributes } from './typedefs/io.k8s.api.authorization.v1beta1.ResourceAttributes';
import { NonResourceRule as authorizationv1beta1NonResourceRule } from './typedefs/io.k8s.api.authorization.v1beta1.NonResourceRule';
import { NonResourceAttributes as authorizationv1beta1NonResourceAttributes } from './typedefs/io.k8s.api.authorization.v1beta1.NonResourceAttributes';
import { LocalSubjectAccessReview as authorizationv1beta1LocalSubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1beta1.LocalSubjectAccessReview';
import { SubjectRulesReviewStatus as authorizationv1SubjectRulesReviewStatus } from './typedefs/io.k8s.api.authorization.v1.SubjectRulesReviewStatus';
import { SubjectAccessReviewStatus as authorizationv1SubjectAccessReviewStatus } from './typedefs/io.k8s.api.authorization.v1.SubjectAccessReviewStatus';
import { SubjectAccessReviewSpec as authorizationv1SubjectAccessReviewSpec } from './typedefs/io.k8s.api.authorization.v1.SubjectAccessReviewSpec';
import { SubjectAccessReview as authorizationv1SubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1.SubjectAccessReview';
import { SelfSubjectRulesReviewSpec as authorizationv1SelfSubjectRulesReviewSpec } from './typedefs/io.k8s.api.authorization.v1.SelfSubjectRulesReviewSpec';
import { SelfSubjectRulesReview as authorizationv1SelfSubjectRulesReview } from './typedefs/io.k8s.api.authorization.v1.SelfSubjectRulesReview';
import { SelfSubjectAccessReviewSpec as authorizationv1SelfSubjectAccessReviewSpec } from './typedefs/io.k8s.api.authorization.v1.SelfSubjectAccessReviewSpec';
import { SelfSubjectAccessReview as authorizationv1SelfSubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1.SelfSubjectAccessReview';
import { ResourceRule as authorizationv1ResourceRule } from './typedefs/io.k8s.api.authorization.v1.ResourceRule';
import { ResourceAttributes as authorizationv1ResourceAttributes } from './typedefs/io.k8s.api.authorization.v1.ResourceAttributes';
import { NonResourceRule as authorizationv1NonResourceRule } from './typedefs/io.k8s.api.authorization.v1.NonResourceRule';
import { NonResourceAttributes as authorizationv1NonResourceAttributes } from './typedefs/io.k8s.api.authorization.v1.NonResourceAttributes';
import { LocalSubjectAccessReview as authorizationv1LocalSubjectAccessReview } from './typedefs/io.k8s.api.authorization.v1.LocalSubjectAccessReview';
import { UserInfo as authenticationv1beta1UserInfo } from './typedefs/io.k8s.api.authentication.v1beta1.UserInfo';
import { TokenReviewStatus as authenticationv1beta1TokenReviewStatus } from './typedefs/io.k8s.api.authentication.v1beta1.TokenReviewStatus';
import { TokenReviewSpec as authenticationv1beta1TokenReviewSpec } from './typedefs/io.k8s.api.authentication.v1beta1.TokenReviewSpec';
import { TokenReview as authenticationv1beta1TokenReview } from './typedefs/io.k8s.api.authentication.v1beta1.TokenReview';
import { UserInfo as authenticationv1UserInfo } from './typedefs/io.k8s.api.authentication.v1.UserInfo';
import { TokenReviewStatus as authenticationv1TokenReviewStatus } from './typedefs/io.k8s.api.authentication.v1.TokenReviewStatus';
import { TokenReviewSpec as authenticationv1TokenReviewSpec } from './typedefs/io.k8s.api.authentication.v1.TokenReviewSpec';
import { TokenReview as authenticationv1TokenReview } from './typedefs/io.k8s.api.authentication.v1.TokenReview';
import { WebhookThrottleConfig as auditregistrationv1alpha1WebhookThrottleConfig } from './typedefs/io.k8s.api.auditregistration.v1alpha1.WebhookThrottleConfig';
import { WebhookClientConfig as auditregistrationv1alpha1WebhookClientConfig } from './typedefs/io.k8s.api.auditregistration.v1alpha1.WebhookClientConfig';
import { Webhook as auditregistrationv1alpha1Webhook } from './typedefs/io.k8s.api.auditregistration.v1alpha1.Webhook';
import { ServiceReference as auditregistrationv1alpha1ServiceReference } from './typedefs/io.k8s.api.auditregistration.v1alpha1.ServiceReference';
import { Policy as auditregistrationv1alpha1Policy } from './typedefs/io.k8s.api.auditregistration.v1alpha1.Policy';
import { AuditSinkSpec as auditregistrationv1alpha1AuditSinkSpec } from './typedefs/io.k8s.api.auditregistration.v1alpha1.AuditSinkSpec';
import { AuditSinkList as auditregistrationv1alpha1AuditSinkList } from './typedefs/io.k8s.api.auditregistration.v1alpha1.AuditSinkList';
import { AuditSink as auditregistrationv1alpha1AuditSink } from './typedefs/io.k8s.api.auditregistration.v1alpha1.AuditSink';
import { StatefulSetUpdateStrategy as appsv1beta2StatefulSetUpdateStrategy } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSetUpdateStrategy';
import { StatefulSetStatus as appsv1beta2StatefulSetStatus } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSetStatus';
import { StatefulSetSpec as appsv1beta2StatefulSetSpec } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSetSpec';
import { StatefulSetList as appsv1beta2StatefulSetList } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSetList';
import { StatefulSetCondition as appsv1beta2StatefulSetCondition } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSetCondition';
import { StatefulSet as appsv1beta2StatefulSet } from './typedefs/io.k8s.api.apps.v1beta2.StatefulSet';
import { ScaleStatus as appsv1beta2ScaleStatus } from './typedefs/io.k8s.api.apps.v1beta2.ScaleStatus';
import { ScaleSpec as appsv1beta2ScaleSpec } from './typedefs/io.k8s.api.apps.v1beta2.ScaleSpec';
import { Scale as appsv1beta2Scale } from './typedefs/io.k8s.api.apps.v1beta2.Scale';
import { RollingUpdateStatefulSetStrategy as appsv1beta2RollingUpdateStatefulSetStrategy } from './typedefs/io.k8s.api.apps.v1beta2.RollingUpdateStatefulSetStrategy';
import { RollingUpdateDeployment as appsv1beta2RollingUpdateDeployment } from './typedefs/io.k8s.api.apps.v1beta2.RollingUpdateDeployment';
import { RollingUpdateDaemonSet as appsv1beta2RollingUpdateDaemonSet } from './typedefs/io.k8s.api.apps.v1beta2.RollingUpdateDaemonSet';
import { ReplicaSetStatus as appsv1beta2ReplicaSetStatus } from './typedefs/io.k8s.api.apps.v1beta2.ReplicaSetStatus';
import { ReplicaSetSpec as appsv1beta2ReplicaSetSpec } from './typedefs/io.k8s.api.apps.v1beta2.ReplicaSetSpec';
import { ReplicaSetList as appsv1beta2ReplicaSetList } from './typedefs/io.k8s.api.apps.v1beta2.ReplicaSetList';
import { ReplicaSetCondition as appsv1beta2ReplicaSetCondition } from './typedefs/io.k8s.api.apps.v1beta2.ReplicaSetCondition';
import { ReplicaSet as appsv1beta2ReplicaSet } from './typedefs/io.k8s.api.apps.v1beta2.ReplicaSet';
import { DeploymentStrategy as appsv1beta2DeploymentStrategy } from './typedefs/io.k8s.api.apps.v1beta2.DeploymentStrategy';
import { DeploymentStatus as appsv1beta2DeploymentStatus } from './typedefs/io.k8s.api.apps.v1beta2.DeploymentStatus';
import { DeploymentSpec as appsv1beta2DeploymentSpec } from './typedefs/io.k8s.api.apps.v1beta2.DeploymentSpec';
import { DeploymentList as appsv1beta2DeploymentList } from './typedefs/io.k8s.api.apps.v1beta2.DeploymentList';
import { DeploymentCondition as appsv1beta2DeploymentCondition } from './typedefs/io.k8s.api.apps.v1beta2.DeploymentCondition';
import { Deployment as appsv1beta2Deployment } from './typedefs/io.k8s.api.apps.v1beta2.Deployment';
import { DaemonSetUpdateStrategy as appsv1beta2DaemonSetUpdateStrategy } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSetUpdateStrategy';
import { DaemonSetStatus as appsv1beta2DaemonSetStatus } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSetStatus';
import { DaemonSetSpec as appsv1beta2DaemonSetSpec } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSetSpec';
import { DaemonSetList as appsv1beta2DaemonSetList } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSetList';
import { DaemonSetCondition as appsv1beta2DaemonSetCondition } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSetCondition';
import { DaemonSet as appsv1beta2DaemonSet } from './typedefs/io.k8s.api.apps.v1beta2.DaemonSet';
import { ControllerRevisionList as appsv1beta2ControllerRevisionList } from './typedefs/io.k8s.api.apps.v1beta2.ControllerRevisionList';
import { ControllerRevision as appsv1beta2ControllerRevision } from './typedefs/io.k8s.api.apps.v1beta2.ControllerRevision';
import { StatefulSetUpdateStrategy as appsv1beta1StatefulSetUpdateStrategy } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSetUpdateStrategy';
import { StatefulSetStatus as appsv1beta1StatefulSetStatus } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSetStatus';
import { StatefulSetSpec as appsv1beta1StatefulSetSpec } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSetSpec';
import { StatefulSetList as appsv1beta1StatefulSetList } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSetList';
import { StatefulSetCondition as appsv1beta1StatefulSetCondition } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSetCondition';
import { StatefulSet as appsv1beta1StatefulSet } from './typedefs/io.k8s.api.apps.v1beta1.StatefulSet';
import { ScaleStatus as appsv1beta1ScaleStatus } from './typedefs/io.k8s.api.apps.v1beta1.ScaleStatus';
import { ScaleSpec as appsv1beta1ScaleSpec } from './typedefs/io.k8s.api.apps.v1beta1.ScaleSpec';
import { Scale as appsv1beta1Scale } from './typedefs/io.k8s.api.apps.v1beta1.Scale';
import { RollingUpdateStatefulSetStrategy as appsv1beta1RollingUpdateStatefulSetStrategy } from './typedefs/io.k8s.api.apps.v1beta1.RollingUpdateStatefulSetStrategy';
import { RollingUpdateDeployment as appsv1beta1RollingUpdateDeployment } from './typedefs/io.k8s.api.apps.v1beta1.RollingUpdateDeployment';
import { RollbackConfig as appsv1beta1RollbackConfig } from './typedefs/io.k8s.api.apps.v1beta1.RollbackConfig';
import { DeploymentStrategy as appsv1beta1DeploymentStrategy } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentStrategy';
import { DeploymentStatus as appsv1beta1DeploymentStatus } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentStatus';
import { DeploymentSpec as appsv1beta1DeploymentSpec } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentSpec';
import { DeploymentRollback as appsv1beta1DeploymentRollback } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentRollback';
import { DeploymentList as appsv1beta1DeploymentList } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentList';
import { DeploymentCondition as appsv1beta1DeploymentCondition } from './typedefs/io.k8s.api.apps.v1beta1.DeploymentCondition';
import { Deployment as appsv1beta1Deployment } from './typedefs/io.k8s.api.apps.v1beta1.Deployment';
import { ControllerRevisionList as appsv1beta1ControllerRevisionList } from './typedefs/io.k8s.api.apps.v1beta1.ControllerRevisionList';
import { ControllerRevision as appsv1beta1ControllerRevision } from './typedefs/io.k8s.api.apps.v1beta1.ControllerRevision';
import { StatefulSetUpdateStrategy as appsv1StatefulSetUpdateStrategy } from './typedefs/io.k8s.api.apps.v1.StatefulSetUpdateStrategy';
import { StatefulSetStatus as appsv1StatefulSetStatus } from './typedefs/io.k8s.api.apps.v1.StatefulSetStatus';
import { StatefulSetSpec as appsv1StatefulSetSpec } from './typedefs/io.k8s.api.apps.v1.StatefulSetSpec';
import { StatefulSetList as appsv1StatefulSetList } from './typedefs/io.k8s.api.apps.v1.StatefulSetList';
import { StatefulSetCondition as appsv1StatefulSetCondition } from './typedefs/io.k8s.api.apps.v1.StatefulSetCondition';
import { StatefulSet as appsv1StatefulSet } from './typedefs/io.k8s.api.apps.v1.StatefulSet';
import { RollingUpdateStatefulSetStrategy as appsv1RollingUpdateStatefulSetStrategy } from './typedefs/io.k8s.api.apps.v1.RollingUpdateStatefulSetStrategy';
import { RollingUpdateDeployment as appsv1RollingUpdateDeployment } from './typedefs/io.k8s.api.apps.v1.RollingUpdateDeployment';
import { RollingUpdateDaemonSet as appsv1RollingUpdateDaemonSet } from './typedefs/io.k8s.api.apps.v1.RollingUpdateDaemonSet';
import { ReplicaSetStatus as appsv1ReplicaSetStatus } from './typedefs/io.k8s.api.apps.v1.ReplicaSetStatus';
import { ReplicaSetSpec as appsv1ReplicaSetSpec } from './typedefs/io.k8s.api.apps.v1.ReplicaSetSpec';
import { ReplicaSetList as appsv1ReplicaSetList } from './typedefs/io.k8s.api.apps.v1.ReplicaSetList';
import { ReplicaSetCondition as appsv1ReplicaSetCondition } from './typedefs/io.k8s.api.apps.v1.ReplicaSetCondition';
import { ReplicaSet as appsv1ReplicaSet } from './typedefs/io.k8s.api.apps.v1.ReplicaSet';
import { DeploymentStrategy as appsv1DeploymentStrategy } from './typedefs/io.k8s.api.apps.v1.DeploymentStrategy';
import { DeploymentStatus as appsv1DeploymentStatus } from './typedefs/io.k8s.api.apps.v1.DeploymentStatus';
import { DeploymentSpec as appsv1DeploymentSpec } from './typedefs/io.k8s.api.apps.v1.DeploymentSpec';
import { DeploymentList as appsv1DeploymentList } from './typedefs/io.k8s.api.apps.v1.DeploymentList';
import { DeploymentCondition as appsv1DeploymentCondition } from './typedefs/io.k8s.api.apps.v1.DeploymentCondition';
import { Deployment as appsv1Deployment } from './typedefs/io.k8s.api.apps.v1.Deployment';
import { DaemonSetUpdateStrategy as appsv1DaemonSetUpdateStrategy } from './typedefs/io.k8s.api.apps.v1.DaemonSetUpdateStrategy';
import { DaemonSetStatus as appsv1DaemonSetStatus } from './typedefs/io.k8s.api.apps.v1.DaemonSetStatus';
import { DaemonSetSpec as appsv1DaemonSetSpec } from './typedefs/io.k8s.api.apps.v1.DaemonSetSpec';
import { DaemonSetList as appsv1DaemonSetList } from './typedefs/io.k8s.api.apps.v1.DaemonSetList';
import { DaemonSetCondition as appsv1DaemonSetCondition } from './typedefs/io.k8s.api.apps.v1.DaemonSetCondition';
import { DaemonSet as appsv1DaemonSet } from './typedefs/io.k8s.api.apps.v1.DaemonSet';
import { ControllerRevisionList as appsv1ControllerRevisionList } from './typedefs/io.k8s.api.apps.v1.ControllerRevisionList';
import { ControllerRevision as appsv1ControllerRevision } from './typedefs/io.k8s.api.apps.v1.ControllerRevision';
import { WebhookClientConfig as admissionregistrationv1beta1WebhookClientConfig } from './typedefs/io.k8s.api.admissionregistration.v1beta1.WebhookClientConfig';
import { ValidatingWebhookConfigurationList as admissionregistrationv1beta1ValidatingWebhookConfigurationList } from './typedefs/io.k8s.api.admissionregistration.v1beta1.ValidatingWebhookConfigurationList';
import { ValidatingWebhookConfiguration as admissionregistrationv1beta1ValidatingWebhookConfiguration } from './typedefs/io.k8s.api.admissionregistration.v1beta1.ValidatingWebhookConfiguration';
import { ValidatingWebhook as admissionregistrationv1beta1ValidatingWebhook } from './typedefs/io.k8s.api.admissionregistration.v1beta1.ValidatingWebhook';
import { ServiceReference as admissionregistrationv1beta1ServiceReference } from './typedefs/io.k8s.api.admissionregistration.v1beta1.ServiceReference';
import { RuleWithOperations as admissionregistrationv1beta1RuleWithOperations } from './typedefs/io.k8s.api.admissionregistration.v1beta1.RuleWithOperations';
import { MutatingWebhookConfigurationList as admissionregistrationv1beta1MutatingWebhookConfigurationList } from './typedefs/io.k8s.api.admissionregistration.v1beta1.MutatingWebhookConfigurationList';
import { MutatingWebhookConfiguration as admissionregistrationv1beta1MutatingWebhookConfiguration } from './typedefs/io.k8s.api.admissionregistration.v1beta1.MutatingWebhookConfiguration';
import { MutatingWebhook as admissionregistrationv1beta1MutatingWebhook } from './typedefs/io.k8s.api.admissionregistration.v1beta1.MutatingWebhook';

// version of kubernetes release these definitions are from
export const version = 'v1.15.1';

export const admissionregistration = {
  v1beta1: {
    MutatingWebhook: admissionregistrationv1beta1MutatingWebhook,
    MutatingWebhookConfiguration: admissionregistrationv1beta1MutatingWebhookConfiguration,
    MutatingWebhookConfigurationList: admissionregistrationv1beta1MutatingWebhookConfigurationList,
    RuleWithOperations: admissionregistrationv1beta1RuleWithOperations,
    ServiceReference: admissionregistrationv1beta1ServiceReference,
    ValidatingWebhook: admissionregistrationv1beta1ValidatingWebhook,
    ValidatingWebhookConfiguration: admissionregistrationv1beta1ValidatingWebhookConfiguration,
    ValidatingWebhookConfigurationList: admissionregistrationv1beta1ValidatingWebhookConfigurationList,
    WebhookClientConfig: admissionregistrationv1beta1WebhookClientConfig,
  },
};

export const apps = {
  v1: {
    ControllerRevision: appsv1ControllerRevision,
    ControllerRevisionList: appsv1ControllerRevisionList,
    DaemonSet: appsv1DaemonSet,
    DaemonSetCondition: appsv1DaemonSetCondition,
    DaemonSetList: appsv1DaemonSetList,
    DaemonSetSpec: appsv1DaemonSetSpec,
    DaemonSetStatus: appsv1DaemonSetStatus,
    DaemonSetUpdateStrategy: appsv1DaemonSetUpdateStrategy,
    Deployment: appsv1Deployment,
    DeploymentCondition: appsv1DeploymentCondition,
    DeploymentList: appsv1DeploymentList,
    DeploymentSpec: appsv1DeploymentSpec,
    DeploymentStatus: appsv1DeploymentStatus,
    DeploymentStrategy: appsv1DeploymentStrategy,
    ReplicaSet: appsv1ReplicaSet,
    ReplicaSetCondition: appsv1ReplicaSetCondition,
    ReplicaSetList: appsv1ReplicaSetList,
    ReplicaSetSpec: appsv1ReplicaSetSpec,
    ReplicaSetStatus: appsv1ReplicaSetStatus,
    RollingUpdateDaemonSet: appsv1RollingUpdateDaemonSet,
    RollingUpdateDeployment: appsv1RollingUpdateDeployment,
    RollingUpdateStatefulSetStrategy: appsv1RollingUpdateStatefulSetStrategy,
    StatefulSet: appsv1StatefulSet,
    StatefulSetCondition: appsv1StatefulSetCondition,
    StatefulSetList: appsv1StatefulSetList,
    StatefulSetSpec: appsv1StatefulSetSpec,
    StatefulSetStatus: appsv1StatefulSetStatus,
    StatefulSetUpdateStrategy: appsv1StatefulSetUpdateStrategy,
  },
  v1beta1: {
    ControllerRevision: appsv1beta1ControllerRevision,
    ControllerRevisionList: appsv1beta1ControllerRevisionList,
    Deployment: appsv1beta1Deployment,
    DeploymentCondition: appsv1beta1DeploymentCondition,
    DeploymentList: appsv1beta1DeploymentList,
    DeploymentRollback: appsv1beta1DeploymentRollback,
    DeploymentSpec: appsv1beta1DeploymentSpec,
    DeploymentStatus: appsv1beta1DeploymentStatus,
    DeploymentStrategy: appsv1beta1DeploymentStrategy,
    RollbackConfig: appsv1beta1RollbackConfig,
    RollingUpdateDeployment: appsv1beta1RollingUpdateDeployment,
    RollingUpdateStatefulSetStrategy: appsv1beta1RollingUpdateStatefulSetStrategy,
    Scale: appsv1beta1Scale,
    ScaleSpec: appsv1beta1ScaleSpec,
    ScaleStatus: appsv1beta1ScaleStatus,
    StatefulSet: appsv1beta1StatefulSet,
    StatefulSetCondition: appsv1beta1StatefulSetCondition,
    StatefulSetList: appsv1beta1StatefulSetList,
    StatefulSetSpec: appsv1beta1StatefulSetSpec,
    StatefulSetStatus: appsv1beta1StatefulSetStatus,
    StatefulSetUpdateStrategy: appsv1beta1StatefulSetUpdateStrategy,
  },
  v1beta2: {
    ControllerRevision: appsv1beta2ControllerRevision,
    ControllerRevisionList: appsv1beta2ControllerRevisionList,
    DaemonSet: appsv1beta2DaemonSet,
    DaemonSetCondition: appsv1beta2DaemonSetCondition,
    DaemonSetList: appsv1beta2DaemonSetList,
    DaemonSetSpec: appsv1beta2DaemonSetSpec,
    DaemonSetStatus: appsv1beta2DaemonSetStatus,
    DaemonSetUpdateStrategy: appsv1beta2DaemonSetUpdateStrategy,
    Deployment: appsv1beta2Deployment,
    DeploymentCondition: appsv1beta2DeploymentCondition,
    DeploymentList: appsv1beta2DeploymentList,
    DeploymentSpec: appsv1beta2DeploymentSpec,
    DeploymentStatus: appsv1beta2DeploymentStatus,
    DeploymentStrategy: appsv1beta2DeploymentStrategy,
    ReplicaSet: appsv1beta2ReplicaSet,
    ReplicaSetCondition: appsv1beta2ReplicaSetCondition,
    ReplicaSetList: appsv1beta2ReplicaSetList,
    ReplicaSetSpec: appsv1beta2ReplicaSetSpec,
    ReplicaSetStatus: appsv1beta2ReplicaSetStatus,
    RollingUpdateDaemonSet: appsv1beta2RollingUpdateDaemonSet,
    RollingUpdateDeployment: appsv1beta2RollingUpdateDeployment,
    RollingUpdateStatefulSetStrategy: appsv1beta2RollingUpdateStatefulSetStrategy,
    Scale: appsv1beta2Scale,
    ScaleSpec: appsv1beta2ScaleSpec,
    ScaleStatus: appsv1beta2ScaleStatus,
    StatefulSet: appsv1beta2StatefulSet,
    StatefulSetCondition: appsv1beta2StatefulSetCondition,
    StatefulSetList: appsv1beta2StatefulSetList,
    StatefulSetSpec: appsv1beta2StatefulSetSpec,
    StatefulSetStatus: appsv1beta2StatefulSetStatus,
    StatefulSetUpdateStrategy: appsv1beta2StatefulSetUpdateStrategy,
  },
};

export const auditregistration = {
  v1alpha1: {
    AuditSink: auditregistrationv1alpha1AuditSink,
    AuditSinkList: auditregistrationv1alpha1AuditSinkList,
    AuditSinkSpec: auditregistrationv1alpha1AuditSinkSpec,
    Policy: auditregistrationv1alpha1Policy,
    ServiceReference: auditregistrationv1alpha1ServiceReference,
    Webhook: auditregistrationv1alpha1Webhook,
    WebhookClientConfig: auditregistrationv1alpha1WebhookClientConfig,
    WebhookThrottleConfig: auditregistrationv1alpha1WebhookThrottleConfig,
  },
};

export const authentication = {
  v1: {
    TokenReview: authenticationv1TokenReview,
    TokenReviewSpec: authenticationv1TokenReviewSpec,
    TokenReviewStatus: authenticationv1TokenReviewStatus,
    UserInfo: authenticationv1UserInfo,
  },
  v1beta1: {
    TokenReview: authenticationv1beta1TokenReview,
    TokenReviewSpec: authenticationv1beta1TokenReviewSpec,
    TokenReviewStatus: authenticationv1beta1TokenReviewStatus,
    UserInfo: authenticationv1beta1UserInfo,
  },
};

export const authorization = {
  v1: {
    LocalSubjectAccessReview: authorizationv1LocalSubjectAccessReview,
    NonResourceAttributes: authorizationv1NonResourceAttributes,
    NonResourceRule: authorizationv1NonResourceRule,
    ResourceAttributes: authorizationv1ResourceAttributes,
    ResourceRule: authorizationv1ResourceRule,
    SelfSubjectAccessReview: authorizationv1SelfSubjectAccessReview,
    SelfSubjectAccessReviewSpec: authorizationv1SelfSubjectAccessReviewSpec,
    SelfSubjectRulesReview: authorizationv1SelfSubjectRulesReview,
    SelfSubjectRulesReviewSpec: authorizationv1SelfSubjectRulesReviewSpec,
    SubjectAccessReview: authorizationv1SubjectAccessReview,
    SubjectAccessReviewSpec: authorizationv1SubjectAccessReviewSpec,
    SubjectAccessReviewStatus: authorizationv1SubjectAccessReviewStatus,
    SubjectRulesReviewStatus: authorizationv1SubjectRulesReviewStatus,
  },
  v1beta1: {
    LocalSubjectAccessReview: authorizationv1beta1LocalSubjectAccessReview,
    NonResourceAttributes: authorizationv1beta1NonResourceAttributes,
    NonResourceRule: authorizationv1beta1NonResourceRule,
    ResourceAttributes: authorizationv1beta1ResourceAttributes,
    ResourceRule: authorizationv1beta1ResourceRule,
    SelfSubjectAccessReview: authorizationv1beta1SelfSubjectAccessReview,
    SelfSubjectAccessReviewSpec: authorizationv1beta1SelfSubjectAccessReviewSpec,
    SelfSubjectRulesReview: authorizationv1beta1SelfSubjectRulesReview,
    SelfSubjectRulesReviewSpec: authorizationv1beta1SelfSubjectRulesReviewSpec,
    SubjectAccessReview: authorizationv1beta1SubjectAccessReview,
    SubjectAccessReviewSpec: authorizationv1beta1SubjectAccessReviewSpec,
    SubjectAccessReviewStatus: authorizationv1beta1SubjectAccessReviewStatus,
    SubjectRulesReviewStatus: authorizationv1beta1SubjectRulesReviewStatus,
  },
};

export const autoscaling = {
  v1: {
    CrossVersionObjectReference: autoscalingv1CrossVersionObjectReference,
    HorizontalPodAutoscaler: autoscalingv1HorizontalPodAutoscaler,
    HorizontalPodAutoscalerList: autoscalingv1HorizontalPodAutoscalerList,
    HorizontalPodAutoscalerSpec: autoscalingv1HorizontalPodAutoscalerSpec,
    HorizontalPodAutoscalerStatus: autoscalingv1HorizontalPodAutoscalerStatus,
    Scale: autoscalingv1Scale,
    ScaleSpec: autoscalingv1ScaleSpec,
    ScaleStatus: autoscalingv1ScaleStatus,
  },
  v2beta1: {
    CrossVersionObjectReference: autoscalingv2beta1CrossVersionObjectReference,
    ExternalMetricSource: autoscalingv2beta1ExternalMetricSource,
    ExternalMetricStatus: autoscalingv2beta1ExternalMetricStatus,
    HorizontalPodAutoscaler: autoscalingv2beta1HorizontalPodAutoscaler,
    HorizontalPodAutoscalerCondition: autoscalingv2beta1HorizontalPodAutoscalerCondition,
    HorizontalPodAutoscalerList: autoscalingv2beta1HorizontalPodAutoscalerList,
    HorizontalPodAutoscalerSpec: autoscalingv2beta1HorizontalPodAutoscalerSpec,
    HorizontalPodAutoscalerStatus: autoscalingv2beta1HorizontalPodAutoscalerStatus,
    MetricSpec: autoscalingv2beta1MetricSpec,
    MetricStatus: autoscalingv2beta1MetricStatus,
    ObjectMetricSource: autoscalingv2beta1ObjectMetricSource,
    ObjectMetricStatus: autoscalingv2beta1ObjectMetricStatus,
    PodsMetricSource: autoscalingv2beta1PodsMetricSource,
    PodsMetricStatus: autoscalingv2beta1PodsMetricStatus,
    ResourceMetricSource: autoscalingv2beta1ResourceMetricSource,
    ResourceMetricStatus: autoscalingv2beta1ResourceMetricStatus,
  },
  v2beta2: {
    CrossVersionObjectReference: autoscalingv2beta2CrossVersionObjectReference,
    ExternalMetricSource: autoscalingv2beta2ExternalMetricSource,
    ExternalMetricStatus: autoscalingv2beta2ExternalMetricStatus,
    HorizontalPodAutoscaler: autoscalingv2beta2HorizontalPodAutoscaler,
    HorizontalPodAutoscalerCondition: autoscalingv2beta2HorizontalPodAutoscalerCondition,
    HorizontalPodAutoscalerList: autoscalingv2beta2HorizontalPodAutoscalerList,
    HorizontalPodAutoscalerSpec: autoscalingv2beta2HorizontalPodAutoscalerSpec,
    HorizontalPodAutoscalerStatus: autoscalingv2beta2HorizontalPodAutoscalerStatus,
    MetricIdentifier: autoscalingv2beta2MetricIdentifier,
    MetricSpec: autoscalingv2beta2MetricSpec,
    MetricStatus: autoscalingv2beta2MetricStatus,
    MetricTarget: autoscalingv2beta2MetricTarget,
    MetricValueStatus: autoscalingv2beta2MetricValueStatus,
    ObjectMetricSource: autoscalingv2beta2ObjectMetricSource,
    ObjectMetricStatus: autoscalingv2beta2ObjectMetricStatus,
    PodsMetricSource: autoscalingv2beta2PodsMetricSource,
    PodsMetricStatus: autoscalingv2beta2PodsMetricStatus,
    ResourceMetricSource: autoscalingv2beta2ResourceMetricSource,
    ResourceMetricStatus: autoscalingv2beta2ResourceMetricStatus,
  },
};

export const batch = {
  v1: {
    Job: batchv1Job,
    JobCondition: batchv1JobCondition,
    JobList: batchv1JobList,
    JobSpec: batchv1JobSpec,
    JobStatus: batchv1JobStatus,
  },
  v1beta1: {
    CronJob: batchv1beta1CronJob,
    CronJobList: batchv1beta1CronJobList,
    CronJobSpec: batchv1beta1CronJobSpec,
    CronJobStatus: batchv1beta1CronJobStatus,
    JobTemplateSpec: batchv1beta1JobTemplateSpec,
  },
  v2alpha1: {
    CronJob: batchv2alpha1CronJob,
    CronJobList: batchv2alpha1CronJobList,
    CronJobSpec: batchv2alpha1CronJobSpec,
    CronJobStatus: batchv2alpha1CronJobStatus,
    JobTemplateSpec: batchv2alpha1JobTemplateSpec,
  },
};

export const certificates = {
  v1beta1: {
    CertificateSigningRequest: certificatesv1beta1CertificateSigningRequest,
    CertificateSigningRequestCondition: certificatesv1beta1CertificateSigningRequestCondition,
    CertificateSigningRequestList: certificatesv1beta1CertificateSigningRequestList,
    CertificateSigningRequestSpec: certificatesv1beta1CertificateSigningRequestSpec,
    CertificateSigningRequestStatus: certificatesv1beta1CertificateSigningRequestStatus,
  },
};

export const coordination = {
  v1: {
    Lease: coordinationv1Lease,
    LeaseList: coordinationv1LeaseList,
    LeaseSpec: coordinationv1LeaseSpec,
  },
  v1beta1: {
    Lease: coordinationv1beta1Lease,
    LeaseList: coordinationv1beta1LeaseList,
    LeaseSpec: coordinationv1beta1LeaseSpec,
  },
};

export const core = {
  v1: {
    AWSElasticBlockStoreVolumeSource: corev1AWSElasticBlockStoreVolumeSource,
    Affinity: corev1Affinity,
    AttachedVolume: corev1AttachedVolume,
    AzureDiskVolumeSource: corev1AzureDiskVolumeSource,
    AzureFilePersistentVolumeSource: corev1AzureFilePersistentVolumeSource,
    AzureFileVolumeSource: corev1AzureFileVolumeSource,
    Binding: corev1Binding,
    CSIPersistentVolumeSource: corev1CSIPersistentVolumeSource,
    CSIVolumeSource: corev1CSIVolumeSource,
    Capabilities: corev1Capabilities,
    CephFSPersistentVolumeSource: corev1CephFSPersistentVolumeSource,
    CephFSVolumeSource: corev1CephFSVolumeSource,
    CinderPersistentVolumeSource: corev1CinderPersistentVolumeSource,
    CinderVolumeSource: corev1CinderVolumeSource,
    ClientIPConfig: corev1ClientIPConfig,
    ComponentCondition: corev1ComponentCondition,
    ComponentStatus: corev1ComponentStatus,
    ComponentStatusList: corev1ComponentStatusList,
    ConfigMap: corev1ConfigMap,
    ConfigMapEnvSource: corev1ConfigMapEnvSource,
    ConfigMapKeySelector: corev1ConfigMapKeySelector,
    ConfigMapList: corev1ConfigMapList,
    ConfigMapNodeConfigSource: corev1ConfigMapNodeConfigSource,
    ConfigMapProjection: corev1ConfigMapProjection,
    ConfigMapVolumeSource: corev1ConfigMapVolumeSource,
    Container: corev1Container,
    ContainerImage: corev1ContainerImage,
    ContainerPort: corev1ContainerPort,
    ContainerState: corev1ContainerState,
    ContainerStateRunning: corev1ContainerStateRunning,
    ContainerStateTerminated: corev1ContainerStateTerminated,
    ContainerStateWaiting: corev1ContainerStateWaiting,
    ContainerStatus: corev1ContainerStatus,
    DaemonEndpoint: corev1DaemonEndpoint,
    DownwardAPIProjection: corev1DownwardAPIProjection,
    DownwardAPIVolumeFile: corev1DownwardAPIVolumeFile,
    DownwardAPIVolumeSource: corev1DownwardAPIVolumeSource,
    EmptyDirVolumeSource: corev1EmptyDirVolumeSource,
    EndpointAddress: corev1EndpointAddress,
    EndpointPort: corev1EndpointPort,
    EndpointSubset: corev1EndpointSubset,
    Endpoints: corev1Endpoints,
    EndpointsList: corev1EndpointsList,
    EnvFromSource: corev1EnvFromSource,
    EnvVar: corev1EnvVar,
    EnvVarSource: corev1EnvVarSource,
    Event: corev1Event,
    EventList: corev1EventList,
    EventSeries: corev1EventSeries,
    EventSource: corev1EventSource,
    ExecAction: corev1ExecAction,
    FCVolumeSource: corev1FCVolumeSource,
    FlexPersistentVolumeSource: corev1FlexPersistentVolumeSource,
    FlexVolumeSource: corev1FlexVolumeSource,
    FlockerVolumeSource: corev1FlockerVolumeSource,
    GCEPersistentDiskVolumeSource: corev1GCEPersistentDiskVolumeSource,
    GitRepoVolumeSource: corev1GitRepoVolumeSource,
    GlusterfsPersistentVolumeSource: corev1GlusterfsPersistentVolumeSource,
    GlusterfsVolumeSource: corev1GlusterfsVolumeSource,
    HTTPGetAction: corev1HTTPGetAction,
    HTTPHeader: corev1HTTPHeader,
    Handler: corev1Handler,
    HostAlias: corev1HostAlias,
    HostPathVolumeSource: corev1HostPathVolumeSource,
    ISCSIPersistentVolumeSource: corev1ISCSIPersistentVolumeSource,
    ISCSIVolumeSource: corev1ISCSIVolumeSource,
    KeyToPath: corev1KeyToPath,
    Lifecycle: corev1Lifecycle,
    LimitRange: corev1LimitRange,
    LimitRangeItem: corev1LimitRangeItem,
    LimitRangeList: corev1LimitRangeList,
    LimitRangeSpec: corev1LimitRangeSpec,
    LoadBalancerIngress: corev1LoadBalancerIngress,
    LoadBalancerStatus: corev1LoadBalancerStatus,
    LocalObjectReference: corev1LocalObjectReference,
    LocalVolumeSource: corev1LocalVolumeSource,
    NFSVolumeSource: corev1NFSVolumeSource,
    Namespace: corev1Namespace,
    NamespaceList: corev1NamespaceList,
    NamespaceSpec: corev1NamespaceSpec,
    NamespaceStatus: corev1NamespaceStatus,
    Node: corev1Node,
    NodeAddress: corev1NodeAddress,
    NodeAffinity: corev1NodeAffinity,
    NodeCondition: corev1NodeCondition,
    NodeConfigSource: corev1NodeConfigSource,
    NodeConfigStatus: corev1NodeConfigStatus,
    NodeDaemonEndpoints: corev1NodeDaemonEndpoints,
    NodeList: corev1NodeList,
    NodeSelector: corev1NodeSelector,
    NodeSelectorRequirement: corev1NodeSelectorRequirement,
    NodeSelectorTerm: corev1NodeSelectorTerm,
    NodeSpec: corev1NodeSpec,
    NodeStatus: corev1NodeStatus,
    NodeSystemInfo: corev1NodeSystemInfo,
    ObjectFieldSelector: corev1ObjectFieldSelector,
    ObjectReference: corev1ObjectReference,
    PersistentVolume: corev1PersistentVolume,
    PersistentVolumeClaim: corev1PersistentVolumeClaim,
    PersistentVolumeClaimCondition: corev1PersistentVolumeClaimCondition,
    PersistentVolumeClaimList: corev1PersistentVolumeClaimList,
    PersistentVolumeClaimSpec: corev1PersistentVolumeClaimSpec,
    PersistentVolumeClaimStatus: corev1PersistentVolumeClaimStatus,
    PersistentVolumeClaimVolumeSource: corev1PersistentVolumeClaimVolumeSource,
    PersistentVolumeList: corev1PersistentVolumeList,
    PersistentVolumeSpec: corev1PersistentVolumeSpec,
    PersistentVolumeStatus: corev1PersistentVolumeStatus,
    PhotonPersistentDiskVolumeSource: corev1PhotonPersistentDiskVolumeSource,
    Pod: corev1Pod,
    PodAffinity: corev1PodAffinity,
    PodAffinityTerm: corev1PodAffinityTerm,
    PodAntiAffinity: corev1PodAntiAffinity,
    PodCondition: corev1PodCondition,
    PodDNSConfig: corev1PodDNSConfig,
    PodDNSConfigOption: corev1PodDNSConfigOption,
    PodList: corev1PodList,
    PodReadinessGate: corev1PodReadinessGate,
    PodSecurityContext: corev1PodSecurityContext,
    PodSpec: corev1PodSpec,
    PodStatus: corev1PodStatus,
    PodTemplate: corev1PodTemplate,
    PodTemplateList: corev1PodTemplateList,
    PodTemplateSpec: corev1PodTemplateSpec,
    PortworxVolumeSource: corev1PortworxVolumeSource,
    PreferredSchedulingTerm: corev1PreferredSchedulingTerm,
    Probe: corev1Probe,
    ProjectedVolumeSource: corev1ProjectedVolumeSource,
    QuobyteVolumeSource: corev1QuobyteVolumeSource,
    RBDPersistentVolumeSource: corev1RBDPersistentVolumeSource,
    RBDVolumeSource: corev1RBDVolumeSource,
    ReplicationController: corev1ReplicationController,
    ReplicationControllerCondition: corev1ReplicationControllerCondition,
    ReplicationControllerList: corev1ReplicationControllerList,
    ReplicationControllerSpec: corev1ReplicationControllerSpec,
    ReplicationControllerStatus: corev1ReplicationControllerStatus,
    ResourceFieldSelector: corev1ResourceFieldSelector,
    ResourceQuota: corev1ResourceQuota,
    ResourceQuotaList: corev1ResourceQuotaList,
    ResourceQuotaSpec: corev1ResourceQuotaSpec,
    ResourceQuotaStatus: corev1ResourceQuotaStatus,
    ResourceRequirements: corev1ResourceRequirements,
    SELinuxOptions: corev1SELinuxOptions,
    ScaleIOPersistentVolumeSource: corev1ScaleIOPersistentVolumeSource,
    ScaleIOVolumeSource: corev1ScaleIOVolumeSource,
    ScopeSelector: corev1ScopeSelector,
    ScopedResourceSelectorRequirement: corev1ScopedResourceSelectorRequirement,
    Secret: corev1Secret,
    SecretEnvSource: corev1SecretEnvSource,
    SecretKeySelector: corev1SecretKeySelector,
    SecretList: corev1SecretList,
    SecretProjection: corev1SecretProjection,
    SecretReference: corev1SecretReference,
    SecretVolumeSource: corev1SecretVolumeSource,
    SecurityContext: corev1SecurityContext,
    Service: corev1Service,
    ServiceAccount: corev1ServiceAccount,
    ServiceAccountList: corev1ServiceAccountList,
    ServiceAccountTokenProjection: corev1ServiceAccountTokenProjection,
    ServiceList: corev1ServiceList,
    ServicePort: corev1ServicePort,
    ServiceSpec: corev1ServiceSpec,
    ServiceStatus: corev1ServiceStatus,
    SessionAffinityConfig: corev1SessionAffinityConfig,
    StorageOSPersistentVolumeSource: corev1StorageOSPersistentVolumeSource,
    StorageOSVolumeSource: corev1StorageOSVolumeSource,
    Sysctl: corev1Sysctl,
    TCPSocketAction: corev1TCPSocketAction,
    Taint: corev1Taint,
    Toleration: corev1Toleration,
    TopologySelectorLabelRequirement: corev1TopologySelectorLabelRequirement,
    TopologySelectorTerm: corev1TopologySelectorTerm,
    TypedLocalObjectReference: corev1TypedLocalObjectReference,
    Volume: corev1Volume,
    VolumeDevice: corev1VolumeDevice,
    VolumeMount: corev1VolumeMount,
    VolumeNodeAffinity: corev1VolumeNodeAffinity,
    VolumeProjection: corev1VolumeProjection,
    VsphereVirtualDiskVolumeSource: corev1VsphereVirtualDiskVolumeSource,
    WeightedPodAffinityTerm: corev1WeightedPodAffinityTerm,
    WindowsSecurityContextOptions: corev1WindowsSecurityContextOptions,
  },
};

export const events = {
  v1beta1: {
    Event: eventsv1beta1Event,
    EventList: eventsv1beta1EventList,
    EventSeries: eventsv1beta1EventSeries,
  },
};

export const extensions = {
  v1beta1: {
    AllowedCSIDriver: extensionsv1beta1AllowedCSIDriver,
    AllowedFlexVolume: extensionsv1beta1AllowedFlexVolume,
    AllowedHostPath: extensionsv1beta1AllowedHostPath,
    DaemonSet: extensionsv1beta1DaemonSet,
    DaemonSetCondition: extensionsv1beta1DaemonSetCondition,
    DaemonSetList: extensionsv1beta1DaemonSetList,
    DaemonSetSpec: extensionsv1beta1DaemonSetSpec,
    DaemonSetStatus: extensionsv1beta1DaemonSetStatus,
    DaemonSetUpdateStrategy: extensionsv1beta1DaemonSetUpdateStrategy,
    Deployment: extensionsv1beta1Deployment,
    DeploymentCondition: extensionsv1beta1DeploymentCondition,
    DeploymentList: extensionsv1beta1DeploymentList,
    DeploymentRollback: extensionsv1beta1DeploymentRollback,
    DeploymentSpec: extensionsv1beta1DeploymentSpec,
    DeploymentStatus: extensionsv1beta1DeploymentStatus,
    DeploymentStrategy: extensionsv1beta1DeploymentStrategy,
    FSGroupStrategyOptions: extensionsv1beta1FSGroupStrategyOptions,
    HTTPIngressPath: extensionsv1beta1HTTPIngressPath,
    HTTPIngressRuleValue: extensionsv1beta1HTTPIngressRuleValue,
    HostPortRange: extensionsv1beta1HostPortRange,
    IDRange: extensionsv1beta1IDRange,
    IPBlock: extensionsv1beta1IPBlock,
    Ingress: extensionsv1beta1Ingress,
    IngressBackend: extensionsv1beta1IngressBackend,
    IngressList: extensionsv1beta1IngressList,
    IngressRule: extensionsv1beta1IngressRule,
    IngressSpec: extensionsv1beta1IngressSpec,
    IngressStatus: extensionsv1beta1IngressStatus,
    IngressTLS: extensionsv1beta1IngressTLS,
    NetworkPolicy: extensionsv1beta1NetworkPolicy,
    NetworkPolicyEgressRule: extensionsv1beta1NetworkPolicyEgressRule,
    NetworkPolicyIngressRule: extensionsv1beta1NetworkPolicyIngressRule,
    NetworkPolicyList: extensionsv1beta1NetworkPolicyList,
    NetworkPolicyPeer: extensionsv1beta1NetworkPolicyPeer,
    NetworkPolicyPort: extensionsv1beta1NetworkPolicyPort,
    NetworkPolicySpec: extensionsv1beta1NetworkPolicySpec,
    PodSecurityPolicy: extensionsv1beta1PodSecurityPolicy,
    PodSecurityPolicyList: extensionsv1beta1PodSecurityPolicyList,
    PodSecurityPolicySpec: extensionsv1beta1PodSecurityPolicySpec,
    ReplicaSet: extensionsv1beta1ReplicaSet,
    ReplicaSetCondition: extensionsv1beta1ReplicaSetCondition,
    ReplicaSetList: extensionsv1beta1ReplicaSetList,
    ReplicaSetSpec: extensionsv1beta1ReplicaSetSpec,
    ReplicaSetStatus: extensionsv1beta1ReplicaSetStatus,
    RollbackConfig: extensionsv1beta1RollbackConfig,
    RollingUpdateDaemonSet: extensionsv1beta1RollingUpdateDaemonSet,
    RollingUpdateDeployment: extensionsv1beta1RollingUpdateDeployment,
    RunAsGroupStrategyOptions: extensionsv1beta1RunAsGroupStrategyOptions,
    RunAsUserStrategyOptions: extensionsv1beta1RunAsUserStrategyOptions,
    RuntimeClassStrategyOptions: extensionsv1beta1RuntimeClassStrategyOptions,
    SELinuxStrategyOptions: extensionsv1beta1SELinuxStrategyOptions,
    Scale: extensionsv1beta1Scale,
    ScaleSpec: extensionsv1beta1ScaleSpec,
    ScaleStatus: extensionsv1beta1ScaleStatus,
    SupplementalGroupsStrategyOptions: extensionsv1beta1SupplementalGroupsStrategyOptions,
  },
};

export const networking = {
  v1: {
    IPBlock: networkingv1IPBlock,
    NetworkPolicy: networkingv1NetworkPolicy,
    NetworkPolicyEgressRule: networkingv1NetworkPolicyEgressRule,
    NetworkPolicyIngressRule: networkingv1NetworkPolicyIngressRule,
    NetworkPolicyList: networkingv1NetworkPolicyList,
    NetworkPolicyPeer: networkingv1NetworkPolicyPeer,
    NetworkPolicyPort: networkingv1NetworkPolicyPort,
    NetworkPolicySpec: networkingv1NetworkPolicySpec,
  },
  v1beta1: {
    HTTPIngressPath: networkingv1beta1HTTPIngressPath,
    HTTPIngressRuleValue: networkingv1beta1HTTPIngressRuleValue,
    Ingress: networkingv1beta1Ingress,
    IngressBackend: networkingv1beta1IngressBackend,
    IngressList: networkingv1beta1IngressList,
    IngressRule: networkingv1beta1IngressRule,
    IngressSpec: networkingv1beta1IngressSpec,
    IngressStatus: networkingv1beta1IngressStatus,
    IngressTLS: networkingv1beta1IngressTLS,
  },
};

export const node = {
  v1alpha1: {
    RuntimeClass: nodev1alpha1RuntimeClass,
    RuntimeClassList: nodev1alpha1RuntimeClassList,
    RuntimeClassSpec: nodev1alpha1RuntimeClassSpec,
  },
  v1beta1: {
    RuntimeClass: nodev1beta1RuntimeClass,
    RuntimeClassList: nodev1beta1RuntimeClassList,
  },
};

export const policy = {
  v1beta1: {
    AllowedCSIDriver: policyv1beta1AllowedCSIDriver,
    AllowedFlexVolume: policyv1beta1AllowedFlexVolume,
    AllowedHostPath: policyv1beta1AllowedHostPath,
    Eviction: policyv1beta1Eviction,
    FSGroupStrategyOptions: policyv1beta1FSGroupStrategyOptions,
    HostPortRange: policyv1beta1HostPortRange,
    IDRange: policyv1beta1IDRange,
    PodDisruptionBudget: policyv1beta1PodDisruptionBudget,
    PodDisruptionBudgetList: policyv1beta1PodDisruptionBudgetList,
    PodDisruptionBudgetSpec: policyv1beta1PodDisruptionBudgetSpec,
    PodDisruptionBudgetStatus: policyv1beta1PodDisruptionBudgetStatus,
    PodSecurityPolicy: policyv1beta1PodSecurityPolicy,
    PodSecurityPolicyList: policyv1beta1PodSecurityPolicyList,
    PodSecurityPolicySpec: policyv1beta1PodSecurityPolicySpec,
    RunAsGroupStrategyOptions: policyv1beta1RunAsGroupStrategyOptions,
    RunAsUserStrategyOptions: policyv1beta1RunAsUserStrategyOptions,
    RuntimeClassStrategyOptions: policyv1beta1RuntimeClassStrategyOptions,
    SELinuxStrategyOptions: policyv1beta1SELinuxStrategyOptions,
    SupplementalGroupsStrategyOptions: policyv1beta1SupplementalGroupsStrategyOptions,
  },
};

export const rbac = {
  v1: {
    AggregationRule: rbacv1AggregationRule,
    ClusterRole: rbacv1ClusterRole,
    ClusterRoleBinding: rbacv1ClusterRoleBinding,
    ClusterRoleBindingList: rbacv1ClusterRoleBindingList,
    ClusterRoleList: rbacv1ClusterRoleList,
    PolicyRule: rbacv1PolicyRule,
    Role: rbacv1Role,
    RoleBinding: rbacv1RoleBinding,
    RoleBindingList: rbacv1RoleBindingList,
    RoleList: rbacv1RoleList,
    RoleRef: rbacv1RoleRef,
    Subject: rbacv1Subject,
  },
  v1alpha1: {
    AggregationRule: rbacv1alpha1AggregationRule,
    ClusterRole: rbacv1alpha1ClusterRole,
    ClusterRoleBinding: rbacv1alpha1ClusterRoleBinding,
    ClusterRoleBindingList: rbacv1alpha1ClusterRoleBindingList,
    ClusterRoleList: rbacv1alpha1ClusterRoleList,
    PolicyRule: rbacv1alpha1PolicyRule,
    Role: rbacv1alpha1Role,
    RoleBinding: rbacv1alpha1RoleBinding,
    RoleBindingList: rbacv1alpha1RoleBindingList,
    RoleList: rbacv1alpha1RoleList,
    RoleRef: rbacv1alpha1RoleRef,
    Subject: rbacv1alpha1Subject,
  },
  v1beta1: {
    AggregationRule: rbacv1beta1AggregationRule,
    ClusterRole: rbacv1beta1ClusterRole,
    ClusterRoleBinding: rbacv1beta1ClusterRoleBinding,
    ClusterRoleBindingList: rbacv1beta1ClusterRoleBindingList,
    ClusterRoleList: rbacv1beta1ClusterRoleList,
    PolicyRule: rbacv1beta1PolicyRule,
    Role: rbacv1beta1Role,
    RoleBinding: rbacv1beta1RoleBinding,
    RoleBindingList: rbacv1beta1RoleBindingList,
    RoleList: rbacv1beta1RoleList,
    RoleRef: rbacv1beta1RoleRef,
    Subject: rbacv1beta1Subject,
  },
};

export const scheduling = {
  v1: {
    PriorityClass: schedulingv1PriorityClass,
    PriorityClassList: schedulingv1PriorityClassList,
  },
  v1alpha1: {
    PriorityClass: schedulingv1alpha1PriorityClass,
    PriorityClassList: schedulingv1alpha1PriorityClassList,
  },
  v1beta1: {
    PriorityClass: schedulingv1beta1PriorityClass,
    PriorityClassList: schedulingv1beta1PriorityClassList,
  },
};

export const settings = {
  v1alpha1: {
    PodPreset: settingsv1alpha1PodPreset,
    PodPresetList: settingsv1alpha1PodPresetList,
    PodPresetSpec: settingsv1alpha1PodPresetSpec,
  },
};

export const storage = {
  v1: {
    StorageClass: storagev1StorageClass,
    StorageClassList: storagev1StorageClassList,
    VolumeAttachment: storagev1VolumeAttachment,
    VolumeAttachmentList: storagev1VolumeAttachmentList,
    VolumeAttachmentSource: storagev1VolumeAttachmentSource,
    VolumeAttachmentSpec: storagev1VolumeAttachmentSpec,
    VolumeAttachmentStatus: storagev1VolumeAttachmentStatus,
    VolumeError: storagev1VolumeError,
  },
  v1alpha1: {
    VolumeAttachment: storagev1alpha1VolumeAttachment,
    VolumeAttachmentList: storagev1alpha1VolumeAttachmentList,
    VolumeAttachmentSource: storagev1alpha1VolumeAttachmentSource,
    VolumeAttachmentSpec: storagev1alpha1VolumeAttachmentSpec,
    VolumeAttachmentStatus: storagev1alpha1VolumeAttachmentStatus,
    VolumeError: storagev1alpha1VolumeError,
  },
  v1beta1: {
    CSIDriver: storagev1beta1CSIDriver,
    CSIDriverList: storagev1beta1CSIDriverList,
    CSIDriverSpec: storagev1beta1CSIDriverSpec,
    CSINode: storagev1beta1CSINode,
    CSINodeDriver: storagev1beta1CSINodeDriver,
    CSINodeList: storagev1beta1CSINodeList,
    CSINodeSpec: storagev1beta1CSINodeSpec,
    StorageClass: storagev1beta1StorageClass,
    StorageClassList: storagev1beta1StorageClassList,
    VolumeAttachment: storagev1beta1VolumeAttachment,
    VolumeAttachmentList: storagev1beta1VolumeAttachmentList,
    VolumeAttachmentSource: storagev1beta1VolumeAttachmentSource,
    VolumeAttachmentSpec: storagev1beta1VolumeAttachmentSpec,
    VolumeAttachmentStatus: storagev1beta1VolumeAttachmentStatus,
    VolumeError: storagev1beta1VolumeError,
  },
};

