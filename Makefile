CI_IMAGE=node:alpine

k8sdefs:
	@npm run update

dist:
	@npm run generate
	@npm run build

dependencies:
	@npm install

publish: dist
	@npm publish

build.ci:
	docker run --entrypoint=make -it $(CI_IMAGE) build
